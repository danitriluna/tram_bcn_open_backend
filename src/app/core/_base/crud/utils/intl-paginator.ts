import { MatPaginatorIntl } from '@angular/material';
// Translate
import { TranslateService } from '@ngx-translate/core';


export class IntlPaginator {
    paginatorIntl = new MatPaginatorIntl();
    text_of = '';

    i18nRangeLabel (page: number, pageSize: number, length: number)  {
        if (length == 0 || pageSize == 0) { return `0 ${this.text_of} ${length}`; }

        length = Math.max(length, 0);

        const startIndex = page * pageSize;

        // If the start index exceeds the list length, do not try and fix the end index to the end.
        const endIndex = startIndex < length ?
            Math.min(startIndex + pageSize, length) :
            startIndex + pageSize;

        // return `${startIndex + 1} - ${endIndex} ${this.text_of} ${length}`;
        return this.translate.instant('MANAGEMENT.COMMON.PAGINATOR_RANGE', {
            startIndex: startIndex + 1,
            endIndex,
            length,
        });
    }
    constructor(private translate: TranslateService) { 
    
        this.paginatorIntl.itemsPerPageLabel = this.translate.instant('MANAGEMENT.COMMON.ITEMS_PER_PAGE');
        this.paginatorIntl.nextPageLabel = this.translate.instant('MANAGEMENT.COMMON.NEXT_PAGE');
        this.paginatorIntl.firstPageLabel = this.translate.instant('MANAGEMENT.COMMON.FIRST_PAGE');
        this.paginatorIntl.lastPageLabel = this.translate.instant('MANAGEMENT.COMMON.LAST_PAGE');
        this.paginatorIntl.previousPageLabel = this.translate.instant('MANAGEMENT.COMMON.PREVIOUS_PAGE');
        this.text_of = this.translate.instant('MANAGEMENT.COMMON.PAGINATOR_OF');
        this.paginatorIntl.getRangeLabel = this.i18nRangeLabel.bind(this);
        // this.paginatorIntl.getRangeLabel = this.getRangeLabel.bind(this);
    
        // return paginatorIntl;
    }

    private getRangeLabel(page: number, pageSize: number, length: number): string {
        if (length === 0 || pageSize === 0) {
            return this.translate.instant('MANAGEMENT.COMMON.PAGINATOR_RANGE_1', { length });
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        // If the start index exceeds the list length, do not try and fix the end index to the end.
        const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
        return this.translate.instant('MANAGEMENT.COMMON.PAGINATOR_RANGE', { startIndex: startIndex + 1, endIndex, length });
    }


    getI18nPaginatorIntl() {
        // const paginatorIntl = new MatPaginatorIntl();
    
        // paginatorIntl.itemsPerPageLabel = this.translate.instant('MANAGEMENT.COMMON.ITEMS_PER_PAGE');
        // paginatorIntl.nextPageLabel = this.translate.instant('MANAGEMENT.COMMON.NEXT_PAGE');
        // paginatorIntl.previousPageLabel = this.translate.instant('MANAGEMENT.COMMON.PREVIOUS_PAGE');
        // paginatorIntl.getRangeLabel = i18nRangeLabel;
    
        return this.paginatorIntl;
    }
}
