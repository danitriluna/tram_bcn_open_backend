// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { debug } from 'util';
import { environment } from '../../../../../environments/environment';
import { Router } from '@angular/router';

/**
 * More information there => https://medium.com/@MetonymyQT/angular-http-interceptors-what-are-they-and-how-to-use-them-52e060321088
 */
@Injectable()
export class InterceptService implements HttpInterceptor {
	constructor(private router: Router) { }

	// intercept request and add token
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		const token: string = localStorage.getItem(environment.authTokenKey);
		let currentLang = localStorage.getItem('language');

		if (token) {
			console.log("token",token);
			request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
		} else {
			// alert("no token");			
			// console.log("no token");
			// console.log('request.url.toString().indexOf("oauth/v2/token")', request.url.toString().indexOf("oauth/v2/token"));
			if (!(request.url.toString().indexOf("oauth/v2/token") >= 0)){		
				localStorage.removeItem(environment.authTokenKey);
				localStorage.removeItem('currentUser');
				this.router.navigate(['/auth/logout']);
			}
		}
		request = request.clone({
			headers: request.headers.set("Content-Type", "application/json")
		});

		if (currentLang) {
			// request = request.clone({
			// 	body: { ...request.body, lang: currentLang.toString() }
			// });
			if (request.method.toLowerCase() == 'post' ) {
				if (request.body instanceof FormData) {
					request = request.clone({
						body: request.body.append('lang', currentLang)
					})
				} else {
					const foo = {}; foo['lang'] = currentLang;
					request = request.clone({
						body: { ...request.body, ...foo }
					})
				}
			}
			if (request.method.toLowerCase() == 'get') {
				request = request.clone({
					params: request.params.set('lang', currentLang)
				});
			} 
		}

		// request = request.clone({
		// 	setHeaders: {
		// 		Accept: "application/json",
		// 		"Access-Control-Allow-Origin": "*"
		// 	}
		// });
		// request = request.clone({
		// 	headers: request.headers.set("Accept", "application/json")
		// });
		// request = request.clone({
		// 	headers: request.headers.set("Access-Control-Allow-Origin", "*")
		// });
		// request = request.clone({
		// 	headers: request.headers.set("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS, PUT")
		// });
		
		// tslint:disable-next-line:no-debugger
		// modify request
		// request = request.clone({
		// 	setHeaders: {
		// 		Authorization: `Bearer ${localStorage.getItem('access_token')}`
		// 	}
		// });
		console.log('----request----');
		console.log(request);
		console.log('--- end of request---');

		console.log("request.headers", request.headers);
		console.log("request.headers.get", request.headers.get("Access-Control-Allow-Origin"));

		return next.handle(request).pipe(
			tap(
				event => {
					console.log("Event", event);
					 if (event instanceof HttpResponse) {
						// console.log('all looks good');
						// http response status code
						// console.log(event.status);
					}
				},
				error => {
					// alert("ERROR");
					// http response status code
					// console.log('----response----');
					// console.error('status code:');
					// tslint:disable-next-line:no-debugger
					// console.error(error.status);
					// console.error(error.message);
					// console.log('--- end of response---');
					if(error.status != 401){ // no autorizado
						return throwError(error);
					}
					localStorage.removeItem(environment.authTokenKey);
					localStorage.removeItem('currentUser');
					this.router.navigate(['/auth/login']);
				}
			)
		);
	}
}
