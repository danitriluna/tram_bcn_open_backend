// Angular
import { Injectable } from '@angular/core';
// RxJS
import { BehaviorSubject } from 'rxjs';
// Translate
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class KtDialogService {
	private ktDialog: any;
	private currentState: BehaviorSubject<boolean> = new BehaviorSubject(false);

	// Public properties
	constructor(private translate: TranslateService) {
		this.ktDialog = new KTDialog({ type: 'loader', placement: 'top center', message: this.translate.instant('COMMON.LOADING')});
	}

	show() {
		this.currentState.next(true);
		this.ktDialog.show();
	}


	hide() {
		this.currentState.next(false);
		this.ktDialog.hide();
	}

	checkIsShown() {
		return this.currentState.value;
	}
}
