// Angular
import { ElementRef, Injectable } from '@angular/core';
import { animate, AnimationBuilder, style } from '@angular/animations';

@Injectable()
export class LoadingBlockService {
	// Private properties
	private el: ElementRef;
	private stopped: boolean;

	/**
	 * Service constctuctor
	 *
	 * @param animationBuilder: AnimationBuilder
	 */
	constructor(private animationBuilder: AnimationBuilder) {
	}

	/**
	 * Init
	 *
	 * @param element: ElementRef
	 */
	init(element: ElementRef) {
		this.el = element;
	}

	/**
	 * Hide
	 */
	hide() {
		console.log("################## HIDE ####################");
		if (this.stopped) {
			console.log("################## return ####################");
			return;
		}
		console.log("################## NONE ####################");
		this.el.nativeElement.style.display = 'none';
		this.el.nativeElement.style.opacity = '0';
		this.stopped = true;
		/*
		const player = this.animationBuilder.build([
			style({opacity: '1'}),
			animate(800, style({opacity: '0'}))
		]).create(this.el.nativeElement);

		player.onDone(() => {
			// if (typeof this.el.nativeElement.remove === 'function') {
			// 	this.el.nativeElement.remove();
			// } else {
				this.el.nativeElement.style.display = 'none';
			// }
			this.stopped = true;
		});

		setTimeout(() => player.play(), 300);*/
	}

	/**
	 * Show
	 */
	show() {

		console.log("################## SHOW ####################");
		// console.log("################## SHOW ####################");
		if (this.stopped === false) {
			console.log("################## return ####################");
			return;
		}


		console.log("################## BLOCK ####################");
		this.el.nativeElement.style.display = 'block';
		this.el.nativeElement.style.opacity = '1';
		this.stopped = false;

		// const player = this.animationBuilder.build([
		// 	style({opacity: '0'}),
		// 	animate(800, style({opacity: '1'}))
		// ]).create(this.el.nativeElement);

		// player.onDone(() => {
		// 	// if (typeof this.el.nativeElement.remove === 'function') {
		// 	// 	this.el.nativeElement.remove();
		// 	// } else {
		// 		this.el.nativeElement.style.display = 'block';
		// 	// }
		// 	this.stopped = false;
		// });

		// setTimeout(() => player.play(), 300);
	}
}
