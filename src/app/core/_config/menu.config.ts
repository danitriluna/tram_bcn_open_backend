

export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
					permission: 'access',
				},
				{
					section: 'Management',
					translate: 'MANAGEMENT.COMMON.SECTION_TITLE',
				},
				{
					title: 'User Management',
					translate: 'USER_MANAGEMENT.TITLE',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-user-outline-symbol',
					permission: 'manageUsers',
					submenu: [
						{
							title: 'Users',
							translate: 'USER_MANAGEMENT.ENTITIES',
							page: '/user-management/users',
							permission: 'manageUsers'
						},
						{
							title: 'Arbitrators',
							translate: 'ARBITRATOR_MANAGEMENT.ENTITIES',
							page: '/user-management/arbitrators',
							permission: 'manageUsers'
						},
						{
							title: 'Players',
							translate: 'PLAYER_MANAGEMENT.ENTITIES',
							page: '/user-management/players',
							permission: 'manageUsers'
						},
						{
							title: 'Roles',
							translate: 'ROLE_MANAGEMENT.ENTITIES',
							page: '/user-management/roles',
							permission: 'manageUsers'
						},
						{
							title: 'Countries',
							translate: 'COUNTRY_MANAGEMENT.ENTITIES',
							page: '/country-management/countries',
							permission: 'manage'
						}
					]
				},
				{
					title: 'Match Management',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-cup',
					permission: 'manage',
					translate: 'MATCH_MANAGEMENT.TITLE',
					submenu: [
						{
							title: 'Matches',
							translate: 'MATCH_MANAGEMENT.ENTITIES',
							page: '/match-management/matches',
							permission: 'manage'
						},
						{
							title: 'Courts',
							translate: 'COURT_MANAGEMENT.ENTITIES',
							page: '/court-management/courts',
							permission: 'manage'
						},
						{
							title: 'Teams',
							translate: 'TEAM_MANAGEMENT.ENTITIES',
							page: '/team-management/teams',
							permission: 'manage'
						}
					]
				},
				{
					title: 'News Management',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-file',
					permission: 'manage',
					translate: 'NEW_MANAGEMENT.TITLE',
					submenu: [
						{
							title: 'News',
							translate: 'NEW_MANAGEMENT.ENTITIES',
							page: '/new-management/news',
							permission: 'manage'
						}
					]
				},
				{
					title: 'Sponsor Management',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-browser',
					permission: 'manageUsers',
					translate: 'SPONSOR_MANAGEMENT.TITLE',
					submenu: [
						{
							title: 'Sponsors',
							translate: 'SPONSOR_MANAGEMENT.ENTITIES',
							page: '/sponsor-management/sponsors',
							permission: 'manageUsers'
						},
						{
							title: 'Banners',
							translate: 'BANNER_MANAGEMENT.ENTITIES',
							page: '/banner-management/banners',
							permission: 'manageUsers'
						},
						{
							title: 'Positions',
							translate: 'POSITION_MANAGEMENT.ENTITIES',
							page: '/position-management/positions',
							permission: 'manageUsers'
						}
					]
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
