// Spain
export const locale = {
	lang: 'es',
	data: {
		TRANSLATOR: {
			SELECT: 'Elige tu idioma',
		},
		MENU: {
			NEW: 'nuevo',
			ACTIONS: 'Comportamiento',
			CREATE_POST: 'Crear nueva publicación',
			PAGES: 'Pages',
			FEATURES: 'Caracteristicas',
			APPS: 'Aplicaciones',
			DASHBOARD: 'Estadísticas'
		},
		AUTH: {
			GENERAL: {
				OR: 'O',
				SUBMIT_BUTTON: 'Enviar',
				NO_ACCOUNT: 'No tienes una cuenta?',
				SIGNUP_BUTTON: 'Regístrate',
				FORGOT_BUTTON: 'Se te olvidó tu contraseña',
				BACK_BUTTON: 'Volver',
				PRIVACY: 'Privacidad',
				LEGAL: 'Legal',
				CONTACT: 'Contacto',
			},
			LOGIN: {
				TITLE: 'Iniciar sesión',
				BUTTON: 'Acceder',
				ERROR: 'Error al inciar sesión',
			},
			FORGOT: {
				TITLE: 'Contraseña olvidada?',
				DESC: 'Ingrese su correo electrónico para restablecer su contraseña',
				SUCCESS: 'Your account has been successfully reset.'
			},
			REGISTER: {
				TITLE: 'Sign Up',
				DESC: 'Enter your details to create your account',
				SUCCESS: 'Your account has been successfuly registered.'
			},
			INPUT: {
				EMAIL: 'Email',
				FULLNAME: 'Fullname',
				PASSWORD: 'Password',
				CONFIRM_PASSWORD: 'Confirm Password',
				USERNAME: 'Usuario'
			},
			VALIDATION: {
				INVALID: '{{name}} is not valid',
				REQUIRED: '{{name}} is required',
				MIN_LENGTH: '{{name}} minimum length is {{min}}',
				AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
				NOT_FOUND: 'The requested {{name}} is not found',
				INVALID_LOGIN: 'The login detail is incorrect',
				REQUIRED_FIELD: 'Required field',
				MIN_LENGTH_FIELD: 'Minimum field length:',
				MAX_LENGTH_FIELD: 'Maximum field length:',
				INVALID_FIELD: 'Field is not valid',
			}
		},
		USER_MANAGEMENT: {
			TITLE: 'Gestión de usuarios',
			ENTITY: 'Usuario',
			ENTITIES: 'Usuarios',
			ENTITY_MIN: 'usuario',
			ENTITIES_MIN: 'usuarios',
			EDIT: {
				UPDATE_MESSAGE: 'El usuario se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el usuario',
				ADD_MESSAGE: 'El usuario se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el usuario'
			}
		},
		ARBITRATOR_MANAGEMENT: {
			ENTITY: 'Árbitro',
			ENTITIES: 'Árbitros',
			ENTITY_MIN: 'árbitro',
			ENTITIES_MIN: 'árbitros',
			EDIT: {
				UPDATE_MESSAGE: 'El árbitro se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el árbitro',
				ADD_MESSAGE: 'El árbitro se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el árbitro'
			}
		},
		PLAYER_MANAGEMENT: {
			ENTITY: 'Jugador',
			ENTITIES: 'Jugadores',
			ENTITY_MIN: 'jugador',
			ENTITIES_MIN: 'jugadores',
			EDIT: {
				UPDATE_MESSAGE: 'El jugador se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el jugador',
				ADD_MESSAGE: 'El jugador se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el jugador'
			}
		},
		SPONSOR_MANAGEMENT: {
			TITLE: 'Gestión de sponsors',
			ENTITY: 'Sponsor',
			ENTITIES: 'Sponsors',
			ENTITY_MIN: 'sponsor',
			ENTITIES_MIN: 'sponsors',
			EDIT: {
				UPDATE_MESSAGE: 'El sponsor se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el sponsor',
				ADD_MESSAGE: 'El sponsor se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el sponsor'
			}
		},
		MATCH_MANAGEMENT: {
			TITLE: 'Gestión de partidos',
			ENTITY: 'Partido',
			ENTITIES: 'Partidos',
			ENTITY_MIN: 'partido',
			ENTITIES_MIN: 'partidos',
			EDIT: {
				UPDATE_MESSAGE: 'El partido se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el partido',
				ADD_MESSAGE: 'El partido se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el partido'
			}
		},
		NEW_MANAGEMENT: {
			TITLE: 'Gestión de noticias',
			ENTITY: 'Noticia',
			ENTITIES: 'Noticias',
			ENTITY_MIN: 'noticia',
			ENTITIES_MIN: 'noticias',
			EDIT: {
				UPDATE_MESSAGE: 'La noticia se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar la noticia',
				ADD_MESSAGE: 'La noticia se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear la noticia'
			}
		},
		NEW_IMG_MANAGEMENT: {
			TITLE: 'Gestión de imágenes',
			ENTITY: 'Imagen',
			ENTITIES: 'Imágenes',
			ENTITY_MIN: 'imagen',
			ENTITIES_MIN: 'imágenes',
			EDIT: {
				UPDATE_MESSAGE: 'La imagen se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar la imagen',
				ADD_MESSAGE: 'La imagen se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear la imagen'
			}
		},
		NEW_VIDEO_MANAGEMENT: {
			TITLE: 'Gestión de videos',
			ENTITY: 'Video',
			ENTITIES: 'Videos',
			ENTITY_MIN: 'video',
			ENTITIES_MIN: 'videos',
			EDIT: {
				UPDATE_MESSAGE: 'El video se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el video',
				ADD_MESSAGE: 'El video se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el video'
			}
		},
		NEW_IMAGE_MANAGEMENT: {
			TITLE: 'Gestión de imágenes',
			FEATURED: 'Destacar',
			ENTITY: 'Imagen',
			ENTITIES: 'Imágenes',
			ENTITY_MIN: 'imagen',
			ENTITIES_MIN: 'imagenes',
			EDIT: {
				UPDATE_MESSAGE: 'La imagen se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar la imagen',
				ADD_MESSAGE: 'La imagen se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear la imagen'
			}
		},
		COURT_MANAGEMENT: {
			TITLE: 'Gestión de pistas',
			ENTITY: 'Pista',
			ENTITIES: 'Pistas',
			ENTITY_MIN: 'pista',
			ENTITIES_MIN: 'pistas',
			EDIT: {
				UPDATE_MESSAGE: 'La pista se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar la pista',
				ADD_MESSAGE: 'La pista se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear la pista'
			}
		},
		COUNTRY_MANAGEMENT: {
			TITLE: 'Gestión de paises',
			ENTITY: 'País',
			ENTITIES: 'Paises',
			ENTITY_MIN: 'país',
			ENTITIES_MIN: 'paises',
			EDIT: {
				UPDATE_MESSAGE: 'El país se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el país',
				ADD_MESSAGE: 'El país se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el país'
			}
		},
		TEAM_MANAGEMENT: {
			TITLE: 'Gestión de equipos',
			ENTITY: 'Equipo',
			ENTITIES: 'Equipos',
			ENTITY_MIN: 'equipo',
			ENTITIES_MIN: 'equipos',
			EDIT: {
				UPDATE_MESSAGE: 'El equipo se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el equipo',
				ADD_MESSAGE: 'El equipo se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el equipo'
			}
		},
		BANNER_MANAGEMENT: {
			TITLE: 'Gestión de banners',
			ENTITY: 'Banner',
			ENTITIES: 'Banners',
			ENTITY_MIN: 'banner',
			ENTITIES_MIN: 'banners',
			EDIT: {
				UPDATE_MESSAGE: 'El banner se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el banner',
				ADD_MESSAGE: 'El banner se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el banner'
			}
		},
		POSITION_MANAGEMENT: {
			TITLE: 'Gestión de posiciones',
			ENTITY: 'Posición',
			ENTITIES: 'Posiciones',
			ENTITY_MIN: 'posición',
			ENTITIES_MIN: 'posiciones',
			EDIT: {
				UPDATE_MESSAGE: 'La posición se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar la posición',
				ADD_MESSAGE: 'La posición se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear la posición'
			}
		},
		ROLE_MANAGEMENT: {
			ENTITY: 'Rol',
			ENTITIES: 'Roles',
			ENTITY_MIN: 'rol',
			ENTITIES_MIN: 'roles',
			PERMISSIONS: 'Permisos',
			EDIT: {
				UPDATE_MESSAGE: 'El rol se ha editado',
				UPDATE_ERROR_MESSAGE: 'Error al editar el rol',
				ADD_MESSAGE: 'El rol se ha creado',
				ADD_ERROR_MESSAGE: 'Error al crear el rol'
			}
		},
		MANAGEMENT: {
			COMMON: {
				SECTION_TITLE: 'Gestión',
				TITLE: 'Gestión de {{entity}}',
				ENTITY_LIST: 'Listado de {{entity}}',
				CREATE_ENTITY: 'Crear {{entity}}',
				EDIT_ENTITY: 'Editar {{entity}}',
				EDIT_ENTITY_NAME: 'Editar {{entity}} - {{entity_name}}',
				DELETE: {
					TITLE: 'Borrar {{entity}} ({{title}})',
					DESCRIPTION: '¿Seguro que quieres borrar?',
					WAIT_DESCRIPTION: 'Borrando...',
					MESSAGE: 'Se ha borrado correctamente'
				},
				CHANGE_PASSWORD_OK: 'La contraseña se ha modificado correctamente',
				PAGINATOR_OF: 'de',
				PAGINATOR_RANGE: '{{startIndex}} - {{endIndex}} de {{length}}',
				ITEMS_PER_PAGE: 'Resultados por página',
				NEXT_PAGE: 'Siguiente',
				FIRST_PAGE: 'Primera',
				LAST_PAGE: 'Última',
				PREVIOUS_PAGE: 'Anterior',
			}
		},
		COMMON: {
			HI: 'Hola, ',
			LOADING: 'Cargando...',
			SELECTED_RECORDS_COUNT: 'Resultados seleccionados: ',
			ACTIONS: 'Acciones',
			ALL: 'Todos',
			NONE: 'Ninguno',
			SUSPENDED: 'Suspendido',
			ACTIVE: 'Activo',
			FILTER: 'Filtro',
			BY_STATUS: 'por estado',
			BY_TYPE: 'por tipo',
			SEARCH: 'Buscar',
			LEGAL: 'Legal',
			PRIVACY: 'Privacidad',
			NO_TRANSLATE: '(Sin traducción)',
			CONTACT: 'Contacto',
			NO_RECORDS_FOUND: 'No hay resultados',
			PLEASE_WAIT: 'Por favor, espera',
			IN_ALL_FIELDS: 'en todos los campos',
			LOGIN: {
				TITLE: 'Tram Barcelona Open',
				SUBTITLE: 'Backend'
			},
			TABS: {
				INFO: 'Información',
				VIDEOS: 'Videos',
				IMGS: 'Imágenes',
				CHANGE_PASSWORD: 'Cambiar contraseña',
			},
			FIELDS: {
				INFO: 'Información',
				ID: 'ID',
				TITLE: 'Título',
				SUBTITLE: 'Subtítulo',
				TEXT: 'Texto',
				NAME: 'Nombre',
				ROLE: 'Rol',
				LASTNAME1: 'Primer apellido',
				LASTNAME2: 'Segundo apellido',
				USERNAME: 'Usuario',
				EMAIL: 'Email',
				IMAGE: 'Imagen',
				ORDER: 'Orden',
				SEX: 'Sexo',
				SEX_MAN: 'Masculino',
				SEX_WOMAN: 'Femenino',
				RANKING: 'Ranking individual',
				BEST_RANKING: 'Mejor ranking individual',
				RANKING_DOUBLES: 'Ranking dobles',
				BEST_RANKING_DOUBLES: 'Mejor ranking dobles',
				BIRTHDAY_YEAR: 'Año de nacimiento',
				AGE: 'Edad',
				AGE_DEBUT: 'Edad de debut',
				URL: 'Enlace',
				PLAY: 'Juego',
				PLAY_LEFT: 'Izquierda',
				PLAY_RIGHT: 'Derecha',
				SURFACE: 'Superficie',
				PASSWORD_CONFIRM: 'Confirmar contraseña',
				PASSWORD: 'Contraseña',
				PUBLICATION_DATE: 'Fecha de publicación',
				CREATED_DATE: 'Fecha de creación',
				DATE_BEST_RANKING: 'Fecha del mejor ranking individual',
				DATE_BEST_RANKING_DOUBLES: 'Fecha del mejor ranking dobles',
				PLAYER: 'Jugador',
				ARBITRATOR: 'Árbitro',
				WINNER: 'Ganador',
				RETIRED: 'Retirado',
				CATEGORY: 'Categoría',
				COURT: 'Pista',
				DATE: 'Fecha',
				DATE_PUBLICATION: 'Fecha de publicación',
				FINALIZED: 'Finalizado',
				MAP: 'Mapa',
				COORDS: 'Coordenadas',
			},
			BUTTONS: {
				BACK: 'Volver',
				RESET: 'Resetear',
				SAVE: 'Guardar',
				ADD: 'Añadir',
				CANCEL: 'Cancelar',
				DELETE: 'Borrar',
				DELETE_FILE: 'Eliminar archivo',
				DELETE_ELEMENT: 'Eliminar elemento',
				CHANGE_PASSWORD: 'Cambiar contraseña',
				SELECT_FILE: 'Seleccionar archivo',
			},
			MESSAGES: {
				NO_IMAGE: 'Ninguna imagen seleccionada',
			},
			ERRORS: {
				COMMON: 'Ha ocurrido un error',
				NO_CHANGES: 'Oh! Parece que no se ha modificado ningún campo.',
				REQUIRED: 'Obligatorio',
				NO_DATA: 'No se han encontrado datos',
				USERNAME_FORMAT: 'El formato no es válido, sólo letras y números',
				FORM_ERROR: 'El formulario no es válido, revisa los campos obligatorios.',
			}
		}
	}
};
