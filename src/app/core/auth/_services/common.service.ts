import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { Surface } from '../_models/surface.model';
import { Lang } from '../_models/lang.model';
import { Role } from '../_models/role.model';
import { catchError, map, tap } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';

const API_SURFACES_URL = environment.apiEndpoint +'/api/surfaces';
const API_LANGS_URL = environment.apiEndpoint +'/api/langs';

@Injectable()
export class CommonService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}


    // Surfaces
    getAllSurfaces(): Observable<Surface[]> {
        return this.http.get<Surface[]>(API_SURFACES_URL);
    }
    

    // Langs
    getAllLangs(): Observable<Lang[]> {
        return this.http.get<Lang[]>(API_LANGS_URL);
    }
    // findLangs(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    //     const httpHeaders = new HttpHeaders();
    //     httpHeaders.set('Content-Type', 'application/json');
    //     return this.http.post<QueryResultsModel>(API_LANGS_URL + '/findLangs', queryParams, { headers: httpHeaders });
    // }
    

    


 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.

            const message = this.translate.instant('COMMON.ERRORS.COMMON');
            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
            
            return of(result);
        };
    }
}
