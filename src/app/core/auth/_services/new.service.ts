import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { New } from '../_models/new.model';
import { NewVideo } from '../_models/newVideo.model';
import { NewImage } from '../_models/newImage.model';
import { catchError, map, tap } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';

const API_NEWS_URL = environment.apiEndpoint + '/api/news';
const API_NEW_URL = environment.apiEndpoint + '/api/new';


@Injectable()
export class NewService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}



    /* News */
    /* New */
    getAllNews(): Observable<New[]> {
        return this.http.get<New[]>(API_NEWS_URL)
            .pipe(catchError(this.handleError('getNews-error', [])));
    }

    getNewById(newId: number): Observable<New> {
        return this.http.get<New>(API_NEW_URL + `/detail/${newId}`)
            .pipe(catchError(this.handleError('getNew-error', [])));
    }


    // DELETE => delete the new from the server
    deleteNew(newId: number) {
        const url = `${API_NEW_URL}/delete/${newId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteNew-error', [])));
    }

    // UPDATE => PUT: update the new on the server
    updateNew(newData: New): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_NEW_URL + '/update', newData, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateNew-error', [])));
    }

    // CREATE =>  POST: add a new new to the server
    createNew(newData: New): Observable<New> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<New>(API_NEW_URL + '/create', newData, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createNew-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findNews(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_NEWS_URL + '/findNews', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('findNews-error', [])));
    }
    


    /* VIDEO */

    // CREATE =>  POST: add a new video to the server
    createNewVideo(newVideoData: NewVideo): Observable<New> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<New>(API_NEW_URL + '/createVideo', newVideoData, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createNewVideo-error', [])));
    }

    /* IMAGEN */

    createNewImage(newImageData: NewImage): Observable<New> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<New>(API_NEW_URL + '/createImage', newImageData, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createNewImage-error', [])));
    }


 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.
            let message = '';

            switch (operation) {
                case 'getNews-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getNew-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteNew-error': {
                    message = this.translate.instant('NEW_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateNew-error': {
                    message = this.translate.instant('NEW_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createNew-error': {
                    message = this.translate.instant('NEW_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findNews-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            }

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
