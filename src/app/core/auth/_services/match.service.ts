import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Match } from '../_models/match.model';
import { Category } from '../_models/category.model';
import { Court } from '../_models/court.model';
import { Surface } from '../_models/surface.model';
import { catchError, map, tap } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { Team } from '..';

const API_MATCHES_URL = environment.apiEndpoint +'/api/matches';
const API_MATCH_URL = environment.apiEndpoint + '/api/match';
const API_CATEGORIES_URL = environment.apiEndpoint + '/api/categories';
const API_COURTS_URL = environment.apiEndpoint + '/api/courts';
const API_COURT_URL = environment.apiEndpoint + '/api/court';
const API_TEAMS_URL = environment.apiEndpoint + '/api/teams';
const API_TEAM_URL = environment.apiEndpoint + '/api/team';


@Injectable()
export class MatchService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}



    /* Matches */
    getAllMatches(): Observable<Match[]> {
        return this.http.get<Match[]>(API_MATCHES_URL)
            .pipe(catchError(this.handleError('getMatches-error', [])));
    }

    getMatchById(matchId: number): Observable<Match> {
        return this.http.get<Match>(API_MATCH_URL + `/detail/${matchId}`)
            .pipe(catchError(this.handleError('getMatch-error', [])));
    }


    // DELETE => delete the match from the server
    deleteMatch(matchId: number) {
        const url = `${API_MATCH_URL}/delete/${matchId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteMatch-error', [])));
    }

    // UPDATE => PUT: update the match on the server
    updateMatch(match: Match): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_MATCH_URL + '/update', match, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateMatch-error', [])));
    }

    // CREATE =>  POST: add a new match to the server
    createMatch(match: Match): Observable<Match> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<Match>(API_MATCH_URL + '/create', match, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createMatch-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findMatches(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_MATCHES_URL + '/findMatches', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('getMatches-error', [])));
    }





    /************************************/


    /* Court */
    getAllCourts(): Observable<Court[]> {
        return this.http.get<Court[]>(API_COURTS_URL)
            .pipe(catchError(this.handleError('getCourts-error', [])));
    }

    getCourtById(courtId: number): Observable<Court> {
        return this.http.get<Court>(API_COURT_URL + `/detail/${courtId}`)
            .pipe(catchError(this.handleError('getCourt-error', [])));
    }


    // DELETE => delete the court from the server
    deleteCourt(courtId: number) {
        const url = `${API_COURT_URL}/delete/${courtId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteCourt-error', [])));
    }

    // UPDATE => PUT: update the court on the server
    updateCourt(court: Court): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_COURT_URL + '/update', court, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateCourt-error', [])));
    }

    // CREATE =>  POST: add a new court to the server
    createCourt(court: Court): Observable<Court> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<Court>(API_COURT_URL + '/create', court, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createCourt-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findCourts(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_COURTS_URL + '/findCourts', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('findCourts-error', [])));
    }
    


    // Categories
    getAllCategories(): Observable<Surface[]> {
        return this.http.get<Category[]>(API_CATEGORIES_URL)
            .pipe(catchError(this.handleError('getCategories-error', [])));
    }


    // Teams

    getAllTeams(): Observable<Team[]> {
        return this.http.get<Team[]>(API_TEAMS_URL)
            .pipe(catchError(this.handleError('getTeams-error', [])));
    }

    getTeamById(teamId: number): Observable<Team> {
        return this.http.get<Team>(API_TEAM_URL + `/detail/${teamId}`)
            .pipe(catchError(this.handleError('getTeam-error', [])));
    }

    // DELETE => delete the team from the server
    deleteTeam(teamId: number) {
        const url = `${API_TEAM_URL}/delete/${teamId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteTeam-error', [])));
    }

    // UPDATE => PUT: update the team on the server
    updateTeam(team: Team): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_TEAM_URL + '/update', team, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateTeam-error', [])));
    }

    // CREATE =>  POST: add a new team to the server
    createTeam(team: Team): Observable<Team> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<Team>(API_TEAM_URL + '/create', team, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createTeam-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findTeams(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_TEAMS_URL + '/findTeams', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('findTeams-error', [])));
    }


 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.
            let message = '';
            switch (operation) {
                case 'getMatches-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getMatch-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteMatch-error': {
                    message = this.translate.instant('MATCH_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateMatch-error': {
                    message = this.translate.instant('MATCH_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createMatch-error': {
                    message = this.translate.instant('MATCH_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findMatches-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getCategories-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getCourts-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getCourt-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteCourt-error': {
                    message = this.translate.instant('COURT_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateCourt-error': {
                    message = this.translate.instant('COURT_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createCourt-error': {
                    message = this.translate.instant('COURT_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findCourts-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getTeams-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getTeam-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteTeam-error': {
                    message = this.translate.instant('TEAM_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateTeam-error': {
                    message = this.translate.instant('TEAM_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createTeam-error': {
                    message = this.translate.instant('TEAM_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findTeams-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            } 

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
