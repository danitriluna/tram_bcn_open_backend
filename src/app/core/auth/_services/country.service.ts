import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Country } from '../_models/country.model';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { catchError } from 'rxjs/operators';

const API_COUNTRIES_URL = environment.apiEndpoint +'/api/countries';
const API_COUNTRY_URL = environment.apiEndpoint +'/api/country';

@Injectable()
export class CountryService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}



    /* Countries */
    getAllCountries(): Observable<Country[]> {
        return this.http.get<Country[]>(API_COUNTRIES_URL)
            .pipe(catchError(this.handleError('getCountries-error', [])));
    }

    getCountryById(countryId: number): Observable<Country> {
        return this.http.get<Country>(API_COUNTRY_URL + `/detail/${countryId}`)
            .pipe(catchError(this.handleError('getCountry-error', [])));
    }


    // DELETE => delete the country from the server
    deleteCountry(countryId: number) {
        const url = `${API_COUNTRY_URL}/delete/${countryId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteCountry-error', [])));
    }

    // UPDATE => PUT: update the country on the server
    updateCountry(country: Country): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_COUNTRY_URL + '/update', country, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateCountry-error', [])));
    }

    // CREATE =>  POST: add a new country to the server
    createCountry(country: Country): Observable<Country> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put<Country>(API_COUNTRY_URL + '/create', country, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createCountry-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findCountries(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_COUNTRIES_URL + '/findCountries', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('findCountries-error', [])));
    }




 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.

            let message = '';

            switch (operation) {
                case 'getCountries-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getCountry-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteCountry-error': {
                    message = this.translate.instant('COUNTRY_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateCountry-error': {
                    message = this.translate.instant('COUNTRY_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createCountry-error': {
                    message = this.translate.instant('COUNTRY_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findCountries-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            }

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
