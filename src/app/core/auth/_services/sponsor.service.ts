import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Sponsor } from '../_models/sponsor.model';
import { Position } from '../_models/position.model';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { Banner } from '..';
import { catchError } from 'rxjs/operators';

const API_SPONSORS_URL = environment.apiEndpoint +'/api/sponsors';
const API_SPONSOR_URL = environment.apiEndpoint +'/api/sponsor';
const API_BANNERS_URL = environment.apiEndpoint +'/api/banners';
const API_BANNER_URL = environment.apiEndpoint +'/api/banner';
const API_POSITIONS_URL = environment.apiEndpoint +'/api/positions';
const API_POSITION_URL = environment.apiEndpoint +'/api/position';

@Injectable()
export class SponsorService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}



    /* Sponsors */
    getAllSponsors(): Observable<Sponsor[]> {
        return this.http.get<Sponsor[]>(API_SPONSORS_URL)
            .pipe(catchError(this.handleError('getSponsors-error', [])));
    }

    getSponsorById(sponsorId: number): Observable<Sponsor> {
        return this.http.get<Sponsor>(API_SPONSOR_URL + `/detail/${sponsorId}`)
            .pipe(catchError(this.handleError('getSponsor-error', [])));
    }


    // DELETE => delete the sponsor from the server
    deleteSponsor(sponsorId: number) {
        const url = `${API_SPONSOR_URL}/${sponsorId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteSponsor-error', [])));
    }

    // UPDATE => PUT: update the sponsor on the server
    updateSponsor(sponsor: Sponsor): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_SPONSOR_URL + '/update', sponsor, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateSponsor-error', [])));
    }

    // CREATE =>  POST: add a new sponsor to the server
    createSponsor(sponsor: Sponsor): Observable<Sponsor> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put<Sponsor>(API_SPONSOR_URL + '/create', sponsor, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createSponsor-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findSponsors(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_SPONSORS_URL + '/findSponsors', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('getSponsors-error', [])));
    }




    /* Banners */
    getAllBanners(): Observable<Banner[]> {
        return this.http.get<Banner[]>(API_BANNERS_URL)
            .pipe(catchError(this.handleError('getBanners-error', [])));
    }

    getBannerById(bannerId: number): Observable<Banner> {
        return this.http.get<Banner>(API_BANNER_URL + `/detail/${bannerId}`)
            .pipe(catchError(this.handleError('getBanner-error', [])));
    }


    // DELETE => delete the banner from the server
    deleteBanner(bannerId: number) {
        const url = `${API_BANNER_URL}/${bannerId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteBanner-error', [])));
    }

    // UPDATE => PUT: update the banner on the server
    updateBanner(banner: Banner): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_BANNER_URL + '/update', banner, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateBanner-error', [])));
    }

    // CREATE =>  POST: add a new banner to the server
    createBanner(banner: Banner): Observable<Banner> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put<Banner>(API_BANNER_URL + '/create', banner, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createBanner-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findBanners(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_BANNERS_URL + '/findBanners', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('getBanners-error', [])));
    }

    // Positions
    getAllPositions(): Observable<Position[]> {
        return this.http.get<Position[]>(API_POSITIONS_URL)
            .pipe(catchError(this.handleError('getAllPositions-error', [])));
    }

 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.
            let message = '';

            switch (operation) {
                case 'getSponsors-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getSponsor-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteSponsor-error': {
                    message = this.translate.instant('SPONSOR_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateSponsor-error': {
                    message = this.translate.instant('SPONSOR_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createSponsor-error': {
                    message = this.translate.instant('SPONSOR_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findSponsor-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getBanners-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getBanner-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteBanner-error': {
                    message = this.translate.instant('BANNER_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateBanner-error': {
                    message = this.translate.instant('BANNER_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createBanner-error': {
                    message = this.translate.instant('BANNER_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findBanner-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            }

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
