import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';

const API_USER_URL = environment.apiEndpoint +'/api/user';
const API_AUTH = environment.apiEndpoint +'/oauth/v2/token';

@Injectable()
export class AuthService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}
    // Authentication/Authorization
    login(username: string, password: string): Observable<User> {
        console.log("LOGIN");
        // return this.http.post<User>(API_USERS_URL, { username, password });
        return this.http
            .post(API_AUTH, {
                client_id: environment.client_id,
                client_secret: environment.client_secret,
                grant_type: environment.grant_type,
                username: username,
                password: password
            })
            .pipe(
                map((res: any) => {
                    console.log("res ", res);
                    localStorage.clear();
                    localStorage.setItem(environment.authTokenKey, res.access_token);
                    // localStorage.setItem('roles', res.roles);
                    // localStorage.setItem(
                    //     "currentUser",
                    //     JSON.stringify({
                    //         id: res.id,
                    //         username: res.username,
                    //         nombre: res.nombre,
                    //         roles: res.roles,
                    //         rol: 'Gestor'
                    //     })
                    // );
                    return res;
                    // return this.getUserByToken();
                    // const userToken = localStorage.getItem(environment.authTokenKey);
                    // const httpHeaders = new HttpHeaders();
                    // httpHeaders.set('Authorization', 'Bearer ' + userToken);
                    // return this.http.get<User>(API_USERS_URL, { headers: httpHeaders });
                }),
                catchError(this.handleError('login-error', []))
            );
    }

    logout() {
        // remove user from local storage to log user out
        // alert("OUT");
        localStorage.removeItem('currentUser');
    }


    getUserByToken(): Observable<User> {
        const userToken = localStorage.getItem(environment.authTokenKey);
        // const httpHeaders = new HttpHeaders();
        // httpHeaders.set('Authorization', 'Bearer ' + userToken);
        return this.http.get<User>(API_USER_URL + '/token/' + userToken)
            .pipe(
                map((res: User) => {
                    console.log("res ", res);
                    localStorage.setItem(
                        "currentUser",
                        JSON.stringify({
                            id: res.id,
                            username: res.username,
                            name: res.name,
                            roles: res.roles
                        })
                    );
                    return res;
                })
            );
    }


    register(user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<User>(API_USER_URL, user, { headers: httpHeaders })
            .pipe(
                map((res: User) => {
                    return res;
                }),
                catchError(err => {
                    return null;
                })
            );
    }

    /*
     * Submit forgot password request
     *
     * @param {string} email
     * @returns {Observable<any>}
     */
    public requestPassword(email: string): Observable<any> {
    	return this.http.get(API_USER_URL + '/forgot?=' + email)
    		.pipe(catchError(this.handleError('forgot-password', []))
	    );
    }

 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.

            let message = this.translate.instant('PLAYER_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
            if(operation == 'login-error'){
                message = this.translate.instant('AUTH.LOGIN.ERROR');
            }
            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
