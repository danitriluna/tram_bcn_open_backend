import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Position } from '../_models/position.model';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { catchError } from 'rxjs/operators';

const API_POSITIONS_URL = environment.apiEndpoint +'/api/positions';
const API_POSITION_URL = environment.apiEndpoint +'/api/position';

@Injectable()
export class PositionService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}



    /* Positions */
    getAllPositions(): Observable<Position[]> {
        return this.http.get<Position[]>(API_POSITIONS_URL)
            .pipe(catchError(this.handleError('getPositions-error', [])));
    }

    getPositionById(positionId: number): Observable<Position> {
        return this.http.get<Position>(API_POSITION_URL + `/detail/${positionId}`)
            .pipe(catchError(this.handleError('getPosition-error', [])));
    }


    // DELETE => delete the position from the server
    deletePosition(positionId: number) {
        const url = `${API_POSITION_URL}/${positionId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deletePosition-error', [])));
    }

    // UPDATE => PUT: update the position on the server
    updatePosition(position: Position): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_POSITION_URL + '/update', position, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updatePosition-error', [])));
    }

    // CREATE =>  POST: add a new position to the server
    createPosition(position: Position): Observable<Position> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<Position>(API_POSITION_URL + '/create', position, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createPosition-error', [])));
    }

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    findPositions(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_POSITIONS_URL + '/findPositions', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('findPositions-error', [])));
    }




 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.

            let message = '';

            switch (operation) {
                case 'getPositions-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getPosition-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deletePosition-error': {
                    message = this.translate.instant('POSITION_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updatePosition-error': {
                    message = this.translate.instant('POSITION_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createPosition-error': {
                    message = this.translate.instant('POSITION_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findPositions-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            }

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
