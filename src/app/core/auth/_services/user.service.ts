import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { Surface } from '../_models/surface.model';
import { Role } from '../_models/role.model';
import { catchError, map, tap } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { LayoutUtilsService, MessageType } from '../../_base/crud';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { Team } from '..';

const API_USERS_URL = environment.apiEndpoint +'/api/users';
const API_USER_URL = environment.apiEndpoint +'/api/user';
const API_PERMISSION_URL = environment.apiEndpoint +'/api/permissions';
const API_SURFACES_URL = environment.apiEndpoint +'/api/surfaces';
const API_ROLES_URL = environment.apiEndpoint +'/api/roles';
const API_ROLE_URL = environment.apiEndpoint +'/api/role';

@Injectable()
export class UserService {
    constructor(
        private http: HttpClient,
        private translate: TranslateService, 
        private layoutUtilsService: LayoutUtilsService
        ) {}


    getAllUsers(): Observable<User[]> {
        return this.http.get<User[]>(API_USERS_URL)
            .pipe(catchError(this.handleError('getUsers-error', [])));
    }


    getAllPlayers(): Observable<User[]> {
        return this.http.get<User[]>(API_USERS_URL + '/players')
            .pipe(catchError(this.handleError('getPlayers-error', [])));
    }

    getAllArbitrators(): Observable<User[]> {
        return this.http.get<User[]>(API_USERS_URL + '/arbitrators')
            .pipe(catchError(this.handleError('getArbitrators-error', [])));
    }

    getUserById(userId: number): Observable<User> {
        return this.http.get<User>(API_USER_URL + `/detail/${userId}`)
            .pipe(catchError(this.handleError('getUser-error', [])));
	}


    // DELETE => delete the user from the server
	deleteUser(userId: number) {
		const url = `${API_USER_URL}/${userId}`;
        return this.http.delete(url)
            .pipe(catchError(this.handleError('deleteUser-error', [])));
    }

    // UPDATE => PUT: update the user on the server
	updateUser(_user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_USER_URL, _user, { headers: httpHeaders }).pipe(
            catchError(this.handleError('updateUser-error', []))
            );
	}

    // CREATE =>  POST: add a new user to the server
	createUser(user: User): Observable<User> {
    	const httpHeaders = new HttpHeaders();
     httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<User>(API_USER_URL+'/create', user, { headers: httpHeaders }).pipe(catchError(this.handleError('createUser-error', [])));
	}

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
	// items => filtered/sorted result
	findUsers(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_USERS_URL + '/findUsers', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('getUsers-error', [])));
    }

    // Permission
    getAllPermissions(): Observable<Permission[]> {
        return this.http.get<Permission[]>(API_PERMISSION_URL)
            .pipe(catchError(this.handleError('getPermissions-error', [])));
    }

    getRolePermissions(roleId: number): Observable<Permission[]> {
        return this.http.get<Permission[]>(API_PERMISSION_URL + '/getRolePermission?=' + roleId)
            .pipe(catchError(this.handleError('getRolePermissions-error', [])));
    }

    getUserPermissions(userId: number): Observable<Permission[]> {
        console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        console.log("@@@@@@@@@@@@@ getUserPermissions @@@@@@@@@@@@@@");
        return this.http.get<Permission[]>(API_PERMISSION_URL + '/getUserPermission?=' + userId)
            .pipe(catchError(this.handleError('getUserPermissions-error', [])));
    }

    // Roles
    getAllRoles(): Observable<Role[]> {
        return this.http.get<Role[]>(API_ROLES_URL)
            .pipe(catchError(this.handleError('getRoles-error', [])));
    }

    getRoleById(roleId: number): Observable<Role> {
        return this.http.get<Role>(API_ROLES_URL + `/${roleId}`)
            .pipe(catchError(this.handleError('getRole-error', [])));
    }

    // CREATE =>  POST: add a new role to the server
	createRole(role: Role): Observable<Role> {
		// Note: Add headers if needed (tokens/bearer)
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<Role>(API_ROLE_URL, role, { headers: httpHeaders })
            .pipe(catchError(this.handleError('createRole-error', [])));
	}

    // UPDATE => PUT: update the role on the server
	updateRole(role: Role): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_ROLE_URL, role, { headers: httpHeaders })
            .pipe(catchError(this.handleError('updateRole-error', [])));
	}

	// DELETE => delete the role from the server
	deleteRole(roleId: number): Observable<Role> {
		const url = `${API_ROLES_URL}/${roleId}`;
        return this.http.delete<Role>(url)
            .pipe(catchError(this.handleError('deleteRole-error', [])));
	}

    // Check Role Before deletion
    isRoleAssignedToUsers(roleId: number): Observable<boolean> {
        return this.http.get<boolean>(API_ROLES_URL + '/checkIsRollAssignedToUser?roleId=' + roleId)
            .pipe(catchError(this.handleError('isRoleAssigned-error', [])));
    }

    findRoles(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        // This code imitates server calls
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<QueryResultsModel>(API_ROLES_URL + '/findRoles', queryParams, { headers: httpHeaders })
            .pipe(catchError(this.handleError('getRoles-error', [])));
    }





 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            console.log("result", result); // log to console instead
            // console.log("error", error); // log to console instead
            // console.log("error.error", error.error); // log to console instead
            // if (error.error.error.exception)
                // alert("ERR: " + JSON.stringify(error.error.error.exception[0].message));

            // Let the app keep running by returning an empty result.
            let message = '';

            switch (operation) {
                case 'getUsers-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getUser-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteUser-error': {
                    message = this.translate.instant('USER_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateUser-error': {
                    message = this.translate.instant('USER_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createUser-error': {
                    message = this.translate.instant('USER_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findUsers-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getRoles-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'getRole-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                case 'deleteRole-error': {
                    message = this.translate.instant('ROLE_MANAGEMENT.EDIT.DELETE_ERROR_MESSAGE');
                    break;
                }
                case 'updateRole-error': {
                    message = this.translate.instant('ROLE_MANAGEMENT.EDIT.UPDATE_ERROR_MESSAGE');
                    break;
                }
                case 'createRole-error': {
                    message = this.translate.instant('ROLE_MANAGEMENT.EDIT.CREATE_ERROR_MESSAGE');
                    break;
                }
                case 'findRoles-error': {
                    message = this.translate.instant('COMMON.ERRORS.NO_DATA');
                    break;
                }
                default: {
                    message = this.translate.instant('COMMON.ERRORS.COMMON');
                    break;
                }
            }

            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, false);
            
            return of(result);
        };
    }
}
