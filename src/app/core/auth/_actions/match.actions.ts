// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Match } from '../_models/match.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum MatchActionTypes {
    AllMatchesRequested = '[Matches Module] All Matches Requested',
    AllMatchesLoaded = '[Matches API] All Matches Loaded',
    MatchOnServerCreated = '[Edit Match Component] Match On Server Created',
    MatchCreated = '[Edit Match Dialog] Match Created',
    MatchUpdated = '[Edit Match Dialog] Match Updated',
    MatchDeleted = '[Matches List Page] Match Deleted',
    MatchesPageRequested = '[Matches List Page] Matches Page Requested',
    MatchesPageLoaded = '[Matches API] Matches Page Loaded',
    MatchesPageCancelled = '[Matches API] Matches Page Cancelled',
    MatchesPageToggleLoading = '[Matches] Matches Page Toggle Loading',
    MatchesActionToggleLoading = '[Matches] Matches Action Toggle Loading'
}

export class MatchOnServerCreated implements Action {
    readonly type = MatchActionTypes.MatchOnServerCreated;
    constructor(public payload: { match: Match }) { }
}

export class MatchCreated implements Action {
    readonly type = MatchActionTypes.MatchCreated;
    constructor(public payload: { match: Match }) { }
}


export class MatchUpdated implements Action {
    readonly type = MatchActionTypes.MatchUpdated;
    constructor(public payload: {
        partialMatch: Update<Match>,
        match: Match
    }) { }
}

export class MatchDeleted implements Action {
    readonly type = MatchActionTypes.MatchDeleted;
    constructor(public payload: { id: number }) {}
}

export class MatchesPageRequested implements Action {
    readonly type = MatchActionTypes.MatchesPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class MatchesPageLoaded implements Action {
    readonly type = MatchActionTypes.MatchesPageLoaded;
    constructor(public payload: { matches: Match[], totalCount: number, page: QueryParamsModel  }) { }
}


export class MatchesPageCancelled implements Action {
    readonly type = MatchActionTypes.MatchesPageCancelled;
}

export class MatchesPageToggleLoading implements Action {
    readonly type = MatchActionTypes.MatchesPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class MatchesActionToggleLoading implements Action {
    readonly type = MatchActionTypes.MatchesActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type MatchActions = MatchCreated
| MatchUpdated
| MatchDeleted
| MatchOnServerCreated
| MatchesPageLoaded
| MatchesPageCancelled
| MatchesPageToggleLoading
| MatchesPageRequested
| MatchesActionToggleLoading;
