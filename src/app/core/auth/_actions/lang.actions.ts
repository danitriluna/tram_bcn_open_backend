// NGRX
import { Action } from '@ngrx/store';
// Models
import { Lang } from '../_models/lang.model';

export enum LangActionTypes {
    AllLangsRequested = '[Init] All Langs Requested',
    AllLangsLoaded = '[Init] All Langs Loaded'
}

export class AllLangsRequested implements Action {
    readonly type = LangActionTypes.AllLangsRequested;
}

export class AllLangsLoaded implements Action {
    readonly type = LangActionTypes.AllLangsLoaded;
    constructor(public payload: { langs: Lang[] }) { }
}

export type LangActions = AllLangsRequested | AllLangsLoaded;
