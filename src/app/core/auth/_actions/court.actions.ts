// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Court } from '../_models/court.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum CourtActionTypes {
    AllCourtsRequested = '[Courts Module] All Courts Requested',
    AllCourtsLoaded = '[Courts API] All Courts Loaded',
    CourtOnServerCreated = '[Edit Court Component] Court On Server Created',
    CourtCreated = '[Edit Court Dialog] Court Created',
    CourtUpdated = '[Edit Court Dialog] Court Updated',
    CourtDeleted = '[Courts List Page] Court Deleted',
    CourtsPageRequested = '[Courts List Page] Courts Page Requested',
    CourtsPageLoaded = '[Courts API] Courts Page Loaded',
    CourtsPageCancelled = '[Courts API] Courts Page Cancelled',
    CourtsPageToggleLoading = '[Courts] Courts Page Toggle Loading',
    CourtsActionToggleLoading = '[Courts] Courts Action Toggle Loading'
}

export class CourtOnServerCreated implements Action {
    readonly type = CourtActionTypes.CourtOnServerCreated;
    constructor(public payload: { court: Court }) { }
}

export class CourtCreated implements Action {
    readonly type = CourtActionTypes.CourtCreated;
    constructor(public payload: { court: Court }) { }
}


export class CourtUpdated implements Action {
    readonly type = CourtActionTypes.CourtUpdated;
    constructor(public payload: {
        partialCourt: Update<Court>,
        court: Court
    }) { }
}

export class CourtDeleted implements Action {
    readonly type = CourtActionTypes.CourtDeleted;
    constructor(public payload: { id: number }) {}
}

export class CourtsPageRequested implements Action {
    readonly type = CourtActionTypes.CourtsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class CourtsPageLoaded implements Action {
    readonly type = CourtActionTypes.CourtsPageLoaded;
    constructor(public payload: { courts: Court[], totalCount: number, page: QueryParamsModel  }) { }
}


export class CourtsPageCancelled implements Action {
    readonly type = CourtActionTypes.CourtsPageCancelled;
}

export class CourtsPageToggleLoading implements Action {
    readonly type = CourtActionTypes.CourtsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class CourtsActionToggleLoading implements Action {
    readonly type = CourtActionTypes.CourtsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type CourtActions = CourtCreated
| CourtUpdated
| CourtDeleted
| CourtOnServerCreated
| CourtsPageLoaded
| CourtsPageCancelled
| CourtsPageToggleLoading
| CourtsPageRequested
| CourtsActionToggleLoading;
