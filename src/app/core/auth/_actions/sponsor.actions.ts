// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Sponsor } from '../_models/sponsor.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum SponsorActionTypes {
    AllSponsorsRequested = '[Sponsors Module] All Sponsors Requested',
    AllSponsorsLoaded = '[Sponsors API] All Sponsors Loaded',
    SponsorOnServerCreated = '[Edit Sponsor Component] Sponsor On Server Created',
    SponsorCreated = '[Edit Sponsor Dialog] Sponsor Created',
    SponsorUpdated = '[Edit Sponsor Dialog] Sponsor Updated',
    SponsorDeleted = '[Sponsors List Page] Sponsor Deleted',
    SponsorsPageRequested = '[Sponsors List Page] Sponsors Page Requested',
    SponsorsPageLoaded = '[Sponsors API] Sponsors Page Loaded',
    SponsorsPageCancelled = '[Sponsors API] Sponsors Page Cancelled',
    SponsorsPageToggleLoading = '[Sponsors] Sponsors Page Toggle Loading',
    SponsorsActionToggleLoading = '[Sponsors] Sponsors Action Toggle Loading'
}

export class SponsorOnServerCreated implements Action {
    readonly type = SponsorActionTypes.SponsorOnServerCreated;
    constructor(public payload: { sponsor: Sponsor }) { }
}

export class SponsorCreated implements Action {
    readonly type = SponsorActionTypes.SponsorCreated;
    constructor(public payload: { sponsor: Sponsor }) { }
}


export class SponsorUpdated implements Action {
    readonly type = SponsorActionTypes.SponsorUpdated;
    constructor(public payload: {
        partialSponsor: Update<Sponsor>,
        sponsor: Sponsor
    }) { }
}

export class SponsorDeleted implements Action {
    readonly type = SponsorActionTypes.SponsorDeleted;
    constructor(public payload: { id: number }) {}
}

export class SponsorsPageRequested implements Action {
    readonly type = SponsorActionTypes.SponsorsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class SponsorsPageLoaded implements Action {
    readonly type = SponsorActionTypes.SponsorsPageLoaded;
    constructor(public payload: { sponsors: Sponsor[], totalCount: number, page: QueryParamsModel  }) { }
}


export class SponsorsPageCancelled implements Action {
    readonly type = SponsorActionTypes.SponsorsPageCancelled;
}

export class SponsorsPageToggleLoading implements Action {
    readonly type = SponsorActionTypes.SponsorsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class SponsorsActionToggleLoading implements Action {
    readonly type = SponsorActionTypes.SponsorsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type SponsorActions = SponsorCreated
| SponsorUpdated
| SponsorDeleted
| SponsorOnServerCreated
| SponsorsPageLoaded
| SponsorsPageCancelled
| SponsorsPageToggleLoading
| SponsorsPageRequested
| SponsorsActionToggleLoading;
