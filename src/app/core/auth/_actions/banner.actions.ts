// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Banner } from '../_models/banner.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum BannerActionTypes {
    AllBannersRequested = '[Banners Module] All Banners Requested',
    AllBannersLoaded = '[Banners API] All Banners Loaded',
    BannerOnServerCreated = '[Edit Banner Component] Banner On Server Created',
    BannerCreated = '[Edit Banner Dialog] Banner Created',
    BannerUpdated = '[Edit Banner Dialog] Banner Updated',
    BannerDeleted = '[Banners List Page] Banner Deleted',
    BannersPageRequested = '[Banners List Page] Banners Page Requested',
    BannersPageLoaded = '[Banners API] Banners Page Loaded',
    BannersPageCancelled = '[Banners API] Banners Page Cancelled',
    BannersPageToggleLoading = '[Banners] Banners Page Toggle Loading',
    BannersActionToggleLoading = '[Banners] Banners Action Toggle Loading'
}

export class BannerOnServerCreated implements Action {
    readonly type = BannerActionTypes.BannerOnServerCreated;
    constructor(public payload: { banner: Banner }) { }
}

export class BannerCreated implements Action {
    readonly type = BannerActionTypes.BannerCreated;
    constructor(public payload: { banner: Banner }) { }
}


export class BannerUpdated implements Action {
    readonly type = BannerActionTypes.BannerUpdated;
    constructor(public payload: {
        partialBanner: Update<Banner>,
        banner: Banner
    }) { }
}

export class BannerDeleted implements Action {
    readonly type = BannerActionTypes.BannerDeleted;
    constructor(public payload: { id: number }) {}
}

export class BannersPageRequested implements Action {
    readonly type = BannerActionTypes.BannersPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class BannersPageLoaded implements Action {
    readonly type = BannerActionTypes.BannersPageLoaded;
    constructor(public payload: { banners: Banner[], totalCount: number, page: QueryParamsModel  }) { }
}


export class BannersPageCancelled implements Action {
    readonly type = BannerActionTypes.BannersPageCancelled;
}

export class BannersPageToggleLoading implements Action {
    readonly type = BannerActionTypes.BannersPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class BannersActionToggleLoading implements Action {
    readonly type = BannerActionTypes.BannersActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type BannerActions = BannerCreated
| BannerUpdated
| BannerDeleted
| BannerOnServerCreated
| BannersPageLoaded
| BannersPageCancelled
| BannersPageToggleLoading
| BannersPageRequested
| BannersActionToggleLoading;
