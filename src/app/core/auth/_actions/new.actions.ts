// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { New } from '../_models/new.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum NewActionTypes {
    AllNewsRequested = '[News Module] All News Requested',
    AllNewsLoaded = '[News API] All News Loaded',
    NewOnServerCreated = '[Edit New Component] New On Server Created',
    NewCreated = '[Edit New Dialog] New Created',
    NewUpdated = '[Edit New Dialog] New Updated',
    NewDeleted = '[News List Page] New Deleted',
    NewsPageRequested = '[News List Page] News Page Requested',
    NewsPageLoaded = '[News API] News Page Loaded',
    NewsPageCancelled = '[News API] News Page Cancelled',
    NewsPageToggleLoading = '[News] News Page Toggle Loading',
    NewsActionToggleLoading = '[News] News Action Toggle Loading'
}

export class NewOnServerCreated implements Action {
    readonly type = NewActionTypes.NewOnServerCreated;
    constructor(public payload: { new: New }) { }
}

export class NewCreated implements Action {
    readonly type = NewActionTypes.NewCreated;
    constructor(public payload: { new: New }) { }
}


export class NewUpdated implements Action {
    readonly type = NewActionTypes.NewUpdated;
    constructor(public payload: {
        partialNew: Update<New>,
        new: New
    }) { }
}

export class NewDeleted implements Action {
    readonly type = NewActionTypes.NewDeleted;
    constructor(public payload: { id: number }) {}
}

export class NewsPageRequested implements Action {
    readonly type = NewActionTypes.NewsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class NewsPageLoaded implements Action {
    readonly type = NewActionTypes.NewsPageLoaded;
    constructor(public payload: { news: New[], totalCount: number, page: QueryParamsModel  }) { }
}


export class NewsPageCancelled implements Action {
    readonly type = NewActionTypes.NewsPageCancelled;
}

export class NewsPageToggleLoading implements Action {
    readonly type = NewActionTypes.NewsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class NewsActionToggleLoading implements Action {
    readonly type = NewActionTypes.NewsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type NewActions = NewCreated
| NewUpdated
| NewDeleted
| NewOnServerCreated
| NewsPageLoaded
| NewsPageCancelled
| NewsPageToggleLoading
| NewsPageRequested
| NewsActionToggleLoading;
