// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Country } from '../_models/country.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum CountryActionTypes {
    AllCountriesRequested = '[Countries Module] All Countries Requested',
    AllCountriesLoaded = '[Countries API] All Countries Loaded',
    CountryOnServerCreated = '[Edit Country Component] Country On Server Created',
    CountryCreated = '[Edit Country Dialog] Country Created',
    CountryUpdated = '[Edit Country Dialog] Country Updated',
    CountryDeleted = '[Countries List Page] Country Deleted',
    CountriesPageRequested = '[Countries List Page] Countries Page Requested',
    CountriesPageLoaded = '[Countries API] Countries Page Loaded',
    CountriesPageCancelled = '[Countries API] Countries Page Cancelled',
    CountriesPageToggleLoading = '[Countries] Countries Page Toggle Loading',
    CountriesActionToggleLoading = '[Countries] Countries Action Toggle Loading'
}

export class CountryOnServerCreated implements Action {
    readonly type = CountryActionTypes.CountryOnServerCreated;
    constructor(public payload: { country: Country }) { }
}

export class CountryCreated implements Action {
    readonly type = CountryActionTypes.CountryCreated;
    constructor(public payload: { country: Country }) { }
}


export class CountryUpdated implements Action {
    readonly type = CountryActionTypes.CountryUpdated;
    constructor(public payload: {
        partialCountry: Update<Country>,
        country: Country
    }) { }
}

export class CountryDeleted implements Action {
    readonly type = CountryActionTypes.CountryDeleted;
    constructor(public payload: { id: number }) {}
}

export class CountriesPageRequested implements Action {
    readonly type = CountryActionTypes.CountriesPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class CountriesPageLoaded implements Action {
    readonly type = CountryActionTypes.CountriesPageLoaded;
    constructor(public payload: { countries: Country[], totalCount: number, page: QueryParamsModel  }) { }
}


export class CountriesPageCancelled implements Action {
    readonly type = CountryActionTypes.CountriesPageCancelled;
}

export class CountriesPageToggleLoading implements Action {
    readonly type = CountryActionTypes.CountriesPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class CountriesActionToggleLoading implements Action {
    readonly type = CountryActionTypes.CountriesActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type CountryActions = CountryCreated
| CountryUpdated
| CountryDeleted
| CountryOnServerCreated
| CountriesPageLoaded
| CountriesPageCancelled
| CountriesPageToggleLoading
| CountriesPageRequested
| CountriesActionToggleLoading;
