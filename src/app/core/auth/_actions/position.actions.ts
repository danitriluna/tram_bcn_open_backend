// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Position } from '../_models/position.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum PositionActionTypes {
    AllPositionsRequested = '[Positions Module] All Positions Requested',
    AllPositionsLoaded = '[Positions API] All Positions Loaded',
    PositionOnServerCreated = '[Edit Position Component] Position On Server Created',
    PositionCreated = '[Edit Position Dialog] Position Created',
    PositionUpdated = '[Edit Position Dialog] Position Updated',
    PositionDeleted = '[Positions List Page] Position Deleted',
    PositionsPageRequested = '[Positions List Page] Positions Page Requested',
    PositionsPageLoaded = '[Positions API] Positions Page Loaded',
    PositionsPageCancelled = '[Positions API] Positions Page Cancelled',
    PositionsPageToggleLoading = '[Positions] Positions Page Toggle Loading',
    PositionsActionToggleLoading = '[Positions] Positions Action Toggle Loading'
}

export class PositionOnServerCreated implements Action {
    readonly type = PositionActionTypes.PositionOnServerCreated;
    constructor(public payload: { position: Position }) { }
}

export class PositionCreated implements Action {
    readonly type = PositionActionTypes.PositionCreated;
    constructor(public payload: { position: Position }) { }
}


export class PositionUpdated implements Action {
    readonly type = PositionActionTypes.PositionUpdated;
    constructor(public payload: {
        partialPosition: Update<Position>,
        position: Position
    }) { }
}

export class PositionDeleted implements Action {
    readonly type = PositionActionTypes.PositionDeleted;
    constructor(public payload: { id: number }) {}
}

export class PositionsPageRequested implements Action {
    readonly type = PositionActionTypes.PositionsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class PositionsPageLoaded implements Action {
    readonly type = PositionActionTypes.PositionsPageLoaded;
    constructor(public payload: { positions: Position[], totalCount: number, page: QueryParamsModel  }) { }
}


export class PositionsPageCancelled implements Action {
    readonly type = PositionActionTypes.PositionsPageCancelled;
}

export class PositionsPageToggleLoading implements Action {
    readonly type = PositionActionTypes.PositionsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class PositionsActionToggleLoading implements Action {
    readonly type = PositionActionTypes.PositionsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export type PositionActions = PositionCreated
| PositionUpdated
| PositionDeleted
| PositionOnServerCreated
| PositionsPageLoaded
| PositionsPageCancelled
| PositionsPageToggleLoading
| PositionsPageRequested
| PositionsActionToggleLoading;
