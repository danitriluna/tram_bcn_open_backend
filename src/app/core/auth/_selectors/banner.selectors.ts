// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { BannersState } from '../_reducers/banner.reducers';
import { each } from 'lodash';
import { Banner } from '../_models/banner.model';


export const selectBannersState = createFeatureSelector<BannersState>('banners');

export const selectBannerById = (bannerId: number) => createSelector(
    selectBannersState,
    bannersState => bannersState.entities[bannerId]
);

export const selectBannersPageLoading = createSelector(
    selectBannersState,
    bannersState => {
        return bannersState.listLoading;
    }
);

export const selectBannersActionLoading = createSelector(
    selectBannersState,
    bannersState => bannersState.actionsloading
);

export const selectLastCreatedBannerId = createSelector(
    selectBannersState,
    bannersState => bannersState.lastCreatedBannerId
);

export const selectBannersPageLastQuery = createSelector(
    selectBannersState,
    bannersState => bannersState.lastQuery
);

export const selectBannersInStore = createSelector(
    selectBannersState,
    bannersState => {
        const items: Banner[] = [];
        each(bannersState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Banner[] = httpExtension.sortArray(items, bannersState.lastQuery.sortField, bannersState.lastQuery.sortOrder);
        return new QueryResultsModel(result, bannersState.totalCount, '');
    }
);

export const selectBannersShowInitWaitingMessage = createSelector(
    selectBannersState,
    bannersState => bannersState.showInitWaitingMessage
);

export const selectHasBannersInStore = createSelector(
    selectBannersState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
