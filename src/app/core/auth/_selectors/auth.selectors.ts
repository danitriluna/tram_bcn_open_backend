// NGRX
import { createSelector } from '@ngrx/store';
// Lodash
import { each, find, some } from 'lodash';
// Selectors
import { selectAllRoles } from './role.selectors';
import { selectAllPermissions } from './permission.selectors';
import { selectAllLangs } from './lang.selectors';
// Models
import { Lang } from '../_models/lang.model';
import { Role } from '../_models/role.model';
import { Permission } from '../_models/permission.model';

export const selectAuthState = state => state.auth;

export const isLoggedIn = createSelector(
    selectAuthState,
    auth => auth.loggedIn
);

export const isLoggedOut = createSelector(
    isLoggedIn,
    loggedIn => !loggedIn
);


export const currentAuthToken = createSelector(
    selectAuthState,
    auth => auth.authToken
);

export const isUserLoaded = createSelector(
    selectAuthState,
    auth => auth.isUserLoaded
);

export const currentUser = createSelector(
    selectAuthState,
    auth => auth.user
);

export const currentUserRoleIds = createSelector(
    currentUser,
    user => {
        if (!user) {
            return [];
        }

        return user.roles;
    }
);

export const currentLangs = createSelector(
    selectAllLangs,
    (l: Lang[]) => {
        if (!l) {
            return [];
        }

        return l;
    }
);

export const currentUserPermissionsIds = createSelector(
    currentUserRoleIds,
    selectAllRoles,
    (userRoleIds: number[], allRoles: Role[]) => {
        const result = getPermissionsIdsFrom(userRoleIds, allRoles);
        return result;
    }
);

export const checkHasUserPermission = (permissionId: number) => createSelector(
    currentUserPermissionsIds,
    (ids: number[]) => {
        console.log("***permissionId", permissionId);
        console.log("***ids", ids);
        return ids.some(id => id === permissionId);
    }
);

export const currentUserPermissionsNames = createSelector(
    currentUserRoleIds,
    selectAllRoles,
    selectAllPermissions,
    (userRoleIds: number[], allRoles: Role[], allPermissions: Permission[]) => {
        const result = getPermissionsNamesFrom(userRoleIds, allRoles, allPermissions);
        return result;
    }
);
export const checkHasUserPermissionByName = (permissionName: string) => createSelector(
    currentUserPermissionsNames,
    (names: string[]) => {
        console.log("***permissionId", permissionName);
        console.log("***names", names);
        return names.some(name => name === permissionName);
    }
);

export const currentUserPermissions = createSelector(
    currentUserPermissionsIds,
    selectAllPermissions,
    (permissionIds: number[], allPermissions: Permission[]) => {
        const result: Permission[] = [];
        each(permissionIds, id => {
            const userPermission = find(allPermissions, elem => elem.id === id);
            if (userPermission) {
                result.push(userPermission);
            }
        });
        console.log("-------allPermissions", allPermissions);
        return result;
    }
);

function getPermissionsIdsFrom(userRolesIds: number[] = [], allRoles: Role[] = []): number[] {
    const userRoles: Role[] = [];
    each(userRolesIds, (_id: number) => {
       const userRole = find(allRoles, (_role: Role) => _role.id === _id);
       if (userRole) {
           userRoles.push(userRole);
       }
    });

    const result: number[] = [];
    each(userRoles, (_role: Role) => {
        each(_role.permissions, id => {
            if (!some(result, _id => _id === id)) {
                result.push(id);
            }
        });
    });
    console.log("-------userRolesIds", userRolesIds);
    console.log("-------allRoles", allRoles);
    console.log("-------getPermissionsIdsFrom", result);
    return result;
}

function getPermissionsNamesFrom(userRolesIds: number[] = [], allRoles: Role[] = [], allPermissions: Permission[] = []): string[] {
    const userRoles: Role[] = [];
    each(userRolesIds, (_id: number) => {
        const userRole = find(allRoles, (_role: Role) => _role.id === _id);
        if (userRole) {
            userRoles.push(userRole);
        }
    });

    const result: string[] = [];
    each(userRoles, (_role: Role) => {
        each(_role.permissions, id => {
            if (!some(result, _id => _id === id)) {

                console.log("@@@id", id);
                const userPermission = find(allPermissions, elem => elem.id === id);
                if (userPermission) {
                    result.push(userPermission.name);
                    console.log("@@@userPermission.name", userPermission.name);
                }
            }

        });
    });
    console.log("-------userRolesIds", userRolesIds);
    console.log("-------allRoles", allRoles);
    console.log("-----------getPermissionsNamesFrom", result);
    return result;
}

