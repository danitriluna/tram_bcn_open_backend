// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { MatchesState } from '../_reducers/match.reducers';
import { each } from 'lodash';
import { Match } from '../_models/match.model';


export const selectMatchesState = createFeatureSelector<MatchesState>('matches');

export const selectMatchById = (matchId: number) => createSelector(
    selectMatchesState,
    matchesState => matchesState.entities[matchId]
);

export const selectMatchesPageLoading = createSelector(
    selectMatchesState,
    matchesState => {
        return matchesState.listLoading;
    }
);

export const selectMatchesActionLoading = createSelector(
    selectMatchesState,
    matchesState => matchesState.actionsloading
);

export const selectLastCreatedMatchId = createSelector(
    selectMatchesState,
    matchesState => matchesState.lastCreatedMatchId
);

export const selectMatchesPageLastQuery = createSelector(
    selectMatchesState,
    matchesState => matchesState.lastQuery
);

export const selectMatchesInStore = createSelector(
    selectMatchesState,
    matchesState => {
        const items: Match[] = [];
        each(matchesState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Match[] = httpExtension.sortArray(items, matchesState.lastQuery.sortField, matchesState.lastQuery.sortOrder);
        return new QueryResultsModel(result, matchesState.totalCount, '');
    }
);

export const selectMatchesShowInitWaitingMessage = createSelector(
    selectMatchesState,
    matchesState => matchesState.showInitWaitingMessage
);

export const selectHasMatchesInStore = createSelector(
    selectMatchesState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
