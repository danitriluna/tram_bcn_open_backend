// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { CountriesState } from '../_reducers/country.reducers';
import { each } from 'lodash';
import { Country } from '../_models/country.model';


export const selectCountriesState = createFeatureSelector<CountriesState>('countries');

export const selectCountryById = (countryId: number) => createSelector(
    selectCountriesState,
    countriesState => countriesState.entities[countryId]
);

export const selectCountriesPageLoading = createSelector(
    selectCountriesState,
    countriesState => {
        return countriesState.listLoading;
    }
);

export const selectCountriesActionLoading = createSelector(
    selectCountriesState,
    countriesState => countriesState.actionsloading
);

export const selectLastCreatedCountryId = createSelector(
    selectCountriesState,
    countriesState => countriesState.lastCreatedCountryId
);

export const selectCountriesPageLastQuery = createSelector(
    selectCountriesState,
    countriesState => countriesState.lastQuery
);

export const selectCountriesInStore = createSelector(
    selectCountriesState,
    countriesState => {
        const items: Country[] = [];
        each(countriesState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Country[] = httpExtension.sortArray(items, countriesState.lastQuery.sortField, countriesState.lastQuery.sortOrder);
        return new QueryResultsModel(result, countriesState.totalCount, '');
    }
);

export const selectCountriesShowInitWaitingMessage = createSelector(
    selectCountriesState,
    countriesState => countriesState.showInitWaitingMessage
);

export const selectHasCountriesInStore = createSelector(
    selectCountriesState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
