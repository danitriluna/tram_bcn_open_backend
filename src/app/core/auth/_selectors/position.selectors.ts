// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { PositionsState } from '../_reducers/position.reducers';
import { each } from 'lodash';
import { Position } from '../_models/position.model';


export const selectPositionsState = createFeatureSelector<PositionsState>('positions');

export const selectPositionById = (positionId: number) => createSelector(
    selectPositionsState,
    positionsState => positionsState.entities[positionId]
);

export const selectPositionsPageLoading = createSelector(
    selectPositionsState,
    positionsState => {
        return positionsState.listLoading;
    }
);

export const selectPositionsActionLoading = createSelector(
    selectPositionsState,
    positionsState => positionsState.actionsloading
);

export const selectLastCreatedPositionId = createSelector(
    selectPositionsState,
    positionsState => positionsState.lastCreatedPositionId
);

export const selectPositionsPageLastQuery = createSelector(
    selectPositionsState,
    positionsState => positionsState.lastQuery
);

export const selectPositionsInStore = createSelector(
    selectPositionsState,
    positionsState => {
        const items: Position[] = [];
        each(positionsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Position[] = httpExtension.sortArray(items, positionsState.lastQuery.sortField, positionsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, positionsState.totalCount, '');
    }
);

export const selectPositionsShowInitWaitingMessage = createSelector(
    selectPositionsState,
    positionsState => positionsState.showInitWaitingMessage
);

export const selectHasPositionsInStore = createSelector(
    selectPositionsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
