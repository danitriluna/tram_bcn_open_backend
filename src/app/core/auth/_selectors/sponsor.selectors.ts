// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { SponsorsState } from '../_reducers/sponsor.reducers';
import { each } from 'lodash';
import { Sponsor } from '../_models/sponsor.model';


export const selectSponsorsState = createFeatureSelector<SponsorsState>('sponsors');

export const selectSponsorById = (sponsorId: number) => createSelector(
    selectSponsorsState,
    sponsorsState => sponsorsState.entities[sponsorId]
);

export const selectSponsorsPageLoading = createSelector(
    selectSponsorsState,
    sponsorsState => {
        return sponsorsState.listLoading;
    }
);

export const selectSponsorsActionLoading = createSelector(
    selectSponsorsState,
    sponsorsState => sponsorsState.actionsloading
);

export const selectLastCreatedSponsorId = createSelector(
    selectSponsorsState,
    sponsorsState => sponsorsState.lastCreatedSponsorId
);

export const selectSponsorsPageLastQuery = createSelector(
    selectSponsorsState,
    sponsorsState => sponsorsState.lastQuery
);

export const selectSponsorsInStore = createSelector(
    selectSponsorsState,
    sponsorsState => {
        const items: Sponsor[] = [];
        each(sponsorsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Sponsor[] = httpExtension.sortArray(items, sponsorsState.lastQuery.sortField, sponsorsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, sponsorsState.totalCount, '');
    }
);

export const selectSponsorsShowInitWaitingMessage = createSelector(
    selectSponsorsState,
    sponsorsState => sponsorsState.showInitWaitingMessage
);

export const selectHasSponsorsInStore = createSelector(
    selectSponsorsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
