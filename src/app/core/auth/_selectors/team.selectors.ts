// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { TeamsState } from '../_reducers/team.reducers';
import { each } from 'lodash';
import { Team } from '../_models/team.model';


export const selectTeamsState = createFeatureSelector<TeamsState>('teams');

export const selectTeamById = (teamId: number) => createSelector(
    selectTeamsState,
    teamsState => teamsState.entities[teamId]
);

export const selectTeamsPageLoading = createSelector(
    selectTeamsState,
    teamsState => {
        return teamsState.listLoading;
    }
);

export const selectTeamsActionLoading = createSelector(
    selectTeamsState,
    teamsState => teamsState.actionsloading
);

export const selectLastCreatedTeamId = createSelector(
    selectTeamsState,
    teamsState => teamsState.lastCreatedTeamId
);

export const selectTeamsPageLastQuery = createSelector(
    selectTeamsState,
    teamsState => teamsState.lastQuery
);

export const selectTeamsInStore = createSelector(
    selectTeamsState,
    teamsState => {
        const items: Team[] = [];
        each(teamsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Team[] = httpExtension.sortArray(items, teamsState.lastQuery.sortField, teamsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, teamsState.totalCount, '');
    }
);

export const selectTeamsShowInitWaitingMessage = createSelector(
    selectTeamsState,
    teamsState => teamsState.showInitWaitingMessage
);

export const selectHasTeamsInStore = createSelector(
    selectTeamsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
