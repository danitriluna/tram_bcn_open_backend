// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// State
import { LangsState } from '../_reducers/lang.reducers';
import * as fromLangs from '../_reducers/lang.reducers';

export const selectLangsState = createFeatureSelector<LangsState>('langs');

export const selectLangById = (langId: number) => createSelector(
    selectLangsState,
    ps => ps.entities[langId]
);

export const selectAllLangs = createSelector(
    selectLangsState,
    fromLangs.selectAll
);

export const selectAllLangsIds = createSelector(
    selectLangsState,
    fromLangs.selectIds
);

export const allLangsLoaded = createSelector(
    selectLangsState,
    ps => ps._isAllLangsLoaded
);
