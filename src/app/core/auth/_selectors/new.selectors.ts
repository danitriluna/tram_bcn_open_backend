// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { NewsState } from '../_reducers/new.reducers';
import { each } from 'lodash';
import { New } from '../_models/new.model';


export const selectNewsState = createFeatureSelector<NewsState>('news');

export const selectNewById = (newId: number) => createSelector(
    selectNewsState,
    newsState => newsState.entities[newId]
);

export const selectNewsPageLoading = createSelector(
    selectNewsState,
    newsState => {
        return newsState.listLoading;
    }
);

export const selectNewsActionLoading = createSelector(
    selectNewsState,
    newsState => newsState.actionsloading
);

export const selectLastCreatedNewId = createSelector(
    selectNewsState,
    newsState => newsState.lastCreatedNewId
);

export const selectNewsPageLastQuery = createSelector(
    selectNewsState,
    newsState => newsState.lastQuery
);

export const selectNewsInStore = createSelector(
    selectNewsState,
    newsState => {
        const items: New[] = [];
        each(newsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: New[] = httpExtension.sortArray(items, newsState.lastQuery.sortField, newsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, newsState.totalCount, '');
    }
);

export const selectNewsShowInitWaitingMessage = createSelector(
    selectNewsState,
    newsState => newsState.showInitWaitingMessage
);

export const selectHasNewsInStore = createSelector(
    selectNewsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
