// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { CourtsState } from '../_reducers/court.reducers';
import { each } from 'lodash';
import { Court } from '../_models/court.model';


export const selectCourtsState = createFeatureSelector<CourtsState>('courts');

export const selectCourtById = (courtId: number) => createSelector(
    selectCourtsState,
    courtsState => courtsState.entities[courtId]
);

export const selectCourtsPageLoading = createSelector(
    selectCourtsState,
    courtsState => {
        return courtsState.listLoading;
    }
);

export const selectCourtsActionLoading = createSelector(
    selectCourtsState,
    courtsState => courtsState.actionsloading
);

export const selectLastCreatedCourtId = createSelector(
    selectCourtsState,
    courtsState => courtsState.lastCreatedCourtId
);

export const selectCourtsPageLastQuery = createSelector(
    selectCourtsState,
    courtsState => courtsState.lastQuery
);

export const selectCourtsInStore = createSelector(
    selectCourtsState,
    courtsState => {
        const items: Court[] = [];
        each(courtsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Court[] = httpExtension.sortArray(items, courtsState.lastQuery.sortField, courtsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, courtsState.totalCount, '');
    }
);

export const selectCourtsShowInitWaitingMessage = createSelector(
    selectCourtsState,
    courtsState => courtsState.showInitWaitingMessage
);

export const selectHasCourtsInStore = createSelector(
    selectCourtsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
