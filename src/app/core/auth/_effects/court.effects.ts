// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { MatchService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    CourtActionTypes,
    CourtsPageRequested,
    CourtsPageLoaded,
    CourtCreated,
    CourtDeleted,
    CourtUpdated,
    CourtOnServerCreated,
    CourtsActionToggleLoading,
    CourtsPageToggleLoading
} from '../_actions/court.actions';

@Injectable()
export class CourtEffects {
    showPageLoadingDistpatcher = new CourtsPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new CourtsPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new CourtsActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new CourtsActionToggleLoading({ isLoading: false });

    @Effect()
    loadCourtsPage$ = this.actions$
        .pipe(
            ofType<CourtsPageRequested>(CourtActionTypes.CourtsPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.matchService.findCourts(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new CourtsPageLoaded({
                    courts: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteCourt$ = this.actions$
        .pipe(
            ofType<CourtDeleted>(CourtActionTypes.CourtDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.deleteCourt(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateCourt$ = this.actions$
        .pipe(
            ofType<CourtUpdated>(CourtActionTypes.CourtUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.updateCourt(payload.court);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createCourt$ = this.actions$
        .pipe(
            ofType<CourtOnServerCreated>(CourtActionTypes.CourtOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.createCourt(payload.court).pipe(
                    tap(res => {
                        this.store.dispatch(new CourtCreated({ court: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private matchService: MatchService, private store: Store<AppState>) { }
}
