// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { CountryService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    CountryActionTypes,
    CountriesPageRequested,
    CountriesPageLoaded,
    CountryCreated,
    CountryDeleted,
    CountryUpdated,
    CountryOnServerCreated,
    CountriesActionToggleLoading,
    CountriesPageToggleLoading
} from '../_actions/country.actions';

@Injectable()
export class CountryEffects {
    showPageLoadingDistpatcher = new CountriesPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new CountriesPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new CountriesActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new CountriesActionToggleLoading({ isLoading: false });

    @Effect()
    loadCountriesPage$ = this.actions$
        .pipe(
            ofType<CountriesPageRequested>(CountryActionTypes.CountriesPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.countryService.findCountries(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new CountriesPageLoaded({
                    countries: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteCountry$ = this.actions$
        .pipe(
            ofType<CountryDeleted>(CountryActionTypes.CountryDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.countryService.deleteCountry(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateCountry$ = this.actions$
        .pipe(
            ofType<CountryUpdated>(CountryActionTypes.CountryUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.countryService.updateCountry(payload.country);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createCountry$ = this.actions$
        .pipe(
            ofType<CountryOnServerCreated>(CountryActionTypes.CountryOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.countryService.createCountry(payload.country).pipe(
                    tap(res => {
                        this.store.dispatch(new CountryCreated({ country: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private countryService: CountryService, private store: Store<AppState>) { }
}
