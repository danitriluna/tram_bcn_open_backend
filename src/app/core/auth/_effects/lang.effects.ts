// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { defer, Observable, of } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
// Services
import { CommonService } from '../_services';
// Actions
import {
    AllLangsLoaded,
    AllLangsRequested,
    LangActionTypes
} from '../_actions/lang.actions';
// Models
import { Lang } from '../_models/lang.model';

@Injectable()
export class LangEffects {
    @Effect()
    loadAllLangs$ = this.actions$
        .pipe(
            ofType<AllLangsRequested>(LangActionTypes.AllLangsRequested),
            mergeMap(() => this.commonService.getAllLangs()),
            map((result: Lang[]) => {
                return new AllLangsLoaded({
                    langs: result
                });
            })
        );

    @Effect()
    init$: Observable<Action> = defer(() => {
        return of(new AllLangsRequested());
    });

    constructor(private actions$: Actions, private commonService: CommonService) { }
}
