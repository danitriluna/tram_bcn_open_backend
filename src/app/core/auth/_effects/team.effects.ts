// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { MatchService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    TeamActionTypes,
    TeamsPageRequested,
    TeamsPageLoaded,
    TeamCreated,
    TeamDeleted,
    TeamUpdated,
    TeamOnServerCreated,
    TeamsActionToggleLoading,
    TeamsPageToggleLoading
} from '../_actions/team.actions';

@Injectable()
export class TeamEffects {
    showPageLoadingDistpatcher = new TeamsPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new TeamsPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new TeamsActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new TeamsActionToggleLoading({ isLoading: false });

    @Effect()
    loadTeamsPage$ = this.actions$
        .pipe(
            ofType<TeamsPageRequested>(TeamActionTypes.TeamsPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.matchService.findTeams(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new TeamsPageLoaded({
                    teams: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteTeam$ = this.actions$
        .pipe(
            ofType<TeamDeleted>(TeamActionTypes.TeamDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.deleteTeam(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateTeam$ = this.actions$
        .pipe(
            ofType<TeamUpdated>(TeamActionTypes.TeamUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.updateTeam(payload.team);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createTeam$ = this.actions$
        .pipe(
            ofType<TeamOnServerCreated>(TeamActionTypes.TeamOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.createTeam(payload.team).pipe(
                    tap(res => {
                        this.store.dispatch(new TeamCreated({ team: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private matchService: MatchService, private store: Store<AppState>) { }
}
