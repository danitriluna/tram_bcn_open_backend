// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { MatchService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    MatchActionTypes,
    MatchesPageRequested,
    MatchesPageLoaded,
    MatchCreated,
    MatchDeleted,
    MatchUpdated,
    MatchOnServerCreated,
    MatchesActionToggleLoading,
    MatchesPageToggleLoading
} from '../_actions/match.actions';

@Injectable()
export class MatchEffects {
    showPageLoadingDistpatcher = new MatchesPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new MatchesPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new MatchesActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new MatchesActionToggleLoading({ isLoading: false });

    @Effect()
    loadMatchesPage$ = this.actions$
        .pipe(
            ofType<MatchesPageRequested>(MatchActionTypes.MatchesPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.matchService.findMatches(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new MatchesPageLoaded({
                    matches: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteMatch$ = this.actions$
        .pipe(
            ofType<MatchDeleted>(MatchActionTypes.MatchDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.deleteMatch(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateMatch$ = this.actions$
        .pipe(
            ofType<MatchUpdated>(MatchActionTypes.MatchUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.updateMatch(payload.match);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createMatch$ = this.actions$
        .pipe(
            ofType<MatchOnServerCreated>(MatchActionTypes.MatchOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.matchService.createMatch(payload.match).pipe(
                    tap(res => {
                        this.store.dispatch(new MatchCreated({ match: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private matchService: MatchService, private store: Store<AppState>) { }
}
