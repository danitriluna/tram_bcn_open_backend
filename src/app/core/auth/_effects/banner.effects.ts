// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { SponsorService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    BannerActionTypes,
    BannersPageRequested,
    BannersPageLoaded,
    BannerCreated,
    BannerDeleted,
    BannerUpdated,
    BannerOnServerCreated,
    BannersActionToggleLoading,
    BannersPageToggleLoading
} from '../_actions/banner.actions';

@Injectable()
export class BannerEffects {
    showPageLoadingDistpatcher = new BannersPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new BannersPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new BannersActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new BannersActionToggleLoading({ isLoading: false });

    @Effect()
    loadBannersPage$ = this.actions$
        .pipe(
            ofType<BannersPageRequested>(BannerActionTypes.BannersPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.sponsorService.findBanners(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new BannersPageLoaded({
                    banners: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteBanner$ = this.actions$
        .pipe(
            ofType<BannerDeleted>(BannerActionTypes.BannerDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.deleteBanner(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateBanner$ = this.actions$
        .pipe(
            ofType<BannerUpdated>(BannerActionTypes.BannerUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.updateBanner(payload.banner);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createBanner$ = this.actions$
        .pipe(
            ofType<BannerOnServerCreated>(BannerActionTypes.BannerOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.createBanner(payload.banner).pipe(
                    tap(res => {
                        this.store.dispatch(new BannerCreated({ banner: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private sponsorService: SponsorService, private store: Store<AppState>) { }
}
