// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { PositionService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    PositionActionTypes,
    PositionsPageRequested,
    PositionsPageLoaded,
    PositionCreated,
    PositionDeleted,
    PositionUpdated,
    PositionOnServerCreated,
    PositionsActionToggleLoading,
    PositionsPageToggleLoading
} from '../_actions/position.actions';

@Injectable()
export class PositionEffects {
    showPageLoadingDistpatcher = new PositionsPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new PositionsPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new PositionsActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new PositionsActionToggleLoading({ isLoading: false });

    @Effect()
    loadPositionsPage$ = this.actions$
        .pipe(
            ofType<PositionsPageRequested>(PositionActionTypes.PositionsPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.positionService.findPositions(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new PositionsPageLoaded({
                    positions: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deletePosition$ = this.actions$
        .pipe(
            ofType<PositionDeleted>(PositionActionTypes.PositionDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.positionService.deletePosition(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updatePosition$ = this.actions$
        .pipe(
            ofType<PositionUpdated>(PositionActionTypes.PositionUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.positionService.updatePosition(payload.position);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createPosition$ = this.actions$
        .pipe(
            ofType<PositionOnServerCreated>(PositionActionTypes.PositionOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.positionService.createPosition(payload.position).pipe(
                    tap(res => {
                        this.store.dispatch(new PositionCreated({ position: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private positionService: PositionService, private store: Store<AppState>) { }
}
