// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { SponsorService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    SponsorActionTypes,
    SponsorsPageRequested,
    SponsorsPageLoaded,
    SponsorCreated,
    SponsorDeleted,
    SponsorUpdated,
    SponsorOnServerCreated,
    SponsorsActionToggleLoading,
    SponsorsPageToggleLoading
} from '../_actions/sponsor.actions';

@Injectable()
export class SponsorEffects {
    showPageLoadingDistpatcher = new SponsorsPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new SponsorsPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new SponsorsActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new SponsorsActionToggleLoading({ isLoading: false });

    @Effect()
    loadSponsorsPage$ = this.actions$
        .pipe(
            ofType<SponsorsPageRequested>(SponsorActionTypes.SponsorsPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.sponsorService.findSponsors(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new SponsorsPageLoaded({
                    sponsors: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteSponsor$ = this.actions$
        .pipe(
            ofType<SponsorDeleted>(SponsorActionTypes.SponsorDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.deleteSponsor(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateSponsor$ = this.actions$
        .pipe(
            ofType<SponsorUpdated>(SponsorActionTypes.SponsorUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.updateSponsor(payload.sponsor);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createSponsor$ = this.actions$
        .pipe(
            ofType<SponsorOnServerCreated>(SponsorActionTypes.SponsorOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.sponsorService.createSponsor(payload.sponsor).pipe(
                    tap(res => {
                        this.store.dispatch(new SponsorCreated({ sponsor: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private sponsorService: SponsorService, private store: Store<AppState>) { }
}
