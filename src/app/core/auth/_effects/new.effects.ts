// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { NewService } from '../_services';
// State
import { AppState } from '../../reducers';
import {
    NewActionTypes,
    NewsPageRequested,
    NewsPageLoaded,
    NewCreated,
    NewDeleted,
    NewUpdated,
    NewOnServerCreated,
    NewsActionToggleLoading,
    NewsPageToggleLoading
} from '../_actions/new.actions';

@Injectable()
export class NewEffects {
    showPageLoadingDistpatcher = new NewsPageToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new NewsPageToggleLoading({ isLoading: false });

    showActionLoadingDistpatcher = new NewsActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new NewsActionToggleLoading({ isLoading: false });

    @Effect()
    loadNewsPage$ = this.actions$
        .pipe(
            ofType<NewsPageRequested>(NewActionTypes.NewsPageRequested),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.newService.findNews(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin(requestToServer, lastQuery);
            }),
            map(response => {
                const result: QueryResultsModel = response[0];
                const lastQuery: QueryParamsModel = response[1];
                return new NewsPageLoaded({
                    news: result.items,
                    totalCount: result.totalCount,
                    page: lastQuery
                });
            }),
        );

    @Effect()
    deleteNew$ = this.actions$
        .pipe(
            ofType<NewDeleted>(NewActionTypes.NewDeleted),
            mergeMap(( { payload } ) => {
                    this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.newService.deleteNew(payload.id);
                }
            ),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    updateNew$ = this.actions$
        .pipe(
            ofType<NewUpdated>(NewActionTypes.NewUpdated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.newService.updateNew(payload.new);
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    @Effect()
    createNew$ = this.actions$
        .pipe(
            ofType<NewOnServerCreated>(NewActionTypes.NewOnServerCreated),
            mergeMap(( { payload } ) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.newService.createNew(payload.new).pipe(
                    tap(res => {
                        this.store.dispatch(new NewCreated({ new: res }));
                    })
                );
            }),
            map(() => {
                return this.hideActionLoadingDistpatcher;
            }),
        );

    constructor(private actions$: Actions, private newService: NewService, private store: Store<AppState>) { }
}
