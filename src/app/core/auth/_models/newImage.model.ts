import { BaseModel } from '../../_base/crud';


export class NewImage extends BaseModel {
    id: number;
    id_news: number;
    lang_info: any;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.id_news = undefined;
        this.lang_info = [];
        this.lang_info_array = [];
    }
}
