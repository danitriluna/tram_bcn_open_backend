import { BaseModel } from '../../_base/crud';


export class User extends BaseModel {
    id: number;
    username: string;
    password: string;
    email: string;
    access_token: string;
    refreshToken: string;
    roles: any[];
    pic: string;
    name: string;
    last_name1: string;
    last_name2: string;
    player_info: any[];
    lang_player_info: any;

    clear(): void {
        this.id = undefined;
        this.username = '';
        this.password = '';
        this.email = '';
        this.roles = [];
        this.name = '';
        this.last_name1 = '';
        this.last_name2 = '';
        this.access_token = 'access-token-' + Math.random();
        this.refreshToken = 'access-token-' + Math.random();
        this.pic = './assets/media/users/default.jpg';
        this.player_info = [];
        this.lang_player_info = [];
    }
}
