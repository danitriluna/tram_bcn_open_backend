import { BaseModel } from '../../_base/crud';
import { Permission } from './permission.model';

export class Role extends BaseModel {
    id: number;
    name: string;
    title: string;
    permissions: any[];
    isCoreRole = false;

    clear(): void {
        this.id = undefined;
        this.name = '';
        this.title = '';
        this.permissions = [];
        this.isCoreRole = false;
	}
}
