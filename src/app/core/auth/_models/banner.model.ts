import { BaseModel } from '../../_base/crud';
import { Sponsor } from './sponsor.model';
import { Position } from './position.model';

export class Banner extends BaseModel {
    id: number;
    sponsor: Sponsor;
    position: Position;
    date_start: string;
    date_end: string;
    lang_info: any;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.sponsor = undefined;
        this.position = undefined;
        this.date_start = '';
        this.date_end = '';
        this.lang_info = [];
        this.lang_info_array = [];
    }
}
