import { BaseModel } from '../../_base/crud';


export class Surface extends BaseModel {
    id: number;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.lang_info_array = [];
    }
}
