import { BaseModel } from '../../_base/crud';
import { User } from './user.model';
import { Category } from './category.model';
import { Court } from './court.model';
import { Team } from './team.model';


export class Match extends BaseModel {
    id: number;
    player1: Team;
    player2: Team;
    arbitrator: User;
    winner: User;
    retired: User;
    category: Category;
    court: Court;
    date: string;
    finalized: number;

    clear(): void {
        this.id = undefined;
        this.player1 = undefined;
        this.player2 = undefined;
        this.arbitrator = undefined;
        this.winner = undefined;
        this.retired = undefined;
        this.category = undefined;
        this.court = undefined;
        this.date = '';
        this.finalized = 0;
    }
}
