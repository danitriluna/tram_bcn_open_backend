import { BaseModel } from '../../_base/crud';


export class Country extends BaseModel {
    id: number;
    flag: string;
    lang_info: any;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.flag = '';
        this.lang_info = [];
        this.lang_info_array = [];
    }
}
