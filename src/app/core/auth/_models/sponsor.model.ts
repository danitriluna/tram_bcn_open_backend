import { BaseModel } from '../../_base/crud';


export class Sponsor extends BaseModel {
    id: number;
    order_position: number;
    langInfo: any;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.order_position = 0;
        this.langInfo = [];
        this.lang_info_array = [];
    }
}
