import { BaseModel } from '../../_base/crud';


export class Lang extends BaseModel {
    id: number;
    name: string;
    key: string;

    clear(): void {
        this.id = undefined;
        this.name = '';
        this.key = '';
    }
}
