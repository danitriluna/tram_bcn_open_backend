import { BaseModel } from '../../_base/crud';


export class Court extends BaseModel {
    id: number;
    map: string;
    coords: string;
    lang_info: any;
    lang_info_array: any;

    clear(): void {
        this.id = undefined;
        this.map = '';
        this.coords = '';
        this.lang_info = [];
        this.lang_info_array = [];
    }
}
