import { BaseModel } from '../../_base/crud';
import { User } from '..';


export class Team extends BaseModel {
    id: number;
    player1: User;
    player2: User;

    clear(): void {
        this.id = undefined;
        this.player1 = undefined;
        this.player2 = undefined;
    }
}
