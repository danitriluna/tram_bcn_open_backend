import { BaseModel } from '../../_base/crud';


export class New extends BaseModel {
    id: number;
    user: any;
    date_publication: string;
    lang_info: any;
    lang_info_array: any;
    video_info: any;
    video_info_array: any;
    img_info: any;
    img_info_array: any;

    clear(): void {
        this.id = undefined;
        this.user = undefined;
        this.date_publication = '';
        this.lang_info = [];
        this.lang_info_array = [];
        this.video_info = [];
        this.video_info_array = [];
        this.img_info = [];
        this.img_info_array = [];
    }
}
