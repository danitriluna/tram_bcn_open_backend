import { BaseModel } from '../../_base/crud';
import { Match } from './match.model';


export class Category extends BaseModel {
    id: number;
    key: number; // 0: MALE, 1: FEMALE, 2: QUAD
    type: number; // 0: SINGLE, 1: DOUBLE, 2: CONSOLATION
    lang_info_array: any;
    matches: Match[];

    clear(): void {
        this.id = undefined;
        this.key = 0;
        this.type = 0;
        this.lang_info_array = [];
        this.matches = [];
    }
}
