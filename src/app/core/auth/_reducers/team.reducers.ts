// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { TeamActions, TeamActionTypes } from '../_actions/team.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Team } from '../_models/team.model';

// tslint:disable-next-line:no-empty-interface
export interface TeamsState extends EntityState<Team> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedTeamId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Team> = createEntityAdapter<Team>();

export const initialTeamsState: TeamsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedTeamId: undefined,
    showInitWaitingMessage: true
});

export function teamsReducer(state = initialTeamsState, action: TeamActions): TeamsState {
    switch  (action.type) {
        case TeamActionTypes.TeamsPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedTeamId: undefined
        };
        case TeamActionTypes.TeamsActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case TeamActionTypes.TeamOnServerCreated: return {
            ...state
        };
        case TeamActionTypes.TeamCreated: return adapter.addOne(action.payload.team, {
            ...state, lastCreatedTeamId: action.payload.team.id
        });
        case TeamActionTypes.TeamUpdated: return adapter.updateOne(action.payload.partialTeam, state);
        case TeamActionTypes.TeamDeleted: return adapter.removeOne(action.payload.id, state);
        case TeamActionTypes.TeamsPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case TeamActionTypes.TeamsPageLoaded: {
            return adapter.addMany(action.payload.teams, {
                ...initialTeamsState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getTeamState = createFeatureSelector<TeamsState>('teams');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
