// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { NewActions, NewActionTypes } from '../_actions/new.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { New } from '../_models/new.model';

// tslint:disable-next-line:no-empty-interface
export interface NewsState extends EntityState<New> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedNewId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<New> = createEntityAdapter<New>();

export const initialNewsState: NewsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedNewId: undefined,
    showInitWaitingMessage: true
});

export function newsReducer(state = initialNewsState, action: NewActions): NewsState {
    switch  (action.type) {
        case NewActionTypes.NewsPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedNewId: undefined
        };
        case NewActionTypes.NewsActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case NewActionTypes.NewOnServerCreated: return {
            ...state
        };
        case NewActionTypes.NewCreated: return adapter.addOne(action.payload.new, {
            ...state, lastCreatedNewId: action.payload.new.id
        });
        case NewActionTypes.NewUpdated: return adapter.updateOne(action.payload.partialNew, state);
        case NewActionTypes.NewDeleted: return adapter.removeOne(action.payload.id, state);
        case NewActionTypes.NewsPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case NewActionTypes.NewsPageLoaded: {
            return adapter.addMany(action.payload.news, {
                ...initialNewsState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getNewState = createFeatureSelector<NewsState>('news');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
