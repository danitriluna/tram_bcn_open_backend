// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { SponsorActions, SponsorActionTypes } from '../_actions/sponsor.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Sponsor } from '../_models/sponsor.model';

// tslint:disable-next-line:no-empty-interface
export interface SponsorsState extends EntityState<Sponsor> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedSponsorId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Sponsor> = createEntityAdapter<Sponsor>();

export const initialSponsorsState: SponsorsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedSponsorId: undefined,
    showInitWaitingMessage: true
});

export function sponsorsReducer(state = initialSponsorsState, action: SponsorActions): SponsorsState {
    switch  (action.type) {
        case SponsorActionTypes.SponsorsPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedSponsorId: undefined
        };
        case SponsorActionTypes.SponsorsActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case SponsorActionTypes.SponsorOnServerCreated: return {
            ...state
        };
        case SponsorActionTypes.SponsorCreated: return adapter.addOne(action.payload.sponsor, {
            ...state, lastCreatedSponsorId: action.payload.sponsor.id
        });
        case SponsorActionTypes.SponsorUpdated: return adapter.updateOne(action.payload.partialSponsor, state);
        case SponsorActionTypes.SponsorDeleted: return adapter.removeOne(action.payload.id, state);
        case SponsorActionTypes.SponsorsPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case SponsorActionTypes.SponsorsPageLoaded: {
            return adapter.addMany(action.payload.sponsors, {
                ...initialSponsorsState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getSponsorState = createFeatureSelector<SponsorsState>('sponsors');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
