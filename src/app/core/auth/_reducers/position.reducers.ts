// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { PositionActions, PositionActionTypes } from '../_actions/position.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Position } from '../_models/position.model';

// tslint:disable-next-line:no-empty-interface
export interface PositionsState extends EntityState<Position> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedPositionId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Position> = createEntityAdapter<Position>();

export const initialPositionsState: PositionsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedPositionId: undefined,
    showInitWaitingMessage: true
});

export function positionsReducer(state = initialPositionsState, action: PositionActions): PositionsState {
    switch  (action.type) {
        case PositionActionTypes.PositionsPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedPositionId: undefined
        };
        case PositionActionTypes.PositionsActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case PositionActionTypes.PositionOnServerCreated: return {
            ...state
        };
        case PositionActionTypes.PositionCreated: return adapter.addOne(action.payload.position, {
            ...state, lastCreatedPositionId: action.payload.position.id
        });
        case PositionActionTypes.PositionUpdated: return adapter.updateOne(action.payload.partialPosition, state);
        case PositionActionTypes.PositionDeleted: return adapter.removeOne(action.payload.id, state);
        case PositionActionTypes.PositionsPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case PositionActionTypes.PositionsPageLoaded: {
            return adapter.addMany(action.payload.positions, {
                ...initialPositionsState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getPositionState = createFeatureSelector<PositionsState>('positions');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
