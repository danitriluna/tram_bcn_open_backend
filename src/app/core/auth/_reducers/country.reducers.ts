// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { CountryActions, CountryActionTypes } from '../_actions/country.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Country } from '../_models/country.model';

// tslint:disable-next-line:no-empty-interface
export interface CountriesState extends EntityState<Country> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedCountryId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Country> = createEntityAdapter<Country>();

export const initialCountriesState: CountriesState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedCountryId: undefined,
    showInitWaitingMessage: true
});

export function countriesReducer(state = initialCountriesState, action: CountryActions): CountriesState {
    switch  (action.type) {
        case CountryActionTypes.CountriesPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedCountryId: undefined
        };
        case CountryActionTypes.CountriesActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case CountryActionTypes.CountryOnServerCreated: return {
            ...state
        };
        case CountryActionTypes.CountryCreated: return adapter.addOne(action.payload.country, {
            ...state, lastCreatedCountryId: action.payload.country.id
        });
        case CountryActionTypes.CountryUpdated: return adapter.updateOne(action.payload.partialCountry, state);
        case CountryActionTypes.CountryDeleted: return adapter.removeOne(action.payload.id, state);
        case CountryActionTypes.CountriesPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case CountryActionTypes.CountriesPageLoaded: {
            return adapter.addMany(action.payload.countries, {
                ...initialCountriesState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getCountryState = createFeatureSelector<CountriesState>('countries');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
