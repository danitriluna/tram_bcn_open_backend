// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { BannerActions, BannerActionTypes } from '../_actions/banner.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Banner } from '../_models/banner.model';

// tslint:disable-next-line:no-empty-interface
export interface BannersState extends EntityState<Banner> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedBannerId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Banner> = createEntityAdapter<Banner>();

export const initialBannersState: BannersState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedBannerId: undefined,
    showInitWaitingMessage: true
});

export function bannersReducer(state = initialBannersState, action: BannerActions): BannersState {
    switch  (action.type) {
        case BannerActionTypes.BannersPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedBannerId: undefined
        };
        case BannerActionTypes.BannersActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case BannerActionTypes.BannerOnServerCreated: return {
            ...state
        };
        case BannerActionTypes.BannerCreated: return adapter.addOne(action.payload.banner, {
            ...state, lastCreatedBannerId: action.payload.banner.id
        });
        case BannerActionTypes.BannerUpdated: return adapter.updateOne(action.payload.partialBanner, state);
        case BannerActionTypes.BannerDeleted: return adapter.removeOne(action.payload.id, state);
        case BannerActionTypes.BannersPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case BannerActionTypes.BannersPageLoaded: {
            return adapter.addMany(action.payload.banners, {
                ...initialBannersState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getBannerState = createFeatureSelector<BannersState>('banners');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
