// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { MatchActions, MatchActionTypes } from '../_actions/match.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Match } from '../_models/match.model';

// tslint:disable-next-line:no-empty-interface
export interface MatchesState extends EntityState<Match> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedMatchId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Match> = createEntityAdapter<Match>();

export const initialMatchesState: MatchesState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedMatchId: undefined,
    showInitWaitingMessage: true
});

export function matchesReducer(state = initialMatchesState, action: MatchActions): MatchesState {
    switch  (action.type) {
        case MatchActionTypes.MatchesPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedMatchId: undefined
        };
        case MatchActionTypes.MatchesActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case MatchActionTypes.MatchOnServerCreated: return {
            ...state
        };
        case MatchActionTypes.MatchCreated: return adapter.addOne(action.payload.match, {
            ...state, lastCreatedMatchId: action.payload.match.id
        });
        case MatchActionTypes.MatchUpdated: return adapter.updateOne(action.payload.partialMatch, state);
        case MatchActionTypes.MatchDeleted: return adapter.removeOne(action.payload.id, state);
        case MatchActionTypes.MatchesPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case MatchActionTypes.MatchesPageLoaded: {
            return adapter.addMany(action.payload.matches, {
                ...initialMatchesState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getMatchState = createFeatureSelector<MatchesState>('matches');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
