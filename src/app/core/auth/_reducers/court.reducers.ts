// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { CourtActions, CourtActionTypes } from '../_actions/court.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Court } from '../_models/court.model';

// tslint:disable-next-line:no-empty-interface
export interface CourtsState extends EntityState<Court> {
    listLoading: boolean;
    actionsloading: boolean;
    totalCount: number;
    lastCreatedCourtId: number;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Court> = createEntityAdapter<Court>();

export const initialCourtsState: CourtsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery:  new QueryParamsModel({}),
    lastCreatedCourtId: undefined,
    showInitWaitingMessage: true
});

export function courtsReducer(state = initialCourtsState, action: CourtActions): CourtsState {
    switch  (action.type) {
        case CourtActionTypes.CourtsPageToggleLoading: return {
            ...state, listLoading: action.payload.isLoading, lastCreatedCourtId: undefined
        };
        case CourtActionTypes.CourtsActionToggleLoading: return {
            ...state, actionsloading: action.payload.isLoading
        };
        case CourtActionTypes.CourtOnServerCreated: return {
            ...state
        };
        case CourtActionTypes.CourtCreated: return adapter.addOne(action.payload.court, {
            ...state, lastCreatedCourtId: action.payload.court.id
        });
        case CourtActionTypes.CourtUpdated: return adapter.updateOne(action.payload.partialCourt, state);
        case CourtActionTypes.CourtDeleted: return adapter.removeOne(action.payload.id, state);
        case CourtActionTypes.CourtsPageCancelled: return {
            ...state, listLoading: false, lastQuery: new QueryParamsModel({})
        };
        case CourtActionTypes.CourtsPageLoaded: {
            return adapter.addMany(action.payload.courts, {
                ...initialCourtsState,
                totalCount: action.payload.totalCount,
                lastQuery: action.payload.page,
                listLoading: false,
                showInitWaitingMessage: false
            });
        }
        default: return state;
    }
}

export const getCourtState = createFeatureSelector<CourtsState>('courts');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
