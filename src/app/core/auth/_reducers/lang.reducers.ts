// NGRX
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
// Actions
import { LangActionTypes, LangActions } from '../_actions/lang.actions';
// Models
import { Lang } from '../_models/lang.model';

export interface LangsState extends EntityState<Lang> {
    _isAllLangsLoaded: boolean;
}

export const adapter: EntityAdapter<Lang> = createEntityAdapter<Lang>();

export const initialLangsState: LangsState = adapter.getInitialState({
    _isAllLangsLoaded: false
});

export function langsReducer(state = initialLangsState, action: LangActions): LangsState {
    switch (action.type) {
        case LangActionTypes.AllLangsRequested:
            return {
                ...state,
                _isAllLangsLoaded: false
            };
        case LangActionTypes.AllLangsLoaded:
            return adapter.addAll(action.payload.langs, {
                ...state,
                _isAllLangsLoaded: true
            });
        default:
            return state;
    }
}

export const getRoleState = createFeatureSelector<LangsState>('langs');

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
