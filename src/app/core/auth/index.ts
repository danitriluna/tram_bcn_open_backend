// SERVICES
export { AuthService, UserService, SponsorService, MatchService, NewService, CommonService, CountryService, PositionService } from './_services';
export { AuthNoticeService } from './auth-notice/auth-notice.service';

// DATA SOURCERS
export { RolesDataSource } from './_data-sources/roles.datasource';
export { UsersDataSource } from './_data-sources/users.datasource';
export { SponsorsDataSource } from './_data-sources/sponsors.datasource';
export { MatchesDataSource } from './_data-sources/matches.datasource';
export { CourtsDataSource } from './_data-sources/court.datasource';
export { NewsDataSource } from './_data-sources/new.datasource';
export { TeamsDataSource } from './_data-sources/team.datasource';
export { BannersDataSource } from './_data-sources/banner.datasource';
export { CountriesDataSource } from './_data-sources/country.datasource';
export { PositionsDataSource } from './_data-sources/position.datasource';
/*XXXXindex_datasourceXXXX*/

// ACTIONS
export {
    Login,
    Logout,
    Register,
    UserRequested,
    UserLoaded,
    AuthActionTypes,
    AuthActions
} from './_actions/auth.actions';
export {
    AllPermissionsRequested,
    AllPermissionsLoaded,
    PermissionActionTypes,
    PermissionActions
} from './_actions/permission.actions';
export {
    RoleOnServerCreated,
    RoleCreated,
    RoleUpdated,
    RoleDeleted,
    RolesPageRequested,
    RolesPageLoaded,
    RolesPageCancelled,
    AllRolesLoaded,
    AllRolesRequested,
    RoleActionTypes,
    RoleActions
} from './_actions/role.actions';
export {
    UserCreated,
    UserUpdated,
    UserDeleted,
    UserOnServerCreated,
    UsersPageLoaded,
    UsersPageCancelled,
    UsersPageToggleLoading,
    UsersPageRequested,
    UsersActionToggleLoading
} from './_actions/user.actions';
export {
    SponsorCreated,
    SponsorUpdated,
    SponsorDeleted,
    SponsorOnServerCreated,
    SponsorsPageLoaded,
    SponsorsPageCancelled,
    SponsorsPageToggleLoading,
    SponsorsPageRequested,
    SponsorsActionToggleLoading
} from './_actions/sponsor.actions';
export {
    MatchCreated,
    MatchUpdated,
    MatchDeleted,
    MatchOnServerCreated,
    MatchesPageLoaded,
    MatchesPageCancelled,
    MatchesPageToggleLoading,
    MatchesPageRequested,
    MatchesActionToggleLoading
} from './_actions/match.actions';
export {
    CourtCreated,
    CourtUpdated,
    CourtDeleted,
    CourtOnServerCreated,
    CourtsPageLoaded,
    CourtsPageCancelled,
    CourtsPageToggleLoading,
    CourtsPageRequested,
    CourtsActionToggleLoading
} from './_actions/court.actions';
export {
    NewCreated,
    NewUpdated,
    NewDeleted,
    NewOnServerCreated,
    NewsPageLoaded,
    NewsPageCancelled,
    NewsPageToggleLoading,
    NewsPageRequested,
    NewsActionToggleLoading
} from './_actions/new.actions';
export {
    TeamCreated,
    TeamUpdated,
    TeamDeleted,
    TeamOnServerCreated,
    TeamsPageLoaded,
    TeamsPageCancelled,
    TeamsPageToggleLoading,
    TeamsPageRequested,
    TeamsActionToggleLoading
} from './_actions/team.actions';
export {
    AllLangsRequested,
    AllLangsLoaded,
    LangActionTypes,
    LangActions
} from './_actions/lang.actions';
export {
    BannerCreated,
    BannerUpdated,
    BannerDeleted,
    BannerOnServerCreated,
    BannersPageLoaded,
    BannersPageCancelled,
    BannersPageToggleLoading,
    BannersPageRequested,
    BannersActionToggleLoading
} from './_actions/banner.actions';
export {
    CountryCreated,
    CountryUpdated,
    CountryDeleted,
    CountryOnServerCreated,
    CountriesPageLoaded,
    CountriesPageCancelled,
    CountriesPageToggleLoading,
    CountriesPageRequested,
    CountriesActionToggleLoading
} from './_actions/country.actions';
export {
    PositionCreated,
    PositionUpdated,
    PositionDeleted,
    PositionOnServerCreated,
    PositionsPageLoaded,
    PositionsPageCancelled,
    PositionsPageToggleLoading,
    PositionsPageRequested,
    PositionsActionToggleLoading
} from './_actions/position.actions';
/*XXXXindex_actionsXXXX*/

// EFFECTS
export { AuthEffects } from './_effects/auth.effects';
export { PermissionEffects } from './_effects/permission.effects';
export { RoleEffects } from './_effects/role.effects';
export { UserEffects } from './_effects/user.effects';
export { SponsorEffects } from './_effects/sponsor.effects';
export { MatchEffects } from './_effects/match.effects';
export { CourtEffects } from './_effects/court.effects';
export { NewEffects } from './_effects/new.effects';
export { TeamEffects } from './_effects/team.effects';
export { LangEffects } from './_effects/lang.effects';
export { BannerEffects } from './_effects/banner.effects';
export { CountryEffects } from './_effects/country.effects';
export { PositionEffects } from './_effects/position.effects';
/*XXXXindex_effectsXXXX*/

// REDUCERS
export { authReducer } from './_reducers/auth.reducers';
export { permissionsReducer } from './_reducers/permission.reducers';
export { rolesReducer } from './_reducers/role.reducers';
export { usersReducer } from './_reducers/user.reducers';
export { sponsorsReducer } from './_reducers/sponsor.reducers';
export { matchesReducer } from './_reducers/match.reducers';
export { courtsReducer } from './_reducers/court.reducers';
export { newsReducer } from './_reducers/new.reducers';
export { teamsReducer } from './_reducers/team.reducers';
export { langsReducer } from './_reducers/lang.reducers';
export { bannersReducer } from './_reducers/banner.reducers';
export { countriesReducer } from './_reducers/country.reducers';
export { positionsReducer } from './_reducers/position.reducers';
/*XXXXindex_reducerXXXX*/

// SELECTORS
export {
    isLoggedIn,
    isLoggedOut,
    isUserLoaded,
    currentAuthToken,
    currentUser,
    currentUserRoleIds,
    currentUserPermissionsIds,
    currentUserPermissions,
    checkHasUserPermission
} from './_selectors/auth.selectors';
export {
    selectPermissionById,
    selectAllPermissions,
    selectAllPermissionsIds,
    allPermissionsLoaded
} from './_selectors/permission.selectors';
export {
    selectRoleById,
    selectAllRoles,
    selectAllRolesIds,
    allRolesLoaded,
    selectLastCreatedRoleId,
    selectRolesPageLoading,
    selectQueryResult,
    selectRolesActionLoading,
    selectRolesShowInitWaitingMessage
} from './_selectors/role.selectors';
export {
    selectUserById,
    selectUsersPageLoading,
    selectLastCreatedUserId,
    selectUsersInStore,
    selectHasUsersInStore,
    selectUsersPageLastQuery,
    selectUsersActionLoading,
    selectUsersShowInitWaitingMessage
} from './_selectors/user.selectors';
export {
    selectSponsorById,
    selectSponsorsPageLoading,
    selectLastCreatedSponsorId,
    selectSponsorsInStore,
    selectHasSponsorsInStore,
    selectSponsorsPageLastQuery,
    selectSponsorsActionLoading,
    selectSponsorsShowInitWaitingMessage
} from './_selectors/sponsor.selectors';
export {
    selectMatchById,
    selectMatchesPageLoading,
    selectLastCreatedMatchId,
    selectMatchesInStore,
    selectHasMatchesInStore,
    selectMatchesPageLastQuery,
    selectMatchesActionLoading,
    selectMatchesShowInitWaitingMessage
} from './_selectors/match.selectors';
export {
    selectCourtById,
    selectCourtsPageLoading,
    selectLastCreatedCourtId,
    selectCourtsInStore,
    selectHasCourtsInStore,
    selectCourtsPageLastQuery,
    selectCourtsActionLoading,
    selectCourtsShowInitWaitingMessage
} from './_selectors/court.selectors';
export {
    selectNewById,
    selectNewsPageLoading,
    selectLastCreatedNewId,
    selectNewsInStore,
    selectHasNewsInStore,
    selectNewsPageLastQuery,
    selectNewsActionLoading,
    selectNewsShowInitWaitingMessage
} from './_selectors/new.selectors';
export {
    selectTeamById,
    selectTeamsPageLoading,
    selectLastCreatedTeamId,
    selectTeamsInStore,
    selectHasTeamsInStore,
    selectTeamsPageLastQuery,
    selectTeamsActionLoading,
    selectTeamsShowInitWaitingMessage
} from './_selectors/team.selectors';
export {
    selectLangById,
    selectAllLangs,
    selectAllLangsIds,
    allLangsLoaded
} from './_selectors/lang.selectors';
export {
    selectBannerById,
    selectBannersPageLoading,
    selectLastCreatedBannerId,
    selectBannersInStore,
    selectHasBannersInStore,
    selectBannersPageLastQuery,
    selectBannersActionLoading,
    selectBannersShowInitWaitingMessage
} from './_selectors/banner.selectors';
export {
    selectCountryById,
    selectCountriesPageLoading,
    selectLastCreatedCountryId,
    selectCountriesInStore,
    selectHasCountriesInStore,
    selectCountriesPageLastQuery,
    selectCountriesActionLoading,
    selectCountriesShowInitWaitingMessage
} from './_selectors/country.selectors';
export {
    selectPositionById,
    selectPositionsPageLoading,
    selectLastCreatedPositionId,
    selectPositionsInStore,
    selectHasPositionsInStore,
    selectPositionsPageLastQuery,
    selectPositionsActionLoading,
    selectPositionsShowInitWaitingMessage
} from './_selectors/position.selectors';
/*XXXXindex_selectorsXXXX*/

// GUARDS
export { AuthGuard } from './_guards/auth.guard';
export { ModuleGuard } from './_guards/module.guard';

// MODELS
export { User } from './_models/user.model';
export { Team } from './_models/team.model';
export { Court } from './_models/court.model';
export { New } from './_models/new.model';
export { Category } from './_models/category.model';
export { Surface } from './_models/surface.model';
export { Sponsor } from './_models/sponsor.model';
export { Match } from './_models/match.model';
export { Permission } from './_models/permission.model';
export { Role } from './_models/role.model';
export { Lang } from './_models/lang.model';
export { Banner } from './_models/banner.model';
export { Country } from './_models/country.model';
export { Position } from './_models/position.model';
/*XXXXindex_modelXXXX*/

export { AuthNotice } from './auth-notice/auth-notice.interface';
export { AuthDataContext } from './_server/auth.data-context';
