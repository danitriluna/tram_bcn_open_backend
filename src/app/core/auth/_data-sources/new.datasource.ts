// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectNewsInStore, selectNewsPageLoading, selectNewsShowInitWaitingMessage } from '../_selectors/new.selectors';


export class NewsDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectNewsPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectNewsShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectNewsInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
