// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectTeamsInStore, selectTeamsPageLoading, selectTeamsShowInitWaitingMessage } from '../_selectors/team.selectors';


export class TeamsDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectTeamsPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectTeamsShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectTeamsInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
