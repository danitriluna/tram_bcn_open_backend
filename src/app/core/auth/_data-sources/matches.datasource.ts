// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectMatchesInStore, selectMatchesPageLoading, selectMatchesShowInitWaitingMessage } from '../_selectors/match.selectors';


export class MatchesDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectMatchesPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectMatchesShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectMatchesInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
