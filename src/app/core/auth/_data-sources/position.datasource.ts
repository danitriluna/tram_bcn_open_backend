// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectPositionsInStore, selectPositionsPageLoading, selectPositionsShowInitWaitingMessage } from '../_selectors/position.selectors';


export class PositionsDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectPositionsPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectPositionsShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectPositionsInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
