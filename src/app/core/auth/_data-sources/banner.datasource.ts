// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectBannersInStore, selectBannersPageLoading, selectBannersShowInitWaitingMessage } from '../_selectors/banner.selectors';


export class BannersDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectBannersPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectBannersShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectBannersInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
