// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectCountriesInStore, selectCountriesPageLoading, selectCountriesShowInitWaitingMessage } from '../_selectors/country.selectors';


export class CountriesDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectCountriesPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectCountriesShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectCountriesInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
