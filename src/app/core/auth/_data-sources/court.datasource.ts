// RxJS
import { of } from 'rxjs';
import { catchError, finalize, tap, debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectCourtsInStore, selectCourtsPageLoading, selectCourtsShowInitWaitingMessage } from '../_selectors/court.selectors';


export class CourtsDataSource extends BaseDataSource {
	constructor(private store: Store<AppState>) {
		super();

		this.loading$ = this.store.pipe(
			select(selectCourtsPageLoading)
		);

		this.isPreloadTextViewed$ = this.store.pipe(
			select(selectCourtsShowInitWaitingMessage)
		);

		this.store.pipe(
			select(selectCourtsInStore)
		).subscribe((response: QueryResultsModel) => {
			this.paginatorTotalSubject.next(response.totalCount);
			this.entitySubject.next(response.items);
		});
	}
}
