// Angular
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import { LayoutConfigService, LoadingBlockService } from '../../../../core/_base/layout';

@Component({
	selector: 'kt-loading-block',
	templateUrl: './loading-block.component.html',
	styleUrls: ['./loading-block.component.scss']
})
export class LoadingBlockComponent implements OnInit {
	// Public proprties
	loaderLogo: string;
	loaderType: string;

	@ViewChild('loadingBlock', {static: true}) loadingBlock: ElementRef;

	/**
	 * Component constructor
	 *
	 * @param el: ElementRef
	 * @param layoutConfigService: LayoutConfigService
	 * @param loadingBlockService: SplachScreenService
	 */
	constructor(
		private el: ElementRef,
		private layoutConfigService: LayoutConfigService,
		private loadingBlockService: LoadingBlockService) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		// init splash screen, see loader option in layout.config.ts
		const loaderConfig = this.layoutConfigService.getConfig('loader');
		this.loaderLogo = objectPath.get(loaderConfig, 'logo');

		this.loadingBlockService.init(this.loadingBlock);
	}
}
