// Angular
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { PositionService, Lang } from '../../../../../core/auth';
// Services and Models
import {
	Position,
	PositionUpdated,
	selectPositionById,
	PositionOnServerCreated,
	selectLastCreatedPositionId,
	selectPositionsActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import {environment } from '../../../../../../environments/environment';

@Component({
	selector: 'kt-position-edit',
	templateUrl: './position-edit.component.html',
})
export class PositionEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	position: Position;
	positionId$: Observable<number>;
	oldPosition: Position;
	selectedTab = 0;
	loading$: Observable<boolean>;
	rolesSubject = new BehaviorSubject<number[]>([]);
	positionForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	/* img */
	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];
	// @ViewChildren('img') imgComponents: QueryList<File>;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param positionFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param positionService: PositionService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private positionFB: FormBuilder,
		private subheaderService: SubheaderService,
		private layoutUtilsService: LayoutUtilsService,
		private store: Store<AppState>,
		private translate: TranslateService, 
		private positionService: PositionService,
		private changeRef: ChangeDetectorRef, 
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		
		this.loadingBlockService.show();
		this.currentLang = this.translate.getDefaultLang();
    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectPositionsActionLoading));


		this.currentLangs$ = this.store.pipe(select(currentLangs));
		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;
			// langs.forEach((l: Lang) => {
			// 	this.currentLangs.push(l);
			// });
			console.log("currentLangs", langs);
		});

		this.viewLoading = true;

		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			if (id && id > 0) {
				

				// this.store.pipe(select(selectPositionById(id))).subscribe(res => {
				// 	if (res) {
				// 		this.position = res;
				// 		console.log('this.position', this.position);
				// 		this.oldPosition = Object.assign({}, this.position);
				// 		this.initPosition();
				// 	} else { // F5
						this.positionService.getPositionById(id).subscribe(res => {
							this.position = res;
							console.log('=====================this.position', this.position);
							this.oldPosition = Object.assign({}, this.position);
							this.initPosition();
						});
				// 	}
				// });
			} else {

				console.log('%%%%%%%  ADD  %%%%%%');
				this.position = new Position();
				this.position.clear();
				this.oldPosition = Object.assign({}, this.position);
				this.initPosition();
			}

		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init position
	 */
	initPosition() {
		console.log('===initPosition==================this.position', this.position);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.position.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN') }), page: `position-management` },
				{
					title: this.translate.instant('POSITION_MANAGEMENT.ENTITIES'),  page: `position-management/positions` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN') }), page: `position-management/positions/add` }
			]);
			return;
		}

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN') }), page: `position-management` },
			{ title: this.translate.instant('POSITION_MANAGEMENT.ENTITIES'),  page: `position-management/positions` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN') }), page: `position-management/positions/edit`, queryParams: { id: this.position.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.position", this.position);
		let group = {}  
		group['name'] = new FormControl(this.position.name, [Validators.required]);

		this.positionForm = this.positionFB.group(group);
		
		
		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/position-management/positions`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh position
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshPosition(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/position-management/positions/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.position = Object.assign({}, this.oldPosition);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.positionForm.markAsPristine();
		this.positionForm.markAsUntouched();
		this.positionForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.positionForm.controls;
		/** check form */
		if (this.positionForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedPosition = this.preparePosition();

		if (editedPosition.id > 0) {
			this.updatePosition(editedPosition, withBack);
			return;
		}

		this.addPosition(editedPosition, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	preparePosition(): Position {
		console.log("prepare");
		const controls = this.positionForm.controls;
		console.log("controls ", controls);
		const _position = new Position();
		_position.clear();
		_position.id = this.position.id;
		_position.name = controls.name.value;
		return _position;
	}

	/**
	 * Add Position
	 *
	 * @param _position: Position
	 * @param withBack: boolean
	 */
	addPosition(_position: Position, withBack: boolean = false) {
		this.store.dispatch(new PositionOnServerCreated({ position: _position }));
		const addSubscription = this.store.pipe(select(selectLastCreatedPositionId)).subscribe(newId => {
			console.log("-------newId", newId);
			if (newId) {
				const message = (newId) ? this.translate.instant('POSITION_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('COMMON.LOADING'); // this.translate.instant('POSITION_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
				this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshPosition(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update position
	 *
	 * @param _position: Position
	 * @param withBack: boolean
	 */
	updatePosition(_position: Position, withBack: boolean = false) {
		// Update Position
		// tslint:disable-next-line:prefer-const

		const updatedPosition: Update<Position> = {
			id: _position.id,
			changes: _position
		};
		this.store.dispatch(new PositionUpdated( { partialPosition: updatedPosition, position: _position }));
		const message = this.translate.instant('POSITION_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshPosition(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN') });
		if (!this.position || !this.position.id) {
			return result;
		}
		let name = (this.position.name ) ? this.position.name : this.position.id;
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN'), entity_name: name });

		// result = `Edit position - ${this.position.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any, lang: string) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_'+lang) as HTMLElement;
		element.click();
	}

	readFile(event: any, lang: string) {
		/*
		// this.fileData[lang] = <File>event.target.files[0];
		// this.image[lang] = this.fileData[lang].name;
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				this.positionForm.get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}


		this.preview(lang);
		*/
	}
	preview(lang:string) {
		// Show preview 
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[lang] = reader.result;
		}
	}

	deletePreviewUrl(lang){
		this.positionForm.controls['img_' + lang].setValue('');
		this.previewUrl[lang] = null;
	}

	deleteImg(lang){
		//this.position.img = '';
	}
}