import { AfterViewInit, AfterViewChecked } from '@angular/core';
// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// LODASH
import { each, find } from 'lodash';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';

// Services
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../core/_base/crud';
// Models
import {
	Position,
	Role,
	PositionsDataSource,
	PositionDeleted,
	PositionsPageRequested,
	selectPositionById
} from '../../../../../core/auth';
import { SubheaderService } from '../../../../../core/_base/layout';
import { LoadingBlockService } from '../../../../../core/_base/layout';

// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'kt-positions-list',
	templateUrl: './positions-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PositionsListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: PositionsDataSource;
	displayedColumns = ['id', 'name', 'actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	lastQuery: QueryParamsModel;
	// Selection
	selection = new SelectionModel<Position>(true, []);
	positionsResult: Position[] = [];
	allRoles: Role[] = [];

	currentLang = '';

	// Subscriptions
	private subscriptions: Subscription[] = [];

	/**
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param store: Store<AppState>
	 * @param router: Router
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param translate: TranslateService
	 * @param subheaderService: SubheaderService
	 * @param cdr: ChangeDetectorRef
	 * @param translate: TranslateService
	 * @param dialog: MatDialog
	 */
	constructor(
		private activatedRoute: ActivatedRoute,
		private store: Store<AppState>,
		private router: Router,
		private layoutUtilsService: LayoutUtilsService,
		private subheaderService: SubheaderService,
		private cdr: ChangeDetectorRef,
		private translate: TranslateService,
		private loadingBlockService: LoadingBlockService,
		public dialog: MatDialog) {
			this.loadingBlockService.show();
			this.currentLang = this.translate.getDefaultLang();
		}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		// load roles list
		// const rolesSubscription = this.store.pipe(select(selectAllRoles)).subscribe(res => this.allRoles = res);
		// this.subscriptions.push(rolesSubscription);

		// If the position changes the sort order, reset back to the first page.
		const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		this.subscriptions.push(sortSubscription);

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => {
				this.loadPositionsList();
			})
		)
		.subscribe();
		this.subscriptions.push(paginatorSubscriptions);


		// Filtration, bind to searchInput
		const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			// tslint:disable-next-line:max-line-length
			debounceTime(150), // The position can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
			distinctUntilChanged(), // This operator will eliminate duplicate values
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadPositionsList();
			})
		)
		.subscribe();
		this.subscriptions.push(searchSubscription);

		// Set title to page breadCrumbs
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITIES_MIN') }));

		// Init DataSource
		this.dataSource = new PositionsDataSource(this.store);
		const entitiesSubscription = this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.positionsResult = res;
			// this.dataSource.hasItems = res.length>0;
		});
		this.subscriptions.push(entitiesSubscription);

		// First Load
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => { // Remove this line, just loading imitation
			this.loadPositionsList();
		});
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load positions list
	 */
	loadPositionsList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		this.store.dispatch(new PositionsPageRequested({ page: queryParams }));
		this.selection.clear();
		this.loadingBlockService.hide();
	}

	/** FILTRATION */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;

		filter.title = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete position
	 *
	 * @param _item: Position
	 */
	deletePosition(_item: Position) {
		const _title = this.translate.instant('MANAGEMENT.COMMON.DELETE.TITLE', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITY_MIN'), title: _item.id });
		const _description = this.translate.instant('MANAGEMENT.COMMON.DELETE.DESCRIPTION');
		const _waitDesciption = this.translate.instant('MANAGEMENT.COMMON.DELETE.WAIT_DESCRIPTION');
		const _deleteMessage = this.translate.instant('MANAGEMENT.COMMON.DELETE.MESSAGE');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.store.dispatch(new PositionDeleted({ id: _item.id }));
			this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
		});
	}


	/**
	 * Redirect to edit page
	 *
	 * @param id
	 */
	editPosition(id) {
		this.router.navigate(['../positions/edit', id], { relativeTo: this.activatedRoute });
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		return this.translate.instant('MANAGEMENT.COMMON.ENTITY_LIST', { entity: this.translate.instant('POSITION_MANAGEMENT.ENTITIES_MIN') });
	}
}
