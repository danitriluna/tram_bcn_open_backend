// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { MatchService, CommonService, UserService } from '../../../../../core/auth';
// Services and Models
import {
	Match,
	MatchUpdated,
	selectMatchById,
	MatchOnServerCreated,
	selectLastCreatedMatchId,
	selectMatchesActionLoading,
	Court,
	User,
	Team,
	Category
} from '../../../../../core/auth';
import { each } from 'lodash';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
	selector: 'kt-match-edit',
	templateUrl: './match-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
	providers: [
		{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
		{ provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
	],
})
export class MatchEditComponent implements OnInit, OnDestroy {
	// Public properties
	match: Match;
	matchId$: Observable<number>;
	oldMatch: Match;
	selectedTab = 0;
	loading$: Observable<boolean>;
	matchForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];


	allCourts: Court[] = [];
	allTeams: Team[] = [];
	allWinnersTeams: Team[] = [];
	allPlayers: User[] = [];
	allWinners: User[] = [];
	allArbitrators: User[] = [];
	allCategories: Category[] = [];
	matchCourtId = 0;
	matchPlayer1Id = 0;
	matchPlayer2Id = 0;
	matchWinnerId = 0;
	matchRetiredId = 0;
	matchArbitratorId = 0;
	matchCategoryId = 0;
	matchDate = null;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param matchFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param matchService: MatchService
	 * @param commonService: CommonService,
	 * @param userService: UserService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private matchFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
		private changeRef: ChangeDetectorRef, 
					private matchService: MatchService,
					private commonService: CommonService,
					private userService: UserService,
		private layoutConfigService: LayoutConfigService,
		private loadingBlockService: LoadingBlockService) { 

		this.loadingBlockService.show();
		}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectMatchesActionLoading));

		this.viewLoading = true;

		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			

			this.matchService.getAllCourts().subscribe((res: Court[]) => {
				each(res, (_s: Court) => {
					this.allCourts.push(_s);

				});
				this.matchService.getAllCategories().subscribe((res: Category[]) => {
					each(res, (_s: Category) => {
						this.allCategories.push(_s);

					});
					this.userService.getAllPlayers().subscribe((res: User[]) => {
						each(res, (_u: User) => {
							this.allPlayers.push(_u);
							this.allWinners.push(_u);
						});
						this.matchService.getAllTeams().subscribe((res: Team[]) => {
							each(res, (_t: Team) => {
								this.allTeams.push(_t);
								this.allWinnersTeams.push(_t);
							});
	
							this.userService.getAllArbitrators().subscribe((res: User[]) => {
								each(res, (_u: User) => {
									this.allArbitrators.push(_u);
								});
								if (id && id > 0) {
	
	
									this.store.pipe(select(selectMatchById(id))).subscribe(res => {
										if (res) {
											this.match = res;
											console.log('this.match', this.match);
											this.oldMatch = Object.assign({}, this.match);
											this.initMatch();
										} else { // F5
											this.matchService.getMatchById(id).subscribe(res => {
												this.match = res;
												console.log('=====================this.match', this.match);
												this.oldMatch = Object.assign({}, this.match);
												this.initMatch();
											});
										}
									});
								} else {
	
									console.log('%%%%%%%  ADD  %%%%%%');
									this.match = new Match();
									this.match.clear();
									this.oldMatch = Object.assign({}, this.match);
									this.initMatch();
								}
							});
						});
					});
				});
			});


		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init match
	 */
	initMatch() {
		console.log('===initMatch==================this.match', this.match);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.match.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('MATCHr_MANAGEMENT.ENTITY_MIN') }), page: `match-management` },
				{
					title: this.translate.instant('MATCH_MANAGEMENT.ENTITIES'),  page: `match-management/matches` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITY_MIN') }), page: `match-management/matches/add` }
			]);
			return;
		}

		this.matchCourtId = this.match.court ? this.match.court.id : 0;
		this.matchPlayer1Id = this.match.player1 ? this.match.player1.id : 0;
		this.matchPlayer2Id = this.match.player2 ? this.match.player2.id : 0;
		this.matchDate = this.match.date ? this.match.date : null;
		this.matchArbitratorId = this.match.arbitrator ? this.match.arbitrator.id : 0;
		this.matchWinnerId = this.match.winner ? this.match.winner.id : 0;
		this.matchRetiredId = this.match.retired ? this.match.retired.id : 0;

		console.log('this.matchPlayer1Id', this.matchPlayer1Id);
		

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITY_MIN') }), page: `match-management` },
			{ title: this.translate.instant('MATCH_MANAGEMENT.ENTITIES'),  page: `match-management/matches` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITY_MIN') }), page: `match-management/matches/edit`, queryParams: { id: this.match.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.match", this.match);
		this.matchForm = this.matchFB.group({
			player1: [this.match.player1 ? this.match.player1.id : ''],
			player2: [this.match.player2 ? this.match.player2.id : ''],
			arbitrator: [this.match.arbitrator ? this.match.arbitrator.id : ''],
			winner: [this.match.winner ? this.match.winner.id : ''],
			retired: [this.match.retired ? this.match.retired.id : ''],
			court: [this.match.court ? this.match.court.id : ''],
			category: [this.match.category ? this.match.category.id : ''],
			date: [this.match.date]
		});
		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/match-management/matches`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh match
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshMatch(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/match-management/matches/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.match = Object.assign({}, this.oldMatch);
		this.createForm();
		this.hasFormErrors = false;
		this.matchForm.markAsPristine();
		this.matchForm.markAsUntouched();
		this.matchForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.matchForm.controls;
		/** check form */
		if (this.matchForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedMatch = this.prepareMatch();

		if (editedMatch.id > 0) {
			this.updateMatch(editedMatch, withBack);
			return;
		}

		this.addMatch(editedMatch, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareMatch(): Match {
		console.log("prepare");
		const controls = this.matchForm.controls;
		const _match = new Match();
		_match.clear();
		_match.id = this.match.id;
		_match.player1 = controls.player1.value;
		_match.player2 = controls.player2.value;
		_match.arbitrator = controls.arbitrator.value;
		_match.winner = controls.winner.value;
		_match.retired = controls.retired.value;
		_match.court = controls.court.value;
		_match.court = controls.court.value;

		return _match;
	}

	/**
	 * Add Match
	 *
	 * @param _match: Match
	 * @param withBack: boolean
	 */
	addMatch(_match: Match, withBack: boolean = false) {
		this.store.dispatch(new MatchOnServerCreated({ match: _match }));
		const addSubscription = this.store.pipe(select(selectLastCreatedMatchId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('Match_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('Match_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshMatch(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update match
	 *
	 * @param _match: Match
	 * @param withBack: boolean
	 */
	updateMatch(_match: Match, withBack: boolean = false) {
		// Update Match
		// tslint:disable-next-line:prefer-const

		const updatedMatch: Update<Match> = {
			id: _match.id,
			changes: _match
		};
		this.store.dispatch(new MatchUpdated( { partialMatch: updatedMatch, match: _match }));
		const message = this.translate.instant('MATCH_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshMatch(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITY_MIN') });
		if (!this.match || !this.match.id) {
			return result;
		}
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('MATCH_MANAGEMENT.ENTITY_MIN'), entity_name: this.match.id });

		// result = `Edit match - ${this.match.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


}
