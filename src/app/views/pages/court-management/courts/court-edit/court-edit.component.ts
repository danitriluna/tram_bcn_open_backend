// Angular
import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { MatchService, Lang } from '../../../../../core/auth';
// Services and Models
import {
	Court,
	CourtUpdated,
	selectCourtById,
	CourtOnServerCreated,
	selectLastCreatedCourtId,
	selectCourtsActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import {environment } from '../../../../../../environments/environment';

@Component({
	selector: 'kt-court-edit',
	templateUrl: './court-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class CourtEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	court: Court;
	courtId$: Observable<number>;
	oldCourt: Court;
	selectedTab = 0;
	loading$: Observable<boolean>;
	courtForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	/* img */
	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];
	// @ViewChildren('img') imgComponents: QueryList<File>;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param courtFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param matchService: MatchService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private courtFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
		private matchService: MatchService,
		private changeRef: ChangeDetectorRef,
		private loadingBlockService: LoadingBlockService,
		           private layoutConfigService: LayoutConfigService) {
		this.currentLang = this.translate.getDefaultLang();
		this.loadingBlockService.show();
				    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectCourtsActionLoading));


		this.currentLangs$ = this.store.pipe(select(currentLangs));
		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;
			// langs.forEach((l: Lang) => {
			// 	this.currentLangs.push(l);
			// });
			console.log("currentLangs", langs);
		});

		this.viewLoading = true;

		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			if (id && id > 0) {
				

				// this.store.pipe(select(selectCourtById(id))).subscribe(res => {
				// 	if (res) {
				// 		this.court = res;
				// 		console.log('this.court', this.court);
				// 		this.oldCourt = Object.assign({}, this.court);
				// 		this.initCourt();
				// 	} else { // F5
						this.matchService.getCourtById(id).subscribe(res => {
							this.court = res;
							console.log('=====================this.court', this.court);
							this.oldCourt = Object.assign({}, this.court);
							this.initCourt();
						});
				// 	}
				// });
			} else {

				console.log('%%%%%%%  ADD  %%%%%%');
				this.court = new Court();
				this.court.clear();
				this.oldCourt = Object.assign({}, this.court);
				this.initCourt();
			}

		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init court
	 */
	initCourt() {
		console.log('===initCourt==================this.court', this.court);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.court.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COURTr_MANAGEMENT.ENTITY_MIN') }), page: `court-management` },
				{
					title: this.translate.instant('COURT_MANAGEMENT.ENTITIES'),  page: `court-management/courts` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITY_MIN') }), page: `court-management/courts/add` }
			]);
			return;
		}

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITY_MIN') }), page: `court-management` },
			{ title: this.translate.instant('COURT_MANAGEMENT.ENTITIES'),  page: `court-management/courts` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITY_MIN') }), page: `court-management/courts/edit`, queryParams: { id: this.court.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.court", this.court);
		let group = {}  
		group['map'] = new FormControl(this.court.map, [Validators.required]);
		group['coords'] = new FormControl(this.court.coords, [Validators.required]);

		this.currentLangs.forEach(l =>{
			group['name_' + l.key] = new FormControl((this.court.lang_info_array[l.key]) ? this.court.lang_info_array[l.key].name : '', [Validators.required]);
			// group['img_' + l.key] = new FormControl((this.court.lang_info_array[l.key]) ? this.court.lang_info_array[l.key].img : '');
			group['img_' + l.key] = new FormControl('');
	    });
		/*
    this.myFormGroup = new FromGroup(group); */

		this.courtForm = this.courtFB.group(group);
		console.log("@@@@@@@@@@@@@@@@@@@@this.courtForm", this.courtForm);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();

		// this.courtForm = this.courtFB.group({
		// 	map: [this.court.map, [Validators.required]]
		// });
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/court-management/courts`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh court
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshCourt(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/court-management/courts/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.court = Object.assign({}, this.oldCourt);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.courtForm.markAsPristine();
		this.courtForm.markAsUntouched();
		this.courtForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.courtForm.controls;
		/** check form */
		if (this.courtForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedCourt = this.prepareCourt();

		if (editedCourt.id > 0) {
			this.updateCourt(editedCourt, withBack);
			return;
		}

		this.addCourt(editedCourt, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareCourt(): Court {
		console.log("prepare");
		const controls = this.courtForm.controls;
		console.log("controls ", controls);
		const _court = new Court();
		_court.clear();
		_court.id = this.court.id;
		_court.map = controls.map.value;
		_court.coords = controls.coords.value;
		_court.lang_info_array = { };
		this.currentLangs.forEach(l => {
			console.log("this.court.lang_info_array[l.key].img", this.court.lang_info_array[l.key].img);
			_court.lang_info_array[l.id] = { 
				'name': controls['name_' + l.key].value,
				'img': (controls['img_' + l.key].value ? controls['img_' + l.key].value : (this.court.lang_info_array[l.key].img ? this.court.lang_info_array[l.key].img : '')),
				// 'img': controls['img_' + l.key].value,
			};
			// _court.lang_info_array[l.id].name = controls['name_'+l.key].value;
			// _court.lang_info_array[l.id].img = controls['img_'+l.key].value;
		});
		_court.lang_info_array = JSON.stringify(_court.lang_info_array);
		return _court;
	}

	/**
	 * Add Court
	 *
	 * @param _court: Court
	 * @param withBack: boolean
	 */
	addCourt(_court: Court, withBack: boolean = false) {
		this.store.dispatch(new CourtOnServerCreated({ court: _court }));
		const addSubscription = this.store.pipe(select(selectLastCreatedCourtId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('Court_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('Court_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshCourt(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update court
	 *
	 * @param _court: Court
	 * @param withBack: boolean
	 */
	updateCourt(_court: Court, withBack: boolean = false) {
		// Update Court
		// tslint:disable-next-line:prefer-const

		const updatedCourt: Update<Court> = {
			id: _court.id,
			changes: _court
		};
		this.store.dispatch(new CourtUpdated( { partialCourt: updatedCourt, court: _court }));
		const message = this.translate.instant('COURT_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshCourt(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITY_MIN') });
		if (!this.court || !this.court.id) {
			return result;
		}
		let name = (this.court.lang_info_array[this.currentLang] && this.court.lang_info_array[this.currentLang].name) ? this.court.lang_info_array[this.currentLang].name : this.court.id;
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('COURT_MANAGEMENT.ENTITY_MIN'), entity_name: name });

		// result = `Edit court - ${this.court.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any, lang: string) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_'+lang) as HTMLElement;
		element.click();
	}
	readFile(event: any, lang: string) {
		// this.fileData[lang] = <File>event.target.files[0];
		// this.image[lang] = this.fileData[lang].name;
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				this.courtForm.get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}


		this.preview(lang);
	}
	preview(lang:string) {
		// Show preview 
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[lang] = reader.result;
		}
	}

	deletePreviewUrl(lang){
		this.courtForm.controls['img_' + lang].setValue('');
		this.previewUrl[lang] = null;
	}

	deleteImg(lang){
		this.court.lang_info_array[lang].img = '';
	}
}
