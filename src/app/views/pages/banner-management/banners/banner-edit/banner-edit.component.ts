// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common'
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { SponsorService, PositionService, Lang } from '../../../../../core/auth';
// Services and Models
import {
	Banner,
	BannerUpdated,
	selectBannerById,
	BannerOnServerCreated,
	selectLastCreatedBannerId,
	selectBannersActionLoading,
	Sponsor,
	Position
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import {environment } from '../../../../../../environments/environment';

@Component({
	selector: 'kt-banner-edit',
	templateUrl: './banner-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class BannerEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	banner: Banner;
	bannerId$: Observable<number>;
	oldBanner: Banner;
	selectedTab = 0;
	loading$: Observable<boolean>;
	bannerForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	allSponsors: Sponsor[] = [];
	allPositions: Position[] = [];

	sponsor_value = 0;
	position_value = 0;
	newDateStart = null;
	newDateEnd = null;

	/* img */
	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];
	// @ViewChildren('img') imgComponents: QueryList<File>;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param bannerFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param sponsorService: SponsorService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		        	private router: Router,
		        	private bannerFB: FormBuilder,
		        	private subheaderService: SubheaderService,
		        	private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
					private changeRef: ChangeDetectorRef, 
					private sponsorService: SponsorService,
					private layoutConfigService: LayoutConfigService,
					private loadingBlockService: LoadingBlockService,
					public datepipe: DatePipe) {
		this.currentLang = this.translate.getDefaultLang();
		this.loadingBlockService.show();
				    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectBannersActionLoading));

		let dis = this;
		let routeSubscription;

		this.currentLangs$ = this.store.pipe(select(currentLangs));
		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;
			// langs.forEach((l: Lang) => {
			// 	this.currentLangs.push(l);
			// });
			console.log("currentLangs", langs);

			dis.sponsorService.getAllPositions().subscribe((res: Position[]) => {
				this.allPositions = [];
				each(res, (_s: Position) => {
					this.allPositions.push(_s);
				});

				dis.sponsorService.getAllSponsors().subscribe((res: Sponsor[]) => {
					this.allSponsors = [];
					each(res, (_s: Sponsor) => {
						this.allSponsors.push(_s);
					});

					dis.viewLoading = true;

					routeSubscription =  this.activatedRoute.params.subscribe(params => {
						const id = params.id;
						console.log('%%%%%%% params', params);
						if (id && id > 0) {
							

							// this.store.pipe(select(selectBannerById(id))).subscribe(res => {
							// 	if (res) {
							// 		this.banner = res;
							// 		console.log('this.banner', this.banner);
							// 		this.oldBanner = Object.assign({}, this.banner);
							// 		this.initBanner();
							// 	} else { // F5
									this.sponsorService.getBannerById(id).subscribe(res => {
										this.banner = res;
										console.log('=====================this.banner', this.banner);
										this.oldBanner = Object.assign({}, this.banner);
										this.initBanner();
									});
							// 	}
							// });
						} else {

							console.log('%%%%%%%  ADD  %%%%%%');
							this.banner = new Banner();
							this.banner.clear();
							this.oldBanner = Object.assign({}, this.banner);
							this.initBanner();
						}

					});
					dis.subscriptions.push(routeSubscription);
				});
			});
		});
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init banner
	 */
	initBanner() {
		console.log('===initBanner==================this.banner', this.banner);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.banner.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('BANNERr_MANAGEMENT.ENTITY_MIN') }), page: `banner-management` },
				{
					title: this.translate.instant('BANNER_MANAGEMENT.ENTITIES'),  page: `banner-management/banners` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITY_MIN') }), page: `banner-management/banners/add` }
			]);
			return;
		}

		this.sponsor_value = this.banner.sponsor ? this.banner.sponsor.id : 0;
		this.position_value = this.banner.position ? this.banner.position.id : 0;
		this.newDateStart = this.banner.date_start ? this.banner.date_start : null;
		this.newDateEnd = this.banner.date_end ? this.banner.date_end : null;

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITY_MIN') }), page: `banner-management` },
			{ title: this.translate.instant('BANNER_MANAGEMENT.ENTITIES'),  page: `banner-management/banners` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITY_MIN') }), page: `banner-management/banners/edit`, queryParams: { id: this.banner.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.banner", this.banner);
		let group = {}  

		group['sponsor'] = new FormControl((this.banner.sponsor) ? this.banner.sponsor.id : '', [Validators.required]);
		group['position'] = new FormControl((this.banner.position) ? this.banner.position.id : '', [Validators.required]);
		
		group['date_start'] = new FormControl((this.banner.date_start) ? this.banner.date_start : '', [Validators.required]);
		group['date_end'] = new FormControl((this.banner.date_end) ? this.banner.date_end : '', [Validators.required]);

		this.currentLangs.forEach(l =>{
			group['name_' + l.key] = new FormControl((this.banner.lang_info_array[l.key]) ? this.banner.lang_info_array[l.key].name : '', [Validators.required]);
			group['img_' + l.key] = new FormControl((this.banner.lang_info_array[l.key]) ? this.banner.lang_info_array[l.key].img : '');
			group['url_' + l.key] = new FormControl((this.banner.lang_info_array[l.key]) ? this.banner.lang_info_array[l.key].url : '');
	    });
		/*
    this.myFormGroup = new FromGroup(group); */

		this.bannerForm = this.bannerFB.group(group);
		console.log("@@@@@@@@@@@@@@@@@@@@this.bannerForm", this.bannerForm);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();

		// this.bannerForm = this.bannerFB.group({
		// 	map: [this.banner.map, [Validators.required]]
		// });


		console.log("ALL POSITIONS", this.allPositions);
		console.log("ALL SPONSORS", this.allSponsors);
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/banner-management/banners`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh banner
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshBanner(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/banner-management/banners/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.banner = Object.assign({}, this.oldBanner);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.bannerForm.markAsPristine();
		this.bannerForm.markAsUntouched();
		this.bannerForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.bannerForm.controls;
		/** check form */
		if (this.bannerForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedBanner = this.prepareBanner();

		if (editedBanner.id > 0) {
			this.updateBanner(editedBanner, withBack);
			return;
		}

		this.addBanner(editedBanner, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareBanner(): Banner {
		console.log("prepare");
		const controls = this.bannerForm.controls;
		console.log("controls ", controls);
		const _banner = new Banner();
		_banner.clear();
		_banner.id = this.banner.id;
		_banner.sponsor = controls.sponsor.value;
		_banner.position = controls.position.value;
		_banner.date_start = this.datepipe.transform(controls['date_start'].value, 'dd-MM-yyyy');
		_banner.date_end = this.datepipe.transform(controls['date_end'].value, 'dd-MM-yyyy');
		_banner.lang_info_array = { };
		this.currentLangs.forEach(l => {
			//console.log("this.banner.lang_info_array[l.key].img", this.banner.lang_info_array[l.key].img);
			_banner.lang_info_array[l.id] = { 
				'name': controls['name_' + l.key].value,
				'img': (controls['img_' + l.key] != undefined ? controls['img_' + l.key].value : (this.banner.lang_info_array[l.key].img ? this.banner.lang_info_array[l.key].img : '')),
				'url': controls['url_' + l.key].value,
			};
			// _banner.lang_info_array[l.id].name = controls['name_'+l.key].value;
			// _banner.lang_info_array[l.id].img = controls['img_'+l.key].value;
		});
		_banner.lang_info_array = JSON.stringify(_banner.lang_info_array);

		return _banner;
	}

	/**
	 * Add Banner
	 *
	 * @param _banner: Banner
	 * @param withBack: boolean
	 */
	addBanner(_banner: Banner, withBack: boolean = false) {
		this.store.dispatch(new BannerOnServerCreated({ banner: _banner }));
		const addSubscription = this.store.pipe(select(selectLastCreatedBannerId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('Banner_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('COMMON.LOADING'); //this.translate.instant('Banner_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshBanner(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update banner
	 *
	 * @param _banner: Banner
	 * @param withBack: boolean
	 */
	updateBanner(_banner: Banner, withBack: boolean = false) {
		// Update Banner
		// tslint:disable-next-line:prefer-const

		const updatedBanner: Update<Banner> = {
			id: _banner.id,
			changes: _banner
		};
		this.store.dispatch(new BannerUpdated( { partialBanner: updatedBanner, banner: _banner }));
		const message = this.translate.instant('BANNER_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshBanner(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITY_MIN') });
		if (!this.banner || !this.banner.id) {
			return result;
		}
		let name = (this.banner.lang_info_array[this.currentLang] && this.banner.lang_info_array[this.currentLang].name) ? this.banner.lang_info_array[this.currentLang].name : this.banner.id;
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('BANNER_MANAGEMENT.ENTITY_MIN'), entity_name: name });

		// result = `Edit banner - ${this.banner.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any, lang: string) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_'+lang) as HTMLElement;
		element.click();
	}
	readFile(event: any, lang: string) {
		// this.fileData[lang] = <File>event.target.files[0];
		// this.image[lang] = this.fileData[lang].name;
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				this.bannerForm.get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}


		this.preview(lang);
	}
	preview(lang:string) {
		// Show preview 
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[lang] = reader.result;
		}
	}

	deletePreviewUrl(lang:string){
		this.bannerForm.controls['img_' + lang].setValue('');
		this.previewUrl[lang] = null;
	}

	deleteImg(lang:string){
		this.banner.lang_info_array[lang].img = '';
	}
}
