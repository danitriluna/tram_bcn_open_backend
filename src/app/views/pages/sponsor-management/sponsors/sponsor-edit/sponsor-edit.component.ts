// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { SponsorService, Lang  } from '../../../../../core/auth';
// Services and Models
import {
	Sponsor,
	SponsorUpdated,
	selectSponsorById,
	SponsorOnServerCreated,
	selectLastCreatedSponsorId,
	selectSponsorsActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import {environment } from '../../../../../../environments/environment';

@Component({
	selector: 'kt-sponsor-edit',
	templateUrl: './sponsor-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class SponsorEditComponent implements OnInit, OnDestroy {
	// Public properties
	sponsor: Sponsor;
	sponsorId$: Observable<number>;
	oldSponsor: Sponsor;
	selectedTab = 0;
	loading$: Observable<boolean>;
	sponsorForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	/* img */
	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];
	
	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param sponsorFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param sponsorService: SponsorService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private sponsorFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
		private changeRef: ChangeDetectorRef, 
		private sponsorService: SponsorService,
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		this.loadingBlockService.show();
		this.currentLang = this.translate.getDefaultLang();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectSponsorsActionLoading));

		this.currentLangs$ = this.store.pipe(select(currentLangs));

		let dis = this;
		let routeSubscription;

		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;
			// langs.forEach((l: Lang) => {
			// 	this.currentLangs.push(l);
			// });
			console.log("currentLangs", langs);

			dis.viewLoading = true;

			routeSubscription =  dis.activatedRoute.params.subscribe(params => {
				const id = params.id;
				console.log('%%%%%%% params', params);
				if (id && id > 0) {
					

					this.store.pipe(select(selectSponsorById(id))).subscribe(res => {
						if (res) {
							this.sponsor = res;
							console.log('this.sponsor', this.sponsor);
							this.oldSponsor = Object.assign({}, this.sponsor);
							this.initSponsor();
						} else { // F5
							this.sponsorService.getSponsorById(id).subscribe(res => {
								this.sponsor = res;
								console.log('=====================this.sponsor', this.sponsor);
								this.oldSponsor = Object.assign({}, this.sponsor);
								this.initSponsor();
							});
						}
					});
				} else {

					console.log('%%%%%%%  ADD  %%%%%%');
					this.sponsor = new Sponsor();
					this.sponsor.clear();
					this.oldSponsor = Object.assign({}, this.sponsor);
					this.initSponsor();
				}

			});
			dis.subscriptions.push(routeSubscription);

		});
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init sponsor
	 */
	initSponsor() {
		console.log('===initSponsor==================this.sponsor', this.sponsor);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.sponsor.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('SPONSORr_MANAGEMENT.ENTITY_MIN') }), page: `sponsor-management` },
				{
					title: this.translate.instant('SPONSOR_MANAGEMENT.ENTITIES'),  page: `sponsor-management/sponsors` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITY_MIN') }), page: `sponsor-management/sponsors/add` }
			]);
			return;
		}

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITY_MIN') }), page: `sponsor-management` },
			{ title: this.translate.instant('SPONSOR_MANAGEMENT.ENTITIES'),  page: `sponsor-management/sponsors` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITY_MIN') }), page: `sponsor-management/sponsors/edit`, queryParams: { id: this.sponsor.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.sponsor", this.sponsor);
		
		/*
		this.sponsorForm = this.sponsorFB.group({
			order_position: [this.sponsor.order_position, [Validators.required]]
		});
		*/
		let group = {}  
		// group['name'] = new FormControl(this.country.name, [Validators.required]);
		group['order_position'] = new FormControl(this.sponsor.order_position, [Validators.required]);

		this.currentLangs.forEach(l =>{
			console.log(" -> name_" + l.key);
			group['name_' + l.key] = new FormControl((this.sponsor.lang_info_array[l.key]) ? this.sponsor.lang_info_array[l.key].name : '', [Validators.required]);
			group['img_' + l.key] = new FormControl((this.sponsor.lang_info_array[l.key]) ? this.sponsor.lang_info_array[l.key].img : '', [Validators.required]);
			group['url_' + l.key] = new FormControl((this.sponsor.lang_info_array[l.key]) ? this.sponsor.lang_info_array[l.key].url : '', [Validators.required]);
	    });

		this.sponsorForm = this.sponsorFB.group(group);
		
		console.log("sponsorForm", this.sponsorForm);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/sponsor-management/sponsors`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh sponsor
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshSponsor(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/sponsor-management/sponsors/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.sponsor = Object.assign({}, this.oldSponsor);
		this.createForm();
		this.hasFormErrors = false;
		this.sponsorForm.markAsPristine();
		this.sponsorForm.markAsUntouched();
		this.sponsorForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.sponsorForm.controls;
		/** check form */
		if (this.sponsorForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedSponsor = this.prepareSponsor();

		if (editedSponsor.id > 0) {
			this.updateSponsor(editedSponsor, withBack);
			return;
		}

		this.addSponsor(editedSponsor, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareSponsor(): Sponsor {
		console.log("prepare");
		const controls = this.sponsorForm.controls;
		const _sponsor = new Sponsor();
		_sponsor.clear();
		_sponsor.id = this.sponsor.id;
		_sponsor.order_position = controls.order_position.value;
		_sponsor.lang_info_array = { };
		this.currentLangs.forEach(l => {
			console.log("this.sponsor.lang_info_array[l.key]", this.sponsor.lang_info_array[l.key]);
			console.log("controls['name_' + l.key].value", controls['name_' + l.key].value);
			_sponsor.lang_info_array[l.id] = { 
				'name': controls['name_' + l.key].value,
				'img': (controls['img_' + l.key].value ? controls['img_' + l.key].value : (this.sponsor.lang_info_array[l.key].img ? this.sponsor.lang_info_array[l.key].img : '')),
				'url': controls['url_' + l.key].value
			};
		});
		_sponsor.lang_info_array = JSON.stringify(_sponsor.lang_info_array);

		return _sponsor;
	}

	/**
	 * Add Sponsor
	 *
	 * @param _sponsor: Sponsor
	 * @param withBack: boolean
	 */
	addSponsor(_sponsor: Sponsor, withBack: boolean = false) {
		this.store.dispatch(new SponsorOnServerCreated({ sponsor: _sponsor }));
		const addSubscription = this.store.pipe(select(selectLastCreatedSponsorId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('SPONSOR_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('COMMON.LOADING'); //this.translate.instant('Sponsor_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshSponsor(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update sponsor
	 *
	 * @param _sponsor: Sponsor
	 * @param withBack: boolean
	 */
	updateSponsor(_sponsor: Sponsor, withBack: boolean = false) {
		// Update Sponsor
		// tslint:disable-next-line:prefer-const

		const updatedSponsor: Update<Sponsor> = {
			id: _sponsor.id,
			changes: _sponsor
		};
		this.store.dispatch(new SponsorUpdated( { partialSponsor: updatedSponsor, sponsor: _sponsor }));
		const message = this.translate.instant('SPONSOR_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshSponsor(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITY_MIN') });
		if (!this.sponsor || !this.sponsor.id) {
			return result;
		}
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('SPONSOR_MANAGEMENT.ENTITY_MIN'), entity_name: this.sponsor.order_position });

		// result = `Edit sponsor - ${this.sponsor.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}

	openBrowser(event: any, lang: string) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_'+lang) as HTMLElement;
		element.click();
	}
	readFile(event: any, lang: string) {
		// this.fileData[lang] = <File>event.target.files[0];
		// this.image[lang] = this.fileData[lang].name;
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				this.sponsorForm.get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}


		this.preview(lang);
	}
	preview(lang:string) {
		// Show preview 
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[lang] = reader.result;
		}
	}

	deletePreviewUrl(lang){
		this.sponsorForm.controls['img_' + lang].setValue('');
		this.previewUrl[lang] = null;
	}

	deleteImg(lang){
		this.sponsor.lang_info_array[lang].img = '';
	}

}
