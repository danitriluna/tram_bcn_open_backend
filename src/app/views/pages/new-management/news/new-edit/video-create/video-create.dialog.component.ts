// Angular
import { Component, OnInit, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// RxJS
import { Observable, of, Subscription} from 'rxjs';
// Lodash
import { each, find, some } from 'lodash';
// NGRX
import { Update } from '@ngrx/entity';
// import { Store, select } from '@ngrx/store';
// State
// import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { delay } from 'rxjs/operators';
import { NewVideo } from '../../../../../../core/auth/_models/newVideo.model';
import { Lang, NewService } from '../../../../../../core/auth';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'kt-video-create-dialog',
	templateUrl: './video-create.dialog.component.html'
})
export class VideoCreateDialogComponent implements OnInit, OnDestroy {
	// Public properties
	id_news: number;
	newVideo: NewVideo;
	newVideo$: Observable<NewVideo>;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	newVideoForm: FormGroup;
	// Private properties
	private componentSubscriptions: Subscription;
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<VideoCreateDialogComponent>
	 * @param data: any
	 * @param translate: TranslateService
	 */
	constructor(public dialogRef: MatDialogRef<VideoCreateDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private newVideoFB: FormBuilder,
		private newService: NewService,
		private changeRef: ChangeDetectorRef, 
		private translate: TranslateService) { 
		this.id_news = data.newId;
		this.currentLangs = data.currentLangs;
		console.log("this.currentLangs", this.currentLangs);
		this.currentLang = this.translate.getDefaultLang();
				   }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		let groupVideo = {}
		this.currentLangs.forEach(l => {
			groupVideo['title_' + l.key] = new FormControl('', [Validators.required]);
			groupVideo['src_' + l.key] = new FormControl('', [Validators.required]);
			// groupVideo['order_position_' + l.key] = new FormControl(0);
		});
		this.newVideoForm = this.newVideoFB.group(groupVideo);
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}


	/**
	 * Returns newVideo for save
	 */
	prepareNewVideo(): NewVideo {
		const controls = this.newVideoForm.controls;
		let _newVideo = new NewVideo();
		_newVideo.id_news = this.id_news;

		_newVideo.lang_info_array = {};
		this.currentLangs.forEach(l => {
			_newVideo.lang_info_array[l.id] = {
				'title': controls['title_' + l.key].value,
				'src': controls['src_' + l.key].value,
				// 'order_position': controls['order_position_' + l.key].value,
			};
		});
		_newVideo.lang_info_array = JSON.stringify(_newVideo.lang_info_array);
		return _newVideo;
	}

	/**
	 * Save data
	 */
	onSubmit() {
		this.hasFormErrors = false;
		this.loadingAfterSubmit = false;
		/** check form */;
		const controls = this.newVideoForm.controls;
		this.currentLangs.forEach(l => {
			// console.log("controls['order_position_' + l.key].value", controls['order_position_' + l.key].value);
			if (controls['title_' + l.key].value == ''){
				controls['title_' + l.key].setErrors({ 'required': true });
				controls['title_' + l.key].markAsTouched();
			}
			if (controls['src_' + l.key].value == ''){
				controls['src_' + l.key].setErrors({ 'required': true });
				controls['src_' + l.key].markAsTouched();
			}
			// if (controls['order_position_' + l.key].value == '' || controls['order_position_' + l.key].value < 0){
			// 	controls['order_position_' + l.key].setErrors({ 'required': true });
			// 	controls['order_position_' + l.key].markAsTouched();
			// }
			console.log("valid", this.newVideoForm.valid);
			// return;

			this.changeRef.detectChanges();
		});
		if(this.newVideoForm.invalid) return;

		const editedNewVideo = this.prepareNewVideo();
		this.createNewVideo(editedNewVideo);
	}

	/**
	 * Create newVideo
	 *
	 * @param _newVideo: NewVideo
	 */
	createNewVideo(_newVideo: NewVideo) {
		this.loadingAfterSubmit = true;
		this.viewLoading = true;
			this.newService.createNewVideo(_newVideo).subscribe(res => {
			if (!res) {
				return;
			}
	
			this.viewLoading = false;
			this.dialogRef.close({
				res,
				isEdit: false
			});
		});
	}

	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}

	/** UI */
	/**
	 * Returns component title
	 */
	getTitle(): string {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('NEW_VIDEO_MANAGEMENT.ENTITY_MIN') });
		// tslint:disable-next-line:no-string-throw
		return result;
	}

}
