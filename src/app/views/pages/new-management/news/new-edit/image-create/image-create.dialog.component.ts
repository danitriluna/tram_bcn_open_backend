// Angular
import { Component, OnInit, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// RxJS
import { Observable, of, Subscription} from 'rxjs';
// Lodash
import { each, find, some } from 'lodash';
// NGRX
import { Update } from '@ngrx/entity';
// import { Store, select } from '@ngrx/store';
// State
// import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { delay } from 'rxjs/operators';
import { NewImage } from '../../../../../../core/auth/_models/newImage.model';
import { Lang, NewService } from '../../../../../../core/auth';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'kt-image-create-dialog',
	templateUrl: './image-create.dialog.component.html'
})
export class ImageCreateDialogComponent implements OnInit, OnDestroy {
	// Public properties
	id_news: number;
	newImage: NewImage;
	newImage$: Observable<NewImage>;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	newImageForm: FormGroup;
	// Private properties
	private componentSubscriptions: Subscription;
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<ImageCreateDialogComponent>
	 * @param data: any
	 * @param translate: TranslateService
	 */
	constructor(public dialogRef: MatDialogRef<ImageCreateDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private newImageFB: FormBuilder,
		private newService: NewService,
		private changeRef: ChangeDetectorRef, 
		private translate: TranslateService) { 
			this.id_news = data.newId;
			this.currentLangs = data.currentLangs;
			console.log("this.currentLangs", this.currentLangs);
			this.currentLang = this.translate.getDefaultLang();
		}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		let groupImage = {}
		this.currentLangs.forEach(l => {
			groupImage['title_' + l.key] = new FormControl('', [Validators.required]);
			groupImage['img_' + l.key] = new FormControl('', [Validators.required]);
		});
		this.newImageForm = this.newImageFB.group(groupImage);
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}


	/**
	 * Returns newImage for save
	 */
	prepareNewImage(): NewImage {
		const controls = this.newImageForm.controls;
		let _newImage = new NewImage();
		_newImage.id_news = this.id_news;

		_newImage.lang_info_array = {};
		this.currentLangs.forEach(l => {
			_newImage.lang_info_array[l.id] = {
				'title': controls['title_' + l.key].value,
				'img': controls['img_' + l.key].value,
				// 'order': controls['order_' + l.key].value,
			};
		});
		_newImage.lang_info_array = JSON.stringify(_newImage.lang_info_array);
		return _newImage;
	}

	/**
	 * Save data
	 */
	onSubmit() {
		this.hasFormErrors = false;
		this.loadingAfterSubmit = false;
		/** check form */;
		const controls = this.newImageForm.controls;
		this.currentLangs.forEach(l => {
			if (controls['title_' + l.key].value == ''){
				controls['title_' + l.key].setErrors({ 'required': true });
				controls['title_' + l.key].markAsTouched();
			}
			if (controls['img_' + l.key].value == ''){
				controls['img_' + l.key].setErrors({ 'required': true });
				controls['img_' + l.key].markAsTouched();
			}
			// if (controls['order_' + l.key].value == '' || controls['order_' + l.key].value < 0){
			// 	controls['order_' + l.key].setErrors({ 'required': true });
			// 	controls['order_' + l.key].markAsTouched();
			// }
			console.log("valid", this.newImageForm.valid);
			// return;

			this.changeRef.detectChanges();
		});
		if(this.newImageForm.invalid) return;

		const editedNewImage = this.prepareNewImage();
		this.createNewImage(editedNewImage);
	}

	/**
	 * Create newImage
	 *
	 * @param _newImage: NewImage
	 */
	createNewImage(_newImage: NewImage) {
		this.loadingAfterSubmit = true;
		this.viewLoading = true;
			this.newService.createNewImage(_newImage).subscribe(res => {
			if (!res) {
				return;
			}
	
			this.viewLoading = false;
			this.dialogRef.close({
				res,
				isEdit: false
			});
		});
	}

	/**
	 * Close alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}

	/** UI */
	/**
	 * Returns component title
	 */
	getTitle(): string {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('NEW_IMAGE_MANAGEMENT.ENTITY_MIN') });
		// tslint:disable-next-line:no-string-throw
		return result;
	}

	openBrowserSingle(event: any, lang: string) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_new_'+lang) as HTMLElement;
		element.click();
	}

	readFileSingle(event: any, lang: string) {
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);
		// console.log("entra 0");

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			// console.log("entra 1");
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				console.log("entra 2");
				this.newImageForm.get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}
		this.previewSingle(lang);
	}

	previewSingle(lang:string) {
		// Show preview 
		console.log("entra previewUrl");
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[lang] = reader.result;
		}

		console.log("this.previewUrl", this.previewUrl);
	}

	deletePreviewUrlSingle(lang){
		this.previewUrl[lang] = null;
	}

}
