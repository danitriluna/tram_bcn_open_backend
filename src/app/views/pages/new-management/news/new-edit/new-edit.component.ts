// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common'
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { NewService, Lang } from '../../../../../core/auth';
// Services and Models
import {
	New,
	NewUpdated,
	selectNewById,
	NewOnServerCreated,
	selectLastCreatedNewId,
	selectNewsActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import { currentUser, Logout, User } from '../../../../../core/auth';
import {environment } from '../../../../../../environments/environment';
import { MatDialog } from '@angular/material';
import { VideoCreateDialogComponent } from './video-create/video-create.dialog.component';
import { ImageCreateDialogComponent } from './image-create/image-create.dialog.component';

@Component({
	selector: 'kt-new-edit',
	templateUrl: './new-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class NewEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	newInfo: New;
	newInfoId$: Observable<number>;
	oldNew: New;
	selectedTab = 0;
	loading$: Observable<boolean>;
	newInfoForm: FormGroup;
	newVideoForm: FormGroup;
	newVideosForm: FormGroup[] = [];
	newImgForm: FormGroup;
	newImgsForm: FormGroup[] = [];
	hasFormErrors = false;
	viewLoading = true;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';
	removeVideos = [];
	removeImages = [];
	// user$: Observable<User>;
	// user: any; 

	newDate = null;

	/* img */
	previewUrl: any[] = [];
	private fileData: File[] = [];
	private image: string[] = [];
	// @ViewChildren('img') imgComponents: QueryList<File>;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param newInfoFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param newService: NewService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog,
		private newInfoFB: FormBuilder,
		private subheaderService: SubheaderService,
		private layoutUtilsService: LayoutUtilsService,
		private store: Store<AppState>,
		private translate: TranslateService, 
		private changeRef: ChangeDetectorRef, 
		private newService: NewService,
		private loadingBlockService: LoadingBlockService,
		public datepipe: DatePipe) {

		this.currentLang = this.translate.getDefaultLang();
		this.loadingBlockService.show();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	getValue(obj: Observable<any>){
	let value: any;
	obj.subscribe(v => value = v);
	return value;
	}

	/**
	 * On init
	 */
	ngOnInit() {
		let that = this;
		// this.user$ = this.store.pipe(select(currentUser));
		// this.user$.subscribe(params => {
		// 	that.user = params;
		// });
		
		this.loading$ = this.store.pipe(select(selectNewsActionLoading));
		console.log("this.loading$", this.loading$);

		
		this.currentLangs$ = this.store.pipe(select(currentLangs));
		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;

			console.log("currentLangs", langs);

			that.viewLoading = true;

			const routeSubscription =  that.activatedRoute.params.subscribe(params => {
				const id = params.id;
				console.log('%%%%%%% params', params);
				if (id && id > 0) {
					
					this.newService.getNewById(id).subscribe(res => {
						this.newInfo = res;
						console.log('=====================this.newInfo', this.newInfo);
						this.oldNew = Object.assign({}, this.newInfo);
						this.initNew();
					});
				} else {

					console.log('%%%%%%%  ADD  %%%%%%');
					this.newInfo = new New();
					this.newInfo.clear();
					this.oldNew = Object.assign({}, this.newInfo);
					this.initNew();
				}

			});
			that.subscriptions.push(routeSubscription);
		});
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	resetInfo(id, isVideo){
		let that = this;
		this.newService.getNewById(id).subscribe(res => {
			console.log('REFRESH=====================this.newInfo', this.newInfo);
			if(isVideo){
				that.createFormNewVideo(res);
			}else{
				that.createFormNewImg(res);
			}
			that.newInfo = res;
			this.changeRef.detectChanges();
		});
	}

	/**
	 * Init newInfo
	 */
	initNew() {
		console.log('===initNew==================this.newInfo', this.newInfo);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.newInfo.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN') }), page: `newInfo-management` },
				{
					title: this.translate.instant('NEW_MANAGEMENT.ENTITIES'),  page: `new-management/news` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN') }), page: `newInfo-management/news/add` }
			]);
			return;
		}

		this.newDate = this.newInfo.date_publication ? this.newInfo.date_publication : null;

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN') }), page: `newInfo-management` },
			{ title: this.translate.instant('NEW_MANAGEMENT.ENTITIES'),  page: `newInfo-management/news` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN') }), page: `newInfo-management/news/edit`, queryParams: { id: this.newInfo.id } }
		]);

	}

	/**
	 * Create form
	 */
	createForm() {

		let group = {}  
		
		group['date_publication'] = this.newInfo.date_publication;

		this.currentLangs.forEach(l =>{
			group['title_' + l.key] = new FormControl((this.newInfo.lang_info_array[l.key]) ? this.newInfo.lang_info_array[l.key].title : '', [Validators.required]);
			group['subtitle_' + l.key] = new FormControl((this.newInfo.lang_info_array[l.key]) ? this.newInfo.lang_info_array[l.key].subtitle : '', [Validators.required]);
			group['text_' + l.key] = new FormControl((this.newInfo.lang_info_array[l.key]) ? this.newInfo.lang_info_array[l.key].text : '', [Validators.required]);
	    });

		this.newInfoForm = this.newInfoFB.group(group);
		console.log("@@@@@@@@@@@@@@@@@@@@this.newInfoForm", this.newInfoForm);

		this.newInfo.video_info.forEach(v => {
			let groupVideos = {}
			groupVideos['id'] = new FormControl(v.id);
			this.currentLangs.forEach(l => {
				groupVideos['title_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].title : '', [Validators.required]);
				groupVideos['src_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].src : '', [Validators.required]);
				groupVideos['order_position_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].order_position : '', [Validators.required]);
			});
			this.newVideosForm[v.id] = (this.newInfoFB.group(groupVideos));
		});
		console.log("@@@@@@@@@@@@@@@@@@@@this.newVideosForm", this.newVideosForm);

		this.newInfo.img_info.forEach(v => {
			let groupImgs = {}
			groupImgs['id'] = new FormControl(v.id);
			groupImgs['featured'] = new FormControl((v.featured) ? v.featured : '', [Validators.required]);
			this.currentLangs.forEach(l => {
				groupImgs['title_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].title : '', [Validators.required]);
				groupImgs['img_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].img : '', [Validators.required]);
				groupImgs['order_position_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].order_position : '', [Validators.required]);
			});
			this.newImgsForm[v.id] = (this.newInfoFB.group(groupImgs));
			this.previewUrl[v.id] = [];
		});
		console.log("@@@@@@@@@@@@@@@@@@@@this.newImgsForm", this.newImgsForm);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	createFormNewVideo(newInfo) {

		let v = newInfo.video_info[newInfo.video_info.length - 1];

		console.log("v",v);

		let groupVideos = {}
		groupVideos['id'] = new FormControl(v.id);
		this.currentLangs.forEach(l => {
			groupVideos['title_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].title : '', [Validators.required]);
			groupVideos['src_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].src : '', [Validators.required]);
			groupVideos['order_position_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].order_position : '', [Validators.required]);
		});
		this.newVideosForm[v.id] = (this.newInfoFB.group(groupVideos));

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	createFormNewImg(newInfo) {

		let v = newInfo.img_info[newInfo.img_info.length - 1];

		let groupImage = {}
		groupImage['id'] = new FormControl(v.id);
		groupImage['featured'] = new FormControl(0);
		this.currentLangs.forEach(l => {
			groupImage['title_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].title : '', [Validators.required]);
			groupImage['img_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].img : '', [Validators.required]);
			groupImage['order_position_' + l.key] = new FormControl((v.lang_info_array[l.key]) ? v.lang_info_array[l.key].order_position : '', [Validators.required]);
		});
		this.previewUrl[v.id] = [];
		this.newImgsForm[v.id] = (this.newInfoFB.group(groupImage));

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/new-management/news`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh newInfo
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshNew(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/new-management/news/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.newInfo = Object.assign({}, this.oldNew);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.newInfoForm.markAsPristine();
		this.newInfoForm.markAsUntouched();
		this.newInfoForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.newInfoForm.controls;
		/** check form */
		if (this.newInfoForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedNew = this.prepareNew();

		if (editedNew.id > 0) {
			this.updateNew(editedNew, withBack);
			return;
		}

		this.addNew(editedNew, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareNew(): New {
		console.log("prepare");
		const that = this;
		const controls = this.newInfoForm.controls;
		console.log("controls ", controls);
		const _newInfo = new New();
		_newInfo.clear();
		_newInfo.id = this.newInfo.id;
		_newInfo.date_publication = this.datepipe.transform(controls['date_publication'].value, 'dd-MM-yyyy');
		// _newInfo.user = this.user.id;
		_newInfo.lang_info_array = { };
		this.currentLangs.forEach(l => {
			_newInfo.lang_info_array[l.id] = { 
				'title': controls['title_' + l.key].value,
				'subtitle': controls['subtitle_' + l.key].value,
				'text': controls['text_' + l.key].value,
				// 'img': (controls['img_' + l.key].value ? controls['img_' + l.key].value : (this.newInfo.lang_info_array[l.key].img ? this.newInfo.lang_info_array[l.key].img : '')),
			};
		});
		_newInfo.lang_info_array = JSON.stringify(_newInfo.lang_info_array);

		_newInfo.video_info_array = {};

		this.newInfo.video_info.forEach(v => {
			_newInfo.video_info_array[v.id] = {};
			
			let videoControls = that.newVideosForm[v.id].controls;
			this.currentLangs.forEach(l => {
				_newInfo.video_info_array[v.id][l.id] = {
					'title': videoControls['title_' + l.key].value,
					'src': videoControls['src_' + l.key].value,
					'order_position': videoControls['order_position_' + l.key].value,
				};
			});
		});

		this.removeVideos.forEach(v => {
			_newInfo.video_info_array[v] = {
				'remove' : true
			};
		});

		console.log("_newInfo.video_info_array", _newInfo.video_info_array);

		_newInfo.video_info_array = JSON.stringify(_newInfo.video_info_array);

		_newInfo.img_info_array = {};

		this.newInfo.img_info.forEach(v => {
			_newInfo.img_info_array[v.id] = {};
			let imgControls = that.newImgsForm[v.id].controls;
			_newInfo.img_info_array[v.id].featured = (imgControls['featured'].value)? 1 : 0;
			this.currentLangs.forEach(l => {
				_newInfo.img_info_array[v.id][l.id] = {
					'title': imgControls['title_' + l.key].value,
					'img': imgControls['img_' + l.key].value,
					'order_position': imgControls['order_position_' + l.key].value,
				};
			});
		});

		this.removeImages.forEach(v => {
			_newInfo.img_info_array[v] = {
				'remove' : true
			};
		});

		_newInfo.img_info_array = JSON.stringify(_newInfo.img_info_array);

		return _newInfo;
	}

	/**
	 * Add New
	 *
	 * @param _newInfo: New
	 * @param withBack: boolean
	 */
	addNew(_newInfo: New, withBack: boolean = false) {
		this.store.dispatch(new NewOnServerCreated({ new: _newInfo }));
		const addSubscription = this.store.pipe(select(selectLastCreatedNewId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('NEW_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('COMMON.LOADING');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshNew(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update newInfo
	 *
	 * @param _newInfo: New
	 * @param withBack: boolean
	 */
	updateNew(_newInfo: New, withBack: boolean = false) {
		// Update New
		// tslint:disable-next-line:prefer-const

		const updatedNew: Update<New> = {
			id: _newInfo.id,
			changes: _newInfo
		};
		this.store.dispatch(new NewUpdated( { partialNew: updatedNew, new: _newInfo }));
		const message = this.translate.instant('NEW_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshNew(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN') });
		if (!this.newInfo || !this.newInfo.id) {
			return result;
		}
		let name = (this.newInfo.lang_info_array[this.currentLang] && this.newInfo.lang_info_array[this.currentLang].title) ? this.newInfo.lang_info_array[this.currentLang].title : this.newInfo.id;
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('NEW_MANAGEMENT.ENTITY_MIN'), entity_name: name });

		// result = `Edit newInfo - ${this.newInfo.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any, lang: string, id: any) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img_'+id+'_'+lang) as HTMLElement;
		element.click();
	}

	readFile(event: any, lang: string, id: any) {
		console.log("@@@@@@@@@@@@@ 22 readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData[lang] = event.target.files[0];
			reader.readAsDataURL(this.fileData[lang]);
			reader.onload = () => {
				this.newImgsForm[id].get('img_'+lang).setValue({
					filename: this.fileData[lang].name,
					filetype: this.fileData[lang].type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}
		this.preview(lang, id);
	}

	preview(lang:string, id: any) {
		// Show preview 
		var mimeType = this.fileData[lang].type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData[lang]);
		reader.onload = (_event) => {
			this.previewUrl[id][lang] = reader.result;
		}
	}

	deletePreviewUrl(lang, id){
		this.newImgsForm[id].get('img_'+lang).setValue('');
		this.previewUrl[id][lang] = null;
	}

	deleteImg(lang, id){
		let that = this;
		this.newInfo.img_info.forEach(function (item, index) {
			if(item.id == id){
				item.lang_info_array[lang].src = "";
			}
		});

		this.newImgsForm[id].get('img_'+lang).setValue('');
	}

	/* VIDEO */
	createVideo(){
		let id = this.newInfo.id;
		const dialogRef = this.dialog.open(VideoCreateDialogComponent, { data: { newId: this.newInfo.id, currentLangs: this.currentLangs } });
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.loadingBlockService.show();
			this.layoutUtilsService.showActionNotification(this.translate.instant('NEW_MANAGEMENT.EDIT.UPDATE_MESSAGE'), MessageType.Create, 10000, false, false);
			this.resetInfo(id, true);
			this.changeRef.detectChanges();
			this.loadingBlockService.hide();
		});
	}

	/* Image */
	createImage(){
		let id = this.newInfo.id;
		const dialogRef = this.dialog.open(ImageCreateDialogComponent, { data: { newId: this.newInfo.id, currentLangs: this.currentLangs } });
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(this.translate.instant('NEW_MANAGEMENT.EDIT.UPDATE_MESSAGE'), MessageType.Create, 10000, false, false);
			this.resetInfo(id, false);
			this.changeRef.detectChanges();
			this.loadingBlockService.hide();
		});
	}

	deleteElementVideo(index, id){
		this.newInfo.video_info.splice(index, 1);
		this.removeVideos.push(id);
		console.log("removeVideos",this.removeVideos);
	}

	deleteElementImage(index, id){
		this.newInfo.img_info.splice(index, 1);
		this.removeImages.push(id);
		console.log("removeImage",this.removeImages);
	}
}
