// Angular
import { NgModule } from '@angular/core';
import { DatePipe, CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { PartialsModule } from '../../partials/partials.module';
// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../../../core/_base/crud';
// Shared
import { ActionNotificationComponent, DeleteEntityDialogComponent } from '../../partials/content/crud';
// Components
import { NewManagementComponent } from './new-management.component';
import { NewsListComponent } from './news/news-list/news-list.component';
import { NewEditComponent } from './news/new-edit/new-edit.component';
import { NewImagesListComponent } from './news/new-images/new-images-list.component';
import { VideoCreateDialogComponent } from './news/new-edit/video-create/video-create.dialog.component';
import { ImageCreateDialogComponent } from './news/new-edit/image-create/image-create.dialog.component';

import { IntlPaginator } from '../../../core/_base/crud/utils/intl-paginator';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatPaginatorIntl,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import {
	newsReducer,
	NewEffects
} from '../../../core/auth';

export const DateFormats = {
	parse: {
		dateInput: ['DD-MM-YYYY']
	},
	display: {
		dateInput: 'DD-MM-YYYY',
		monthYearLabel: 'MMM YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'MMMM YYYY',
	},
};

const routes: Routes = [
	{
		path: '',
		component: NewManagementComponent,
		children: [
			{
				path: '',
				redirectTo: 'news',
				pathMatch: 'full'
			},
			{
				path: 'news',
				component: NewsListComponent
			},
			{
				path: 'news:id',
				component: NewsListComponent
			},
			{
				path: 'news/add',
				component: NewEditComponent
			},
			{
				path: 'news/add:id',
				component: NewEditComponent
			},
			{
				path: 'news/edit',
				component: NewEditComponent
			},
			{
				path: 'news/edit/:id',
				component: NewEditComponent
			},
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		AngularEditorModule,
		PartialsModule,
		RouterModule.forChild(routes),
		StoreModule.forFeature('news', newsReducer),
        EffectsModule.forFeature([NewEffects]),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
        MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	providers: [
		InterceptService,
		{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'kt-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		{
			provide: MatPaginatorIntl,
			deps: [TranslateService],
			useFactory: (translateService: TranslateService) => new IntlPaginator(translateService).getI18nPaginatorIntl()
		},
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService,
		DatePipe,
		{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: DateFormats }
	],
	entryComponents: [
		ActionNotificationComponent,
		DeleteEntityDialogComponent,
		VideoCreateDialogComponent,
		ImageCreateDialogComponent
	],
	declarations: [
		NewManagementComponent,
		NewsListComponent,
		NewEditComponent,
		VideoCreateDialogComponent,
		ImageCreateDialogComponent,
		NewImagesListComponent
	]
})
export class NewManagementModule {}
