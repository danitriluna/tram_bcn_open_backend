import { Component, OnInit } from '@angular/core';
import { Logout } from '../../../../core/auth';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'logout',
    templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {

	/**
	 * Component constructor
	 *
	 * @param router: Router
     */
    constructor(private router: Router){

    }

    ngOnInit() {
        // alert("LOGOUT");
        localStorage.removeItem('auth_app_token');
        localStorage.clear();

        this.router.navigateByUrl('/auth/login');
    }

}