// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { PartialsModule } from '../../partials/partials.module';
// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../../../core/_base/crud';
// Shared
import { ActionNotificationComponent, DeleteEntityDialogComponent } from '../../partials/content/crud';
// Components
import { TeamManagementComponent } from './team-management.component';
import { TeamsListComponent } from './teams/teams-list/teams-list.component';
import { TeamEditComponent } from './teams/team-edit/team-edit.component';

import { IntlPaginator } from '../../../core/_base/crud/utils/intl-paginator';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatPaginatorIntl,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import {
	teamsReducer,
	TeamEffects
} from '../../../core/auth';

const routes: Routes = [
	{
		path: '',
		component: TeamManagementComponent,
		children: [
			{
				path: '',
				redirectTo: 'teams',
				pathMatch: 'full'
			},
			{
				path: 'teams',
				component: TeamsListComponent
			},
			{
				path: 'teams:id',
				component: TeamsListComponent
			},
			{
				path: 'teams/add',
				component: TeamEditComponent
			},
			{
				path: 'teams/add:id',
				component: TeamEditComponent
			},
			{
				path: 'teams/edit',
				component: TeamEditComponent
			},
			{
				path: 'teams/edit/:id',
				component: TeamEditComponent
			},
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		StoreModule.forFeature('teams', teamsReducer),
        EffectsModule.forFeature([TeamEffects]),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
        MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	providers: [
		InterceptService,
		{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'kt-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		{
			provide: MatPaginatorIntl,
			deps: [TranslateService],
			useFactory: (translateService: TranslateService) => new IntlPaginator(translateService).getI18nPaginatorIntl()
		},
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		ActionNotificationComponent,
		DeleteEntityDialogComponent
	],
	declarations: [
		TeamManagementComponent,
		TeamsListComponent,
		TeamEditComponent
	]
})
export class TeamManagementModule {}
