// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { MatchService, UserService, Lang, User } from '../../../../../core/auth';
// Services and Models
import {
	Team,
	TeamUpdated,
	selectTeamById,
	TeamOnServerCreated,
	selectLastCreatedTeamId,
	selectTeamsActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';

@Component({
	selector: 'kt-team-edit',
	templateUrl: './team-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class TeamEditComponent implements OnInit, OnDestroy {
	// Public properties
	team: Team;
	teamId$: Observable<number>;
	oldTeam: Team;
	selectedTab = 0;
	loading$: Observable<boolean>;
	teamForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;

	player1Id = 0;
	player2Id = 0;
	allPlayers: User[] = [];
	// Private properties
	private subscriptions: Subscription[] = [];


	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param teamFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param matchService: MatchService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private teamFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
					private matchService: MatchService,
		private changeRef: ChangeDetectorRef,
		private userService: UserService,
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		this.loadingBlockService.show();
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectTeamsActionLoading));


		this.viewLoading = true;

		const routeSubscription =  this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			this.userService.getAllPlayers().subscribe((res: User[]) => {
				each(res, (_u: User) => {
					this.allPlayers.push(_u);
				});
				if (id && id > 0) {
					
	
					this.store.pipe(select(selectTeamById(id))).subscribe(res => {
						if (res) {
							this.team = res;
							console.log('this.team', this.team);
							this.oldTeam = Object.assign({}, this.team);
							this.player1Id = this.team.player1.id;
							this.player2Id = this.team.player2.id;
							this.initTeam();
						} else { // F5
							this.matchService.getTeamById(id).subscribe(res => {
								this.team = res;
								console.log('=====================this.team', this.team);
								this.oldTeam = Object.assign({}, this.team);
								this.initTeam();
							});
						}
					});
				} else {
	
					console.log('%%%%%%%  ADD  %%%%%%');
					this.team = new Team();
					this.team.clear();
					this.oldTeam = Object.assign({}, this.team);
					this.initTeam();
				}
			});

		});
		this.subscriptions.push(routeSubscription);
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init team
	 */
	initTeam() {
		console.log('===initTeam==================this.team', this.team);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.team.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('TEAMr_MANAGEMENT.ENTITY_MIN') }), page: `team-management` },
				{
					title: this.translate.instant('TEAM_MANAGEMENT.ENTITIES'),  page: `team-management/teams` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITY_MIN') }), page: `team-management/teams/add` }
			]);
			return;
		}

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITY_MIN') }), page: `team-management` },
			{ title: this.translate.instant('TEAM_MANAGEMENT.ENTITIES'),  page: `team-management/teams` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITY_MIN') }), page: `team-management/teams/edit`, queryParams: { id: this.team.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.team", this.team);
		let group = {}  
		group['player1'] = new FormControl(this.team.player1, [Validators.required]);
		group['player2'] = new FormControl(this.team.player2, [Validators.required]);

		this.teamForm = this.teamFB.group(group);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/team-management/teams`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh team
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshTeam(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/team-management/teams/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.team = Object.assign({}, this.oldTeam);
		this.createForm();
		this.hasFormErrors = false;
		this.teamForm.markAsPristine();
		this.teamForm.markAsUntouched();
		this.teamForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.teamForm.controls;
		/** check form */
		if (this.teamForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedTeam = this.prepareTeam();

		if (editedTeam.id > 0) {
			this.updateTeam(editedTeam, withBack);
			return;
		}

		this.addTeam(editedTeam, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareTeam(): Team {
		console.log("prepare");
		const controls = this.teamForm.controls;
		console.log("controls ", controls);
		const _team = new Team();
		_team.clear();
		_team.id = this.team.id;
		_team.player1 = controls.player1.value;
		_team.player2 = controls.player2.value;
		
		return _team;
	}

	/**
	 * Add Team
	 *
	 * @param _team: Team
	 * @param withBack: boolean
	 */
	addTeam(_team: Team, withBack: boolean = false) {
		this.store.dispatch(new TeamOnServerCreated({ team: _team }));
		const addSubscription = this.store.pipe(select(selectLastCreatedTeamId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('Team_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('Team_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshTeam(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update team
	 *
	 * @param _team: Team
	 * @param withBack: boolean
	 */
	updateTeam(_team: Team, withBack: boolean = false) {
		// Update Team
		// tslint:disable-next-line:prefer-const

		const updatedTeam: Update<Team> = {
			id: _team.id,
			changes: _team
		};
		this.store.dispatch(new TeamUpdated( { partialTeam: updatedTeam, team: _team }));
		const message = this.translate.instant('TEAM_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshTeam(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITY_MIN') });
		if (!this.team || !this.team.id) {
			return result;
		}
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('TEAM_MANAGEMENT.ENTITY_MIN'), entity_name: this.team.id });

		// result = `Edit team - ${this.team.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


}
