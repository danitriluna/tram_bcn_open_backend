// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Lodash
import { each, find, remove } from 'lodash';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { UserService, Role, selectAllRoles } from '../../../../../core/auth/';
// Services and Models
import {
	User,
	UserUpdated,
	selectHasUsersInStore,
	selectUserById,
	UserOnServerCreated,
	selectLastCreatedUserId,
	selectUsersActionLoading
} from '../../../../../core/auth';

@Component({
	selector: 'kt-user-edit',
	templateUrl: './user-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
})
export class UserEditComponent implements OnInit, OnDestroy {
	// Public properties
	user: User;
	userId$: Observable<number>;
	oldUser: User;
	selectedTab = 0;
	loading$: Observable<boolean>;
	// rolesSubject = new BehaviorSubject<number[]>([]);
	userForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];

	currentRoleId:number = 0;
	allUserRoles$: Observable<Role[]>;
	allRoles: Role[] = [];
	unassignedRoles: Role[] = [];
	assignedRoles: Role[] = [];

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param userFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param userService: UserService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private userFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
		private changeRef: ChangeDetectorRef, 
		private userService: UserService,
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		this.loadingBlockService.show();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		this.loading$ = this.store.pipe(select(selectUsersActionLoading));

		this.viewLoading = true;

		const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			if (id && id > 0) {


				this.store.pipe(select(selectUserById(id))).subscribe(res => {
					if (res) {
						this.user = res;
						console.log('this.user', this.user);
						// this.rolesSubject.next(this.user.roles);
						this.oldUser = Object.assign({}, this.user);
						this.initUser();
					} else { // F5
						this.userService.getUserById(id).subscribe(res => {
							this.user = res;
							console.log('=====================this.user', this.user);
							// this.rolesSubject.next(this.user.roles);
							this.oldUser = Object.assign({}, this.user);
							this.initUser();
							this.refreshUser(false);
						});
					}
				});
			} else {

				console.log('%%%%%%%  ADD  %%%%%%');
				this.user = new User();
				this.user.clear();
				// this.rolesSubject.next(this.user.roles);
				this.oldUser = Object.assign({}, this.user);
				this.initUser();
			}

		});
		this.subscriptions.push(routeSubscription);

		this.allUserRoles$ = this.store.pipe(select(selectAllRoles));
		this.allUserRoles$.subscribe((res: Role[]) => {
			each(res, (_role: Role) => {
				console.log('allUserRoles _role', _role);
				this.allRoles.push(_role);
			});

			// each(this.rolesSubject.value, (roleId: number) => {
			// 	each(this.rolesSubject.value, (roleId: string) => {
			// 	console.log('subject roleId', roleId);
			// 	const role = find(this.allRoles, (_role: Role) => {
			// 		return _role.id === roleId;
			// 	});

			// });
		});
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init user
	 */
	initUser() {
		console.log('===initUser==================this.user', this.user);

		if (this.user.roles) {
			this.currentRoleId = this.user.roles[0];
		}
		
		this.createForm();
			this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('USER_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.user.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('USER_MANAGEMENT.ENTITY_MIN') }), page: `user-management` },
				{ title: this.translate.instant('USER_MANAGEMENT.ENTITIES'),  page: `user-management/users` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('USER_MANAGEMENT.ENTITY_MIN') }), page: `user-management/users/add` }
			]);
			return;
		}

		console.log('===ROLES==================this.user', this.user);
		console.log('===currentRoleId==================this.currentRoleId', this.currentRoleId);

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('USER_MANAGEMENT.ENTITY_MIN') }), page: `user-management` },
			{ title: this.translate.instant('USER_MANAGEMENT.ENTITIES'),  page: `user-management/users` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('USER_MANAGEMENT.ENTITY_MIN') }), page: `user-management/users/edit`, queryParams: { id: this.user.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.user", this.user);
		this.userForm = this.userFB.group({
			username: [this.user.username, [Validators.required, this.noWhitespaceValidator, Validators.pattern('^[A-Za-z0-9\-_]+$')]],
			name: [this.user.name, Validators.required],
			roles: [(this.user.roles) ? parseInt(this.user.roles[0]) : this.currentRoleId, Validators.required],
			last_name1: [this.user.last_name1],
			last_name2: [this.user.last_name2],
			email: [this.user.email, [Validators.email, Validators.required]],
			password: [this.user.password]
		});
		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/user-management/users`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh user
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshUser(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/user-management/users/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.user = Object.assign({}, this.oldUser);
		this.createForm();
		this.hasFormErrors = false;
		this.userForm.markAsPristine();
  this.userForm.markAsUntouched();
  this.userForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.userForm.controls;
		/** check form */
		if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedUser = this.prepareUser();

		if (editedUser.id > 0) {
			this.updateUser(editedUser, withBack);
			return;
		}

		this.addUser(editedUser, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareUser(): User {
		console.log("prepare");
		const controls = this.userForm.controls;
		const _user = new User();
		_user.clear();
		// _user.roles = this.rolesSubject.value;
		_user.roles = [controls.roles.value];
		_user.access_token = this.user.access_token;
		_user.refreshToken = this.user.refreshToken;
		_user.pic = this.user.pic;
		_user.id = this.user.id;
		_user.username = controls.username.value;
		_user.email = controls.email.value;
		_user.name = controls.name.value;
		_user.last_name1 = controls.last_name1.value;
		_user.last_name2 = controls.last_name2.value;
		_user.password = this.user.password;
		return _user;
	}

	/**
	 * Add User
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	addUser(_user: User, withBack: boolean = false) {
		this.store.dispatch(new UserOnServerCreated({ user: _user }));
		const addSubscription = this.store.pipe(select(selectLastCreatedUserId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('USER_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('USER_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshUser(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update user
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	updateUser(_user: User, withBack: boolean = false) {
		// Update User
		// tslint:disable-next-line:prefer-const

		const updatedUser: Update<User> = {
			id: _user.id,
			changes: _user
		};
		this.store.dispatch(new UserUpdated( { partialUser: updatedUser, user: _user }));
		const message = this.translate.instant('USER_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshUser(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('USER_MANAGEMENT.USER_MIN') });
		if (!this.user || !this.user.id) {
			return result;
		}
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('USER_MANAGEMENT.USER_MIN'), entity_name: this.user.name });

		// result = `Edit user - ${this.user.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


}
