// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// Translate
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { PartialsModule } from '../../partials/partials.module';
// Services
import { HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../../../core/_base/crud';
// Shared
import { ActionNotificationComponent, DeleteEntityDialogComponent } from '../../partials/content/crud';
// Components
import { UserManagementComponent } from './user-management.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UserEditComponent } from './users/user-edit/user-edit.component';
import { RolesListComponent } from './roles/roles-list/roles-list.component';
import { RoleEditDialogComponent } from './roles/role-edit/role-edit.dialog.component';
// import { UserRolesListComponent } from './users/_subs/user-roles/user-roles-list.component';
import { ArbitratorsListComponent } from './arbitrators/arbitrators-list/arbitrators-list.component';
import { ArbitratorEditComponent } from './arbitrators/arbitrator-edit/arbitrator-edit.component';
import { PlayersListComponent } from './players/players-list/players-list.component';
import { PlayerEditComponent } from './players/player-edit/player-edit.component';
import { ChangePasswordComponent } from './users/_subs/change-password/change-password.component';

import { IntlPaginator } from '../../../core/_base/crud/utils/intl-paginator';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatPaginatorIntl,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import {
	usersReducer,
	UserEffects
} from '../../../core/auth';

const routes: Routes = [
	{
		path: '',
		component: UserManagementComponent,
		children: [
			{
				path: '',
				redirectTo: 'users',
				pathMatch: 'full'
			},
			{
				path: 'arbitrators',
				component: ArbitratorsListComponent
			},
			{
				path: 'arbitrators:id',
				component: ArbitratorsListComponent
			},
			{
				path: 'arbitrators/add',
				component: ArbitratorEditComponent
			},
			{
				path: 'arbitrators/add:id',
				component: ArbitratorEditComponent
			},
			{
				path: 'arbitrators/edit',
				component: ArbitratorEditComponent
			},
			{
				path: 'arbitrators/edit/:id',
				component: ArbitratorEditComponent
			},
			{
				path: 'players',
				component: PlayersListComponent
			},
			{
				path: 'players:id',
				component: PlayersListComponent
			},
			{
				path: 'players/add',
				component: PlayerEditComponent
			},
			{
				path: 'players/add:id',
				component: PlayerEditComponent
			},
			{
				path: 'players/edit',
				component: PlayerEditComponent
			},
			{
				path: 'players/edit/:id',
				component: PlayerEditComponent
			},
			{
				path: 'roles',
				component: RolesListComponent
			},
			{
				path: 'users',
				component: UsersListComponent
			},
			{
				path: 'users:id',
				component: UsersListComponent
			},
			{
				path: 'users/add',
				component: UserEditComponent
			},
			{
				path: 'users/add:id',
				component: UserEditComponent
			},
			{
				path: 'users/edit',
				component: UserEditComponent
			},
			{
				path: 'users/edit/:id',
				component: UserEditComponent
			},
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		PartialsModule,
		RouterModule.forChild(routes),
		StoreModule.forFeature('users', usersReducer),
        EffectsModule.forFeature([UserEffects]),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
        MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatExpansionModule,
		MatTabsModule,
		MatTooltipModule,
		MatDialogModule
	],
	providers: [
		InterceptService,
		{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
			multi: true
		},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'kt-mat-dialog-container__wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		{ 
			provide: MatPaginatorIntl, 
			deps: [TranslateService], 
			useFactory: (translateService: TranslateService) => new IntlPaginator(translateService).getI18nPaginatorIntl()
		},
		HttpUtilsService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		ActionNotificationComponent,
		RoleEditDialogComponent,
		DeleteEntityDialogComponent
	],
	declarations: [
		UserManagementComponent,
		UsersListComponent,
		UserEditComponent,
		RolesListComponent,
		RoleEditDialogComponent,
		ArbitratorsListComponent,
		ArbitratorEditComponent,
		PlayersListComponent,
		PlayerEditComponent,
		ChangePasswordComponent
	]
})
export class UserManagementModule {}
