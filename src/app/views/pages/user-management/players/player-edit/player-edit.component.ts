// Angular
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Lodash
import { each, find, remove } from 'lodash';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { UserService, CommonService } from '../../../../../core/auth';
import { environment } from '../../../../../../environments/environment';
// Services and Models
import {
	User,
	UserUpdated,
	selectHasUsersInStore,
	selectUserById,
	UserOnServerCreated,
	selectLastCreatedUserId,
	selectUsersActionLoading,
	Surface
} from '../../../../../core/auth';
import { catchError } from 'rxjs/operators';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
	selector: 'kt-player-edit',
	templateUrl: './player-edit.component.html',
	// changeDetection: ChangeDetectionStrategy.Default,
	providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' }, 
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
	{ provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
	],
})
export class PlayerEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	user: User;
	userId$: Observable<number>;
	oldUser: User;
	selectedTab = 0;
	loading$: Observable<boolean>;
	userForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];

	currentRoleId: number = environment.role_player;

	allSurfaces: Surface[] = [];
	currentSurfaceId = 0;
	currentSexId = 0;
	currentPlayId = 0;
	currentDateBestRanking = '';
	currentDateBestRankingDoubles = '';

	/* img */
	previewUrl: any = null;
	private fileData: File = null;
	private image = '';

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param userFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param userService: UserService
	 * @param commonService: CommonService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private userFB: FormBuilder,
		           private subheaderService: SubheaderService,
		           private layoutUtilsService: LayoutUtilsService,
					private store: Store<AppState>,
					private translate: TranslateService, 
					private userService: UserService,
		private changeRef: ChangeDetectorRef,
		private commonService: CommonService,
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		this.loadingBlockService.show();

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		this.loading$ = this.store.pipe(select(selectUsersActionLoading));

		this.viewLoading = true;

		const routeSubscription = this.activatedRoute.params.subscribe(params => {
			const id = params.id;
			console.log('%%%%%%% params', params);
			

			this.commonService.getAllSurfaces().subscribe((res: Surface[]) => {
				each(res, (_ug: Surface) => {
					this.allSurfaces.push(_ug);

				});
				if (id && id > 0) {


					// this.store.pipe(select(selectUserById(id))).subscribe(res => {
					// 	if (res) {
					// 		this.user = res;
					// 		console.log('this.user', this.user);
					// 		this.oldUser = Object.assign({}, this.user);
					// 		this.initUser();
					// 	} else { // F5
							this.userService.getUserById(id).subscribe(res => {
								this.user = res;
								console.log('=====================this.user', this.user);
								this.oldUser = Object.assign({}, this.user);
								this.initUser();
								this.refreshUser(false);
							});
						// }
					// });
				} else {

					console.log('%%%%%%%  ADD  %%%%%%');
					this.user = new User();
					this.user.clear();
					this.oldUser = Object.assign({}, this.user);
					this.initUser();
				}
			});
		});
		this.subscriptions.push(routeSubscription);

	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init user
	 */
	initUser() {
		console.log('===initUser==================this.user', this.user);

		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.user.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN') }), page: `user-management` },
				{
					title: this.translate.instant('PLAYER_MANAGEMENT.ENTITIES'), page: `user-management/players` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN') }), page: `user-management/players/add` }
			]);
			return;
		}
		if (this.user.lang_player_info.surface) {
			this.currentSurfaceId = this.user.lang_player_info.surface.id;
		}
		this.currentSexId = this.user.lang_player_info.sex;
		console.log("currentSexId", this.currentSexId);
		this.currentPlayId = this.user.lang_player_info.play;
		this.currentDateBestRanking = this.user.lang_player_info.date_best_ranking
		this.currentDateBestRankingDoubles = this.user.lang_player_info.date_best_ranking_doubles

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN') }), page: `user-management` },
			{ title: this.translate.instant('PLAYER_MANAGEMENT.ENTITIES'), page: `user-management/players` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN') }), page: `user-management/players/edit`, queryParams: { id: this.user.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.user", this.user);
		this.userForm = this.userFB.group({
			username: [this.user.username, [Validators.required, this.noWhitespaceValidator, Validators.pattern('^[A-Za-z0-9\-_]+$')]],
			name: [this.user.name, Validators.required],
			last_name1: [this.user.last_name1],
			age: [this.user.lang_player_info.age ? this.user.lang_player_info.age : '', [Validators.min(10), Validators.max(100), Validators.required]],
			age_debut: [this.user.lang_player_info.age_debut ? this.user.lang_player_info.age_debut : '', [Validators.min(10), Validators.max(100), Validators.required]],
			birthday_year: [this.user.lang_player_info.birthday_year ? this.user.lang_player_info.birthday_year : '', [Validators.minLength(4), Validators.maxLength(4), Validators.required]],
			play: [this.user.lang_player_info.play ? this.user.lang_player_info.play : '', Validators.required],
			sex: [this.user.lang_player_info.sex!= null ? this.user.lang_player_info.sex : '', Validators.required],
			ranking: [this.user.lang_player_info.ranking ? this.user.lang_player_info.ranking : ''],
			best_ranking: [this.user.lang_player_info.best_ranking ? this.user.lang_player_info.best_ranking : ''],
			date_best_ranking: [this.user.lang_player_info.date_best_ranking ? this.user.lang_player_info.date_best_ranking : ''],
			ranking_doubles: [this.user.lang_player_info.ranking_doubles ? this.user.lang_player_info.ranking_doubles : ''],
			best_ranking_doubles: [this.user.lang_player_info.best_ranking_doubles ? this.user.lang_player_info.best_ranking_doubles : ''],
			date_best_ranking_doubles: [this.user.lang_player_info.date_best_ranking_doubles ? this.user.lang_player_info.date_best_ranking_doubles : ''],
			img: [''],
			email: [this.user.email, [Validators.email, Validators.required]],
			surface: [(this.user.lang_player_info.surface) ? this.user.lang_player_info.surface.id : ''],
			password: [this.user.password]
		});
		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/user-management/players`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh player
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshUser(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/user-management/players/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.user = Object.assign({}, this.oldUser);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.userForm.markAsPristine();
		this.userForm.markAsUntouched();
		this.userForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.userForm.controls;
		/** check form */
		if (this.userForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedUser = this.prepareUser();

		if (editedUser.id > 0) {
			this.updateUser(editedUser, withBack);
			return;
		}

		this.addUser(editedUser, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareUser(): User {
		console.log("prepare");
		const controls = this.userForm.controls;
		const _user = new User();
		_user.clear();
		_user.roles = [this.currentRoleId];
		_user.access_token = this.user.access_token;
		_user.refreshToken = this.user.refreshToken;
		// _user.pic = this.user.pic;
		_user.id = this.user.id;
		_user.username = controls.username.value;
		_user.email = controls.email.value;
		_user.name = controls.name.value;
		_user.last_name1 = controls.last_name1.value;
		// _user.last_name2 = controls.last_name2.value;
		_user.password = this.user.password;
		
		let lang_player_info = {
			'age': controls.age.value,
			'sex': controls.sex.value,
			'play': controls.play.value,
			'ranking': controls.ranking.value,
			'best_ranking': controls.best_ranking.value,
			'date_best_ranking': controls.date_best_ranking.value,
			'ranking_doubles': controls.ranking_doubles.value,
			'best_ranking_doubles': controls.best_ranking_doubles.value,
			'date_best_ranking_doubles': controls.date_best_ranking_doubles.value,
			'age_debut': controls.age_debut.value,
			'birthday_year': controls.birthday_year.value,
			'surface': controls.surface.value,
			'img': (this.previewUrl) ? {
				filename: this.fileData.name,
				filetype: this.fileData.type,
				value: this.previewUrl.toString().split(',')[1]
			} : '',
		};

		_user.lang_player_info = JSON.stringify(lang_player_info);
		console.log("//////////////////// _user", _user);
		return _user;
	}

	/**
	 * Add User
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	addUser(_user: User, withBack: boolean = false) {
		this.store.dispatch(new UserOnServerCreated({ user: _user }));
		const addSubscription = this.store.pipe(select(selectLastCreatedUserId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('PLAYER_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('PLAYER_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshUser(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update user
	 *
	 * @param _user: User
	 * @param withBack: boolean
	 */
	updateUser(_user: User, withBack: boolean = false) {
		// Update User
		// tslint:disable-next-line:prefer-const

		const updatedUser: Update<User> = {
			id: _user.id,
			changes: _user
		};
		this.store.dispatch(new UserUpdated( { partialUser: updatedUser, user: _user }));
		const message = this.translate.instant('PLAYER_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshUser(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN') });
		if (!this.user || !this.user.id) {
			return result;
		}
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('PLAYER_MANAGEMENT.ENTITY_MIN'), entity_name: this.user.name });

		// result = `Edit user - ${this.user.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('img') as HTMLElement;
		element.click();
	}
	readFile(event: any) {
		// this.fileData = <File>event.target.files[0];
		// this.image = this.fileData.name;

		console.log('event.target.files', event.target.files);
		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData = event.target.files[0];
			console.log('fileData',this.fileData);
			reader.readAsDataURL(this.fileData);
			reader.onload = () => {
				// this.userForm.get('img').setValue(reader.result.toString().split(',')[1]);
			};
		}


		this.preview();
	}
	preview() {
		console.log("preview");
		// Show preview 
		var mimeType = this.fileData.type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData);
		reader.onload = (_event) => {
			this.previewUrl = reader.result;
		}
	}


	deletePreviewUrl(lang) {
		this.userForm.controls['img'].setValue('');
		this.previewUrl = null;
	}

	deleteImg(lang) {
		this.user.lang_player_info.img = '';
	}
}
