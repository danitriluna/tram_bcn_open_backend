// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { UserManagementModule } from './user-management/user-management.module';
import { SponsorManagementModule } from './sponsor-management/sponsor-management.module';
import { MatchManagementModule } from './match-management/match-management.module';
import { CourtManagementModule } from './court-management/court-management.module';
import { NewManagementModule } from './new-management/new-management.module';
import { TeamManagementModule } from './team-management/team-management.module';
import { BannerManagementModule } from './banner-management/banner-management.module';
import { CountryManagementModule } from './country-management/country-management.module';
/*XXXXimportXXXX*/

@NgModule({
	declarations: [],
	exports: [],
	imports: [
		CommonModule,
		HttpClientModule,
		FormsModule,
		CoreModule,
		PartialsModule,
		UserManagementModule,
		SponsorManagementModule,
		MatchManagementModule,
		CourtManagementModule,
		NewManagementModule,
		TeamManagementModule,
		BannerManagementModule,
		CountryManagementModule,
/*XXXXXXXX*/
	],
	providers: []
})
export class PagesModule {
}
