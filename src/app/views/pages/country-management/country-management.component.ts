// Angular
import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// AppState
import { AppState } from '../../../core/reducers';
// Auth
import { Permission, Lang } from '../../../core/auth';
import { currentUserPermissions, checkHasUserPermissionByName } from '../../../core/auth/_selectors/auth.selectors';

const countryManagementPermission = 'manageCountries';
@Component({
	selector: 'kt-country-management',
	templateUrl: './country-management.component.html',
	// changeDetection: ChangeDetectionStrategy.Default
})
export class CountryManagementComponent implements OnInit {
	// Public properties
	hasUserAccess$: Observable<boolean>;
	currentUserPermission$: Observable<Permission[]>;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 * @param router: Router
	 */
	constructor(private store: Store<AppState>, private router: Router) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.currentUserPermission$ = this.store.pipe(select(currentUserPermissions));
		this.currentUserPermission$.subscribe(permissions => {
			if (permissions && permissions.length > 0) {
				this.hasUserAccess$ =
					this.store.pipe(select(checkHasUserPermissionByName(countryManagementPermission)));
				this.hasUserAccess$.subscribe(res => {
					if (!res) {
						console.log("SIN PERMISO");
						// this.router.navigateByUrl('/error/403');
					}
				});
			}
		});
	}
}
