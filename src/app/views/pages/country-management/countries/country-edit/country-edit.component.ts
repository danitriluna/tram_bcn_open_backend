// Angular
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
// RxJS
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { AppState } from '../../../../../core/reducers';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Layout
import { SubheaderService, LayoutConfigService, LoadingBlockService } from '../../../../../core/_base/layout';
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Auth
import { CountryService, Lang } from '../../../../../core/auth';
// Services and Models
import {
	Country,
	CountryUpdated,
	selectCountryById,
	CountryOnServerCreated,
	selectLastCreatedCountryId,
	selectCountriesActionLoading
} from '../../../../../core/auth';
import { each } from 'lodash';
import { currentLangs } from '../../../../../core/auth/_selectors/auth.selectors';
import {environment } from '../../../../../../environments/environment';

@Component({
	selector: 'kt-country-edit',
	templateUrl: './country-edit.component.html',
})
export class CountryEditComponent implements OnInit, OnDestroy {
	// Public properties
	baseUrl = environment.baseUrlImg;
	country: Country;
	countryId$: Observable<number>;
	oldCountry: Country;
	selectedTab = 0;
	loading$: Observable<boolean>;
	rolesSubject = new BehaviorSubject<number[]>([]);
	countryForm: FormGroup;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private subscriptions: Subscription[] = [];
	currentLangs$: Observable<Lang[]>;
	currentLangs: Lang[] = [];
	currentLang = '';

	/* img */
	previewUrl: any;
	private fileData: File;
	private image: string[] = [];
	// @ViewChildren('img') imgComponents: QueryList<File>;

	/**
	 * Component constructor
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param countryFB: FormBuilder
	 * @param subheaderService: SubheaderService
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param countryService: CountryService
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private activatedRoute: ActivatedRoute,
		private router: Router,
		private countryFB: FormBuilder,
		private subheaderService: SubheaderService,
		private layoutUtilsService: LayoutUtilsService,
		private store: Store<AppState>,
		private translate: TranslateService, 
		private countryService: CountryService,
		private changeRef: ChangeDetectorRef, 
		private loadingBlockService: LoadingBlockService,
		private layoutConfigService: LayoutConfigService) {
		
		this.loadingBlockService.show();
		this.currentLang = this.translate.getDefaultLang();
    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loading$ = this.store.pipe(select(selectCountriesActionLoading));


		this.currentLangs$ = this.store.pipe(select(currentLangs));

		let dis = this;
		let routeSubscription;

		this.currentLangs$.subscribe(langs => {
			this.currentLangs = langs;
			// langs.forEach((l: Lang) => {
			// 	this.currentLangs.push(l);
			// });
			console.log("currentLangs", langs);


			dis.viewLoading = true;

			routeSubscription =  dis.activatedRoute.params.subscribe(params => {
				const id = params.id;
				console.log('%%%%%%% params', params);
				if (id && id > 0) {
					

					// this.store.pipe(select(selectCountryById(id))).subscribe(res => {
					// 	if (res) {
					// 		this.country = res;
					// 		console.log('this.country', this.country);
					// 		this.oldCountry = Object.assign({}, this.country);
					// 		this.initCountry();
					// 	} else { // F5
							this.countryService.getCountryById(id).subscribe(res => {
								this.country = res;
								console.log('=====================this.country', this.country);
								this.oldCountry = Object.assign({}, this.country);
								this.initCountry();
							});
					// 	}
					// });
				} else {

					console.log('%%%%%%%  ADD  %%%%%%');
					this.country = new Country();
					this.country.clear();
					this.oldCountry = Object.assign({}, this.country);
					this.initCountry();
				}

			});
			dis.subscriptions.push(routeSubscription);
		});
		
	}

	ngOnDestroy() {
		console.log("ngOnDestroy");
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}

	/**
	 * Init country
	 */
	initCountry() {
		console.log('===initCountry==================this.country', this.country);
		
		this.createForm();
		this.subheaderService.setTitle(this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITIES_MIN') }));
		if (!this.country.id) {
			this.subheaderService.setBreadcrumbs([
				{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN') }), page: `country-management` },
				{
					title: this.translate.instant('COUNTRY_MANAGEMENT.ENTITIES'),  page: `country-management/countries` },
				{ title: this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN') }), page: `country-management/countries/add` }
			]);
			return;
		}

		this.subheaderService.setBreadcrumbs([
			{ title: this.translate.instant('MANAGEMENT.COMMON.TITLE', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN') }), page: `country-management` },
			{ title: this.translate.instant('COUNTRY_MANAGEMENT.ENTITIES'),  page: `country-management/countries` },
			{ title: this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN') }), page: `country-management/countries/edit`, queryParams: { id: this.country.id } }
		]);
		this.viewLoading = false;
	}

	/**
	 * Create form
	 */
	createForm() {

		console.log("createForm");
		console.log("this.country", this.country);
		let group = {}  
		// group['name'] = new FormControl(this.country.name, [Validators.required]);
		group['flag'] = new FormControl(this.country.flag, [Validators.required]);

		this.currentLangs.forEach(l =>{
			console.log(" -> name_" + l.key);
			group['name_' + l.key] = new FormControl((this.country.lang_info_array[l.key]) ? this.country.lang_info_array[l.key].name : '', [Validators.required]);
			// group['img_' + l.key] = new FormControl((this.country.lang_info_array[l.key]) ? this.country.lang_info_array[l.key].img : '');
			//group['img_' + l.key] = new FormControl('');
	    });

		this.countryForm = this.countryFB.group(group);
		
		console.log("countryForm", this.countryForm);

		this.loadingBlockService.hide();
		this.changeRef.detectChanges();
	}

	public noWhitespaceValidator(control: FormControl) {
		const hasWhitespace = control.value.trim() !== control.value;
		console.log("////////// hasWhitespace", hasWhitespace);
		const isValid = !hasWhitespace;
		return isValid ? null : { 'whitespace': true };
	}


	/**
	 * Redirect to list
	 *
	 */
	goBackWithId() {
		const url = `/country-management/countries`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Refresh country
	 *
	 * @param isNew: boolean
	 * @param id: number
	 */
	refreshCountry(isNew: boolean = false, id = 0) {
		console.log("refresh");
		let url = this.router.url;
		if (!isNew) {
			this.router.navigate([url], { relativeTo: this.activatedRoute });
			return;
		}

		url = `/country-management/countries/edit/${id}`;
		this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
	}

	/**
	 * Reset
	 */
	reset() {
		this.country = Object.assign({}, this.oldCountry);
		this.previewUrl = null;
		this.createForm();
		this.hasFormErrors = false;
		this.countryForm.markAsPristine();
		this.countryForm.markAsUntouched();
		this.countryForm.updateValueAndValidity();
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSumbit(withBack: boolean = false) {
		console.log("submit");
		this.hasFormErrors = false;
		const controls = this.countryForm.controls;
		/** check form */
		if (this.countryForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}

		const editedCountry = this.prepareCountry();

		if (editedCountry.id > 0) {
			this.updateCountry(editedCountry, withBack);
			return;
		}

		this.addCountry(editedCountry, withBack);
	}

	/**
	 * Returns prepared data for save
	 */
	prepareCountry(): Country {
		console.log("prepare");
		const controls = this.countryForm.controls;
		console.log("controls ", controls);
		const _country = new Country();
		_country.clear();
		_country.id = this.country.id;
		_country.flag = controls['flag'].value;
		_country.lang_info_array = { };
		this.currentLangs.forEach(l => {
			//console.log("this.country.lang_info_array[l.key].img", this.country.lang_info_array[l.key].img);
			_country.lang_info_array[l.id] = { 
				'name': controls['name_' + l.key].value
				//'img': (controls['img_' + l.key].value ? controls['img_' + l.key].value : (this.country.lang_info_array[l.key].img ? this.country.lang_info_array[l.key].img : '')),
				// 'img': controls['img_' + l.key].value,
			};
			// _country.lang_info_array[l.id].name = controls['name_'+l.key].value;
			// _country.lang_info_array[l.id].img = controls['img_'+l.key].value;
		});
		_country.lang_info_array = JSON.stringify(_country.lang_info_array);
		return _country;
	}

	/**
	 * Add Country
	 *
	 * @param _country: Country
	 * @param withBack: boolean
	 */
	addCountry(_country: Country, withBack: boolean = false) {
		this.store.dispatch(new CountryOnServerCreated({ country: _country }));
		const addSubscription = this.store.pipe(select(selectLastCreatedCountryId)).subscribe(newId => {
			console.log("-------newId", newId);
			const message = (newId) ? this.translate.instant('COUNTRY_MANAGEMENT.EDIT.ADD_MESSAGE') : this.translate.instant('COMMON.LOADING'); //this.translate.instant('COUNTRY_MANAGEMENT.EDIT.ADD_ERROR_MESSAGE');
			this.layoutUtilsService.showActionNotification(message, MessageType.Create, 5000, true, true);
			if (newId) {
				if (withBack) {
					this.goBackWithId();
				} else {
					this.refreshCountry(true, newId);
				}
			}
		});
		this.subscriptions.push(addSubscription);
	}

	/**
	 * Update country
	 *
	 * @param _country: Country
	 * @param withBack: boolean
	 */
	updateCountry(_country: Country, withBack: boolean = false) {
		// Update Country
		// tslint:disable-next-line:prefer-const

		const updatedCountry: Update<Country> = {
			id: _country.id,
			changes: _country
		};
		this.store.dispatch(new CountryUpdated( { partialCountry: updatedCountry, country: _country }));
		const message = this.translate.instant('COUNTRY_MANAGEMENT.EDIT.UPDATE_MESSAGE');
		this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, true, true);
		if (withBack) {
			this.goBackWithId();
		} else {
			this.refreshCountry(false);
		}
	}

	/**
	 * Returns component title
	 */
	getComponentTitle() {
		let result = this.translate.instant('MANAGEMENT.COMMON.CREATE_ENTITY', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN') });
		if (!this.country || !this.country.id) {
			return result;
		}
		let name = (this.country.lang_info_array[this.currentLang] && this.country.lang_info_array[this.currentLang].name) ? this.country.lang_info_array[this.currentLang].name : this.country.id;
		result = this.translate.instant('MANAGEMENT.COMMON.EDIT_ENTITY_NAME', { entity: this.translate.instant('COUNTRY_MANAGEMENT.ENTITY_MIN'), entity_name: name });

		// result = `Edit country - ${this.country.name}`;
		return result;
	}

	/**
	 * Close Alert
	 *
	 * @param $event: Event
	 */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}


	openBrowser(event: any) {
		event.preventDefault();
		let element: HTMLElement = document.getElementById('flag') as HTMLElement;
		element.click();
	}
	readFile(event: any) {
		// this.fileData[lang] = <File>event.target.files[0];
		// this.image[lang] = this.fileData[lang].name;
		console.log("@@@@@@@@@@@@@ readFile",this.fileData);

		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			this.fileData = event.target.files[0];
			reader.readAsDataURL(this.fileData);
			reader.onload = () => {
				this.countryForm.get('flag').setValue({
					filename: this.fileData.name,
					filetype: this.fileData.type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}


		this.preview();
	}
	preview() {
		// Show preview 
		var mimeType = this.fileData.type;
		if (mimeType.match(/image\/*/) == null) {
			return;
		}

		var reader = new FileReader();
		reader.readAsDataURL(this.fileData);
		reader.onload = (_event) => {
			this.previewUrl = reader.result;
		}
	}

	deletePreviewUrl(lang){
		this.countryForm.controls['flag'].setValue('');
		this.previewUrl = null;
	}

	deleteImg(lang){
		this.country.flag = '';
	}
}
