// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
  apiEndpoint: 'http://192.168.1.135:8000',
  baseUrlImg: 'https://api.tram-bcn-open.tridenia.es/public',
  client_id: "1_3wvnizv8jpwk4ok0ssks44w0c8w8w044k8ccwc0o4sk0k8kkwo",
  client_secret: "3f35xti5tz284gw8w4cwk8kcog4oo8wk80g8kg48w8co0880sc",
  grant_type: "password",
  role_arbitrator: 5,
  role_player: 6
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
