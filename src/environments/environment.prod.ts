export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	apiEndpoint: 'https://api.tram-bcn-open.tridenia.es',
	baseUrlImg: 'https://api.tram-bcn-open.tridenia.es/public',
	client_id: "1_3wvnizv8jpwk4ok0ssks44w0c8w8w044k8ccwc0o4sk0k8kkwo",
	client_secret: "3f35xti5tz284gw8w4cwk8kcog4oo8wk80g8kg48w8co0880sc",
	grant_type: "password",
	role_arbitrator: 5,
	role_player: 6
};
